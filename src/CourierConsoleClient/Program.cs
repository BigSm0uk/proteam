﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.MenuItems;
using CourierConsoleClient.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CourierConsoleClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            var ClientType = ConsoleExtensions.ChooseMenuItemName(ClientInfo.ClientTypes);
            Console.Title = $"{ClientType} {Environment.ProcessId}";
            services.AddSingleton(ClientInfo.Create(ClientType));

            services.RegisterServices();

            services.AddMenuItems();

            services.AddScoped<NewMenu>();

            var serviceProvider = services.BuildServiceProvider();


            while (true)
            {
                try
                //using (var scope = serviceProvider.CreateScope())
                //{
                //    var menuItems = scope.ServiceProvider.GetServices<IMenuItem>();
                //    menu.Init(menuItems);
                //}
                {
                    using (var scope = serviceProvider.CreateScope())
                    {
                        var menu = serviceProvider.GetRequiredService<NewMenu>();
                        var type = menu.GetMenuItem().GetType();
                        var svc = scope.ServiceProvider.GetRequiredService(type) as IMenuItem;
                        svc?.Execute();
                    }
                }
                catch (Exception ex)
                {
                    ConsoleExtensions.ShowExceptionAndWait(ex);
                }
            }
        }
    }
}