﻿using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class UserInfoMenuItem : IMenuItem
    {
        private readonly IUserInterface _userInterface;
        private readonly ITokenHandler _tokenHandler;

        public UserInfoMenuItem(IUserInterface userInterface, ITokenHandler tokenHandler)
        {
            _userInterface = userInterface;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.ShowUserInfo;

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin;
        }

        public void Execute() =>
            _userInterface.ShowUserInfo();

    }
}
