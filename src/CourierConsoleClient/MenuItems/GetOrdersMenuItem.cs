﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Services;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class GetOrdersMenuItem : IMenuItem
    {
        private readonly IOrderApiClient _orderApiClient;
        private readonly ITokenHandler _tokenHandler;

        public GetOrdersMenuItem(IOrderApiClient orderApi, 
            ITokenHandler tokenHandler)
        {
            _orderApiClient = orderApi;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.ShowOrderDetails;

        public void Execute()
        {
            var orders = _orderApiClient.GetAvailableOrders();

            var orderMenu = orders
                .Select(x => $"{x.Status}: {x.Title ?? x.Description}")
                .ToList();
            orderMenu.Add(CommandOptions.Back.Name);

            int cursorPosition = ConsoleExtensions.ChooseMenuItem(orderMenu.ToArray());

            if (cursorPosition == orderMenu.Count - 1)
                return;

            var orderId = orders.ElementAt(cursorPosition).Id;
            var orderDetails = _orderApiClient.GetOrderDetails(orderId);

            Console.WriteLine($"{orderDetails}");

            Console.ReadKey(true);
        }

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin && _tokenHandler.Role == ClientInfo.Courier;
        }
    }
}
