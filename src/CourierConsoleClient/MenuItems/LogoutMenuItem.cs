﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class LogoutMenuItem : IMenuItem
    {
        private readonly IAuthApiClient _authApiClient;
        private readonly ITokenHandler _tokenHandler;


        public LogoutMenuItem(IAuthApiClient authApiClient, 
            ITokenHandler tokenHandler)
        {
            _authApiClient = authApiClient;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.Logout;

        public void Execute()
        {
            if (_authApiClient.TryLogout().Result)
                ConsoleExtensions.ShowMessageAndWait("Выход выполнен");
        }

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin;
        }
    }
}
