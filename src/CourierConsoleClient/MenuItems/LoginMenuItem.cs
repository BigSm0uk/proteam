﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class LoginMenuItem : IMenuItem
    {
        private readonly IAuthApiClient _authApiClient;
        private readonly IUserInterface _userInterface;
        private readonly ITokenHandler _tokenHandler;


        public LoginMenuItem(IAuthApiClient authApiClient, 
            IUserInterface userInterface, 
            ITokenHandler tokenHandler)
        {
            _authApiClient = authApiClient;
            _userInterface = userInterface;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.Login;

        public void Execute()
        {
            var userData = _userInterface.GetUserData();
            var authResult = _authApiClient.Login(userData).Result;
            _tokenHandler.SetUserToken(authResult);

            Console.WriteLine();
            ConsoleExtensions.ShowMessageAndWait(authResult);
        }

        public bool CanExecute()
        {
            return !_tokenHandler.IsLogin;
        }
    }
}
