﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Services;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class CreateOrderMenuItem : IMenuItem
    {
        private readonly IOrderApiClient _orderApiClient;
        private readonly ITokenHandler _tokenHandler;

        public CreateOrderMenuItem(IOrderApiClient orderApi, 
            ITokenHandler tokenHandler)
        {
            _orderApiClient = orderApi;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.CreateOrder;

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin && _tokenHandler.Role == ClientInfo.Customer;
        }

        public void Execute()
        {
            var orderToCreate = new
            {
                Title = ConsoleExtensions.ReadNotEmptyString("Введите название заказа:"),
                Description = ConsoleExtensions.ReadNotEmptyString("Введите описание заказа:"),
                //WeightKg = ConsoleExtensions.ReadDouble("Введите общий вес заказа:"),
                DeliveryCost = ConsoleExtensions.ReadDouble("Введите стоимость доставки:"),
                CustomerId = _tokenHandler.UserId
            };

            var orderId = _orderApiClient.CreateOrder(orderToCreate);

            Console.WriteLine($"Заказ с номером {orderId} успешно создан");

            Console.ReadKey(true);
        }
    }
}
