﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Services;
using System.Text.Json.Serialization;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class RegisterMenuItem : IMenuItem
    {
        private readonly IAuthApiClient _authApiClient;
        private readonly IUserInterface _userInterface;
        private readonly ITokenHandler _tokenHandler;
        private readonly ClientInfo _clientInfo;

        public RegisterMenuItem(IAuthApiClient authApiClient,
            IUserInterface userInterface,
            ITokenHandler tokenHandler,
            ClientInfo clientInfo)
        {
            _authApiClient = authApiClient;
            _userInterface = userInterface;
            _tokenHandler = tokenHandler;
            _clientInfo = clientInfo;
        }

        public (string Name, int Rank) Options => CommandOptions.Register;

        public void Execute()
        {
            var newUserData = _userInterface.GetNewUserData();
            newUserData.Role = _clientInfo.Role;
            var result = _authApiClient.Register(newUserData).Result;
            Console.WriteLine(result);
            Console.ReadKey();
        }

        public bool CanExecute()
        {
            return !_tokenHandler.IsLogin;
        }
    }
}
