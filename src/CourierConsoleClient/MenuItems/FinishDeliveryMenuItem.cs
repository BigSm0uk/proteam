﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Services;
using System.ComponentModel.Design;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class FinishDeliveryMenuItem : IMenuItem
    {
        private readonly IOrderApiClient _orderApiClient;
        private readonly ITokenHandler _tokenHandler;

        public FinishDeliveryMenuItem(IOrderApiClient orderApi, 
            ITokenHandler tokenHandler)
        {
            _orderApiClient = orderApi;
            _tokenHandler = tokenHandler;
        }

        public (string Name, int Rank) Options => CommandOptions.FinishDelivery;

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin && _tokenHandler.Role == ClientInfo.Courier;
        }

        public void Execute()
        {
            var orders = _orderApiClient.GetLoadedOrders();

            var orderMenu = orders
                .Select(x => $"{x.Status}: {x.Title ?? x.Description}")
                .ToList();
            orderMenu.Add(CommandOptions.Back.Name);

            int cursorPosition = ConsoleExtensions.ChooseMenuItem(orderMenu.ToArray());

            if (cursorPosition == orderMenu.Count - 1)
                return;

            var orderId = orders.ElementAt(cursorPosition).Id;

            var result = _orderApiClient.FinishDelivery(orderId)
                ? "Вы завершили заказ"
                : "Не удалось завершить заказ";

            Console.WriteLine(result);

            Console.ReadKey(true);
        }
    }
}
