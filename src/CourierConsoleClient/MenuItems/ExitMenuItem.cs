﻿using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    internal class ExitMenuItem : IMenuItem
    {
        public (string Name, int Rank) Options => CommandOptions.Exit;

        public bool CanExecute() => true;

        public void Execute() => Environment.Exit(0);
    }
}
