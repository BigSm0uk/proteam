﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Services;

namespace CourierConsoleClient.MenuItems
{
    [ScopedRegistration]
    public class GetMyOrdersMenuItem : IMenuItem
    {
        private readonly IOrderApiClient _orderApiClient;
        private readonly ITokenHandler _tokenHandler;
        private readonly ClientInfo _clientInfo;

        public GetMyOrdersMenuItem(IOrderApiClient orderApi, 
            ITokenHandler tokenHandler, ClientInfo clientInfo)
        {
            _orderApiClient = orderApi;
            _tokenHandler = tokenHandler;
            _clientInfo = clientInfo;
        }

        public (string Name, int Rank) Options => _clientInfo.Role == ClientInfo.Courier 
            ? CommandOptions.MyDeliveries
            : CommandOptions.MyOrders;

        public void Execute()
        {
            var orders = _orderApiClient.GetMyOrders();

            var orderMenu = orders
                .Select(x => $"{x.Status}: {x.Title ?? x.Description}")
                .ToList();
            orderMenu.Add(CommandOptions.Back.Name);

            int cursorPosition = ConsoleExtensions.ChooseMenuItem(orderMenu.ToArray());

            if (cursorPosition == orderMenu.Count - 1)
                return;

            var orderId = orders.ElementAt(cursorPosition).Id;
            var orderDetails = _orderApiClient.GetOrderDetails(orderId);

            Console.WriteLine($"{orderDetails}");

            Console.ReadKey(true);
        }

        public bool CanExecute()
        {
            return _tokenHandler.IsLogin;
        }
    }
}
