﻿namespace CourierConsoleClient.Models
{
    public record Order(
        Guid Id,
        string? Title,
        string? Description,
        string? Status);
}
