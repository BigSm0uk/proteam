﻿using System.Text.Json.Serialization;

namespace CourierConsoleClient.Models
{
    public class UserData
    {
        [JsonPropertyName("Username")]
        public string UserName { get; set; }

        [JsonPropertyName("Password")]
        public string Password { get; set; }
    }
}
