﻿using System.Text.Json.Serialization;

namespace CourierConsoleClient.Models
{
    public class NewUserData : UserData
    {
        [JsonPropertyName("ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        [JsonPropertyName("Email")]
        public string Email { get; set; }

        [JsonPropertyName("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonPropertyName("Role")]
        public string Role { get; set; }
    }
}
