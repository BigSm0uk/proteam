﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierConsoleClient.Models
{
    public class CreateOrderRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public double WeightKg { get; set; }
        public double DeliveryCost { get; set; }
        public Guid CustomerId { get; set; }
    }
}
