﻿namespace CourierConsoleClient.ConsoleUIExtensions
{
    public static class ConsoleExtensions
    {
        /// <summary>
        /// Ввод данных с их проверкой (пока только не непустую строку)
        /// </summary>
        /// <returns></returns>
        public static string ReadNotEmptyString(string? tip = null)
        {
            if (tip != null)
                Console.WriteLine(tip);

            string? data = "";
            while (true)
            {
                data = Console.ReadLine()?.Trim();
                Console.WriteLine();
                if (string.IsNullOrEmpty(data))
                    Console.WriteLine("Поле ввода не должно быть пустым! Попробуйте снова");
                else 
                    break;
            }
            return data;
        }

        public static double ReadDouble(string? tip = null)
        {
            double weight;
            while (true)
            {
                if (!double.TryParse(ReadNotEmptyString(tip), out weight))
                    Console.WriteLine("Необходимо ввести числовое значение! Попробуйте снова");
                else 
                    break;
            }
            return weight;
        }

        public static void ShowMessageAndWait(string message)
        {
            // Show
            Console.WriteLine(message);
            // Wait
            Console.ReadKey(true);
        }

        /// <summary>
        /// Метод выбора пункта меню
        /// </summary>
        /// <param name="items">Пункты меню</param>
        /// <returns>Возвращает число обозначающее выбранный пользователем пункт меню</returns>
        public static int ChooseMenuItem(params string[] items)
        {
            int cursorPosition = 0; 

            ConsoleKeyInfo buttonPressed;

            while (true) 
            {
                Console.Clear();

                for (int i = 0; i < items.Length; i++) 
                {
                    if (i == cursorPosition)
                        Console.Write("> "); 
                    else
                        Console.Write("  "); 
                    Console.WriteLine(items[i]);
                }

                buttonPressed = Console.ReadKey(true);

                if (buttonPressed.Key == ConsoleKey.UpArrow && cursorPosition != 0)
                    cursorPosition--;
                if (buttonPressed.Key == ConsoleKey.DownArrow && cursorPosition != items.Length - 1)
                    cursorPosition++;
                if (buttonPressed.Key == ConsoleKey.Enter) 
                    break;
            }

            return cursorPosition;
        }

        public static string ChooseMenuItemName(params string[] items)
        {
            return items[ChooseMenuItem(items)];
        }

        internal static void ShowExceptionAndWait(Exception ex)
        {
            Console.WriteLine();
            Console.WriteLine("(!) " + ex.GetType().Name);
            Console.WriteLine(ex.Message);
            Console.ReadKey(true);
        }
    }
}
