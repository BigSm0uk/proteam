﻿using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.Services
{
    //[ScopedRegistration]
    public class Menu : IMenu
    {
        private Dictionary<string, Action> _actions;

        public Menu(IEnumerable<IMenuItem> menuItems)
        {
            _actions = PullMenuItems(menuItems);
            var exitWord = "Выход";
            _actions.Add(exitWord, () => Environment.Exit(0));
        }

        private Dictionary<string, Action> PullMenuItems(IEnumerable<IMenuItem> menuItems)
        {
            var actions = new Dictionary<string, Action>();

            foreach (var item in menuItems)
            {
                actions.Add(item.ItemName, item.Execute);
            }
            return actions;
        }

        public void ExecuteMenuItem()
        {
            while (true)
            {
                var userChoice = _actions[ChooseMenuItem()];
                if (userChoice != null)
                {
                    try
                    {
                        userChoice.Invoke();
                        //return;
                    }
                    catch (Exception ex)
                    {                         
                        //var errorMessage = ex.Message;
                        Console.WriteLine(ex.Message);
                        Console.ReadKey();
                        //Environment.Exit(-1);
                    }
                }
            }
        }

        /// <summary>
        /// Метод выбора пункта меню
        /// </summary>
        /// <param name="menuItems">Массив содержащий пункты меню</param>
        /// <returns>Возвращает число обозначающее выбранный пользователем пункт меню</returns>
        public string ChooseMenuItem()
        {
            var _menuItems = _actions.Keys.ToArray();


            int visibleChoice = 0; //счетчик выбранного пользователем пункта меню

            ConsoleKeyInfo buttonPressed; //нажимаемая пользователем клавиша

            while (true) //цикл использования меню
            {
                Console.Clear();

                for (int i = 0; i < _menuItems.Length; i++) //цикл для определения положения выбора пользователя
                {
                    if (i == visibleChoice)
                        Console.Write(
                            "> "); //отображение положения пользователя в меню если счетчик совпадает с номером строки
                    else
                        Console.Write(
                            "  "); //отображение пробела (пустого места) в меню если счетчик не совпадает с номером строки
                    Console.WriteLine(_menuItems[i]); //отображение пунктов меню
                }

                buttonPressed = Console.ReadKey();

                if (buttonPressed.Key == ConsoleKey.UpArrow && visibleChoice != 0)
                    visibleChoice--; //уменьшение счетчика если нажата клавиша "вверх" и счетчик не находится в максимально верхнем положении
                if (buttonPressed.Key == ConsoleKey.DownArrow && visibleChoice != _menuItems.Length - 1)
                    visibleChoice++; //увеличение счетчика если нажата клавиша "вниз" и счетчик не находится в максимально нижнем положении

                if (buttonPressed.Key == ConsoleKey.Enter) break;
            }

            return _menuItems[visibleChoice];
        }
    }
}
