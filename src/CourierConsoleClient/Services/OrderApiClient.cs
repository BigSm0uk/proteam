﻿using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Models;
using System.Net.Http.Json;

namespace CourierConsoleClient.Services
{
    [ScopedRegistration]
    public class OrderApiClient : IOrderApiClient
    {
        private readonly ITokenHandler _tokenHandler;
        private readonly HttpClient _client;

        public OrderApiClient(ITokenHandler tokenHandler)
        {
            _tokenHandler = tokenHandler;
            _client = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:7110"),
            };
            PutAccessToken();
        }

        public Guid CreateOrder<T>(T content)
        {
            var response = _client
                .PostAsJsonAsync("api/v1/Order/CreateOrder", content)
                .Result;

            response.EnsureSuccessStatusCode();

            var newOrderId = response
                .Content
                .ReadFromJsonAsync<Guid>()
                .Result;

            return newOrderId;
        }

        public IEnumerable<Order> GetAvailableOrders() => GetOrders(status: "Created");
        public IEnumerable<Order> GetAcceptedOrders() => GetOrders(status: "Accepted");
        public IEnumerable<Order> GetLoadedOrders() => GetOrders(status: "Loaded");
        public IEnumerable<Order> GetMyOrders()
            => _tokenHandler.Role == ClientInfo.Customer ? GetOrders(customerId: _tokenHandler.UserId)
            : _tokenHandler.Role == ClientInfo.Courier ? GetOrders(courierId: _tokenHandler.UserId)
            : GetOrders();

        private IEnumerable<Order> GetOrders(string? status = null, Guid? customerId = null, Guid? courierId = null)
        {
            var response = _client
                .GetAsync($"api/v1/Order/GetOrdersList" +
                $"?OrderStatus={status}" +
                $"&CustomerId={customerId}" +
                $"&CourierId={courierId}")
                .Result;

            response.EnsureSuccessStatusCode();

            var orders = response
                .Content
                .ReadFromJsonAsync<IEnumerable<Order>>()
                .Result
                ?? throw new NullReferenceException("Результат десериализации контента - null");

            return orders;
        }


        public string GetOrderDetails(Guid orderId)
        {
            var response = _client
                .GetAsync($"api/v1/Order/GetSingleOrderById?OrderId={orderId}")
                .Result;

            response.EnsureSuccessStatusCode();

            var orderString = response
                .Content
                .ReadAsStringAsync()
                .Result;

            return JsonHelper.FormatJson(orderString);
        }

        public bool AcceptOrder(Guid orderId) => ChangeOrder(orderId, "Accepted", _tokenHandler.UserId);
        public bool StartDelivery(Guid orderId) => ChangeOrder(orderId, "Loaded");
        public bool FinishDelivery(Guid orderId) => ChangeOrder(orderId, "Delivered");

        private bool ChangeOrder(Guid orderId, string orderStatus, Guid? courierId = null)
        {
            var response = _client
                .PostAsJsonAsync($"api/v1/Order/UpdateOrder", new
                {
                    Id = orderId,
                    Status = orderStatus,
                    CourierId = courierId
                })
                .Result;

            response.EnsureSuccessStatusCode();

            return true;
        }

        private void EnsureUserIsLogin()
        {
            if (!_client.DefaultRequestHeaders.Contains("Authorization"))
                PutAccessToken();
        }

        private void PutAccessToken()
        {
            var token = _tokenHandler.Token;
            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
        }
    }
}
