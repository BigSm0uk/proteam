﻿using CourierConsoleClient.Interfaces;
using System.IdentityModel.Tokens.Jwt;

namespace CourierConsoleClient.Services
{
    [SingletonRegistration]
    public class TokenHandler : ITokenHandler
    {
        private readonly ClientInfo _clientInfo;

        public TokenHandler(ClientInfo clientInfo)
        {
            _clientInfo = clientInfo;
        }

        public bool IsLogin => !string.IsNullOrEmpty(Token);
        public string? Token { get; private set; }

        public void SetUserToken(string token)
        {
            if (!string.IsNullOrEmpty(Token))
            {
                throw new Exception("Пользователь уже вошел. Завершите текущий сеанс.");
            }
            Token = token;
        }

        public void ResetUserToken()
        {
            Token = null;
        }

        public Guid UserId
        {
            get
            {
                if (!IsLogin)
                    throw new Exception("Токен доступа отсутствует");

                var id = JwtToken.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;

                if (string.IsNullOrEmpty(id))
                    throw new Exception("Не удалось получить Id пользователя");

                return Guid.Parse(id);
            }
        }

        public string Role //=> _clientInfo.Role;
        {
            get
            {
                if (!IsLogin)
                    throw new Exception("Токен доступа отсутствует");

                var role = JwtToken.Claims.FirstOrDefault(c => c.Type == "role")?.Value;

                if (string.IsNullOrEmpty(role))
                    throw new Exception("Не удалось получить Role пользователя");

                return role;
            }
}

public string Scopes
        {
            get
            {
                if (!IsLogin)
                    throw new Exception("Токен доступа отсутствует");

                return  string.Concat(
                    JwtToken
                    .Claims
                    .Where(c => c.Type == "scope")
                    .Select(c => c.Value + " "));
            }
        }

        private JwtSecurityToken JwtToken => 
            (JwtSecurityToken)new JwtSecurityTokenHandler().ReadToken(Token);
    }
}
