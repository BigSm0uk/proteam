﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierConsoleClient.Services
{

    [ScopedRegistration]
    internal class ConsoleUserInterface : IUserInterface
    {
        private ITokenHandler _tokenHandler;

        public ConsoleUserInterface(ITokenHandler tokenHandler)
        {
            _tokenHandler = tokenHandler;
        }

        public NewUserData GetNewUserData()
        {
            NewUserData userData = new NewUserData();

            bool correctInput = false;

            Console.Write("Введите имя пользователя:");
            userData.UserName = ConsoleExtensions.ReadNotEmptyString();
            while (!correctInput)
            {
                Console.Write("Введите пароль:");
                userData.Password = ConsoleExtensions.ReadNotEmptyString();

                Console.Write("Введите подтверждение пароля:");
                userData.ConfirmPassword = ConsoleExtensions.ReadNotEmptyString();

                if (userData.Password == userData.ConfirmPassword) correctInput = true;
                else Console.WriteLine("Пароли должны совпадать. Попробуйте снова!");
            }

            Console.Write("Введите адрес эл.почты:");
            userData.Email = ConsoleExtensions.ReadNotEmptyString();

            Console.Write("Введите номер телефона:");
            userData.PhoneNumber = ConsoleExtensions.ReadNotEmptyString();

            return userData;
        }

        public UserData GetUserData()
        {
            UserData userData = new UserData();

            Console.Write("Введите имя пользователя:");
            userData.UserName = ConsoleExtensions.ReadNotEmptyString();

            Console.Write("Введите пароль:");
            userData.Password = ConsoleExtensions.ReadNotEmptyString();

            return userData;
        }

        public void ShowUserInfo()
        {
            Console.WriteLine($"ID пользователя: {_tokenHandler.UserId}");
            Console.WriteLine($"Области доступа пользователя: {_tokenHandler.Scopes}");
            Console.ReadKey();
        }

        public CreateOrderRequest GetCreateOrderRequest()
        {
            var orderRequest = new CreateOrderRequest();

            orderRequest.Title = ConsoleExtensions.ReadNotEmptyString("Введите название заказа:");
            orderRequest.Description = ConsoleExtensions.ReadNotEmptyString("Введите описание заказа:");
            orderRequest.WeightKg = ConsoleExtensions.ReadDouble("Введите общий вес заказа:");
            orderRequest.DeliveryCost = ConsoleExtensions.ReadDouble("Введите стоимость доставки:");

            return orderRequest;
        }

        public void ShowMessageAndWait(string message)
        {
            // Show
            Console.WriteLine(message);
            // Wait
            Console.ReadKey(true);
        }
    }
}
