﻿using CourierConsoleClient.Interfaces;
using CourierConsoleClient.Models;
using IdentityModel.Client;
using System.Net.Http.Json;
using System.Text.Json;

namespace CourierConsoleClient.Services
{
    [ScopedRegistration]
    public class IdentityServerClient : IAuthApiClient
    {
        private readonly HttpClient _client;
        private readonly ClientInfo _clientInfo;
        private readonly ITokenHandler _tokenHandler;

        public IdentityServerClient(ClientInfo info, ITokenHandler tokenHandler)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri ("http://localhost:6001")
            };
            _clientInfo = info;
            _tokenHandler = tokenHandler;
        }

        public async Task<string> Login(UserData user)
        {
            var discoveryDocumetn = await _client.GetDiscoveryDocumentAsync();
            
            var tokenResponse = await _client.RequestPasswordTokenAsync(
                new PasswordTokenRequest
                {
                    Address = discoveryDocumetn.TokenEndpoint,
                    ClientId = "console",
                    ClientSecret = "secret",
                    Scope = _clientInfo.Scopes,
                    UserName = user.UserName,
                    Password = user.Password,
                });
            if (tokenResponse?.AccessToken != null)
            {
                return tokenResponse.AccessToken;
            }            
            throw new Exception("Вход в систему не выполнен!");
        }

        public async Task<bool> TryLogout()
        {
            var discoveryDocumetn = await _client.GetDiscoveryDocumentAsync();

            // На самом деле токен остается действительным почему-то 
            var response = await _client.RevokeTokenAsync(
                new TokenRevocationRequest
                {
                    Address = discoveryDocumetn.RevocationEndpoint,
                    ClientId = "console",
                    ClientSecret = "secret",
                    Token = _tokenHandler.Token
                });

            if (response.IsError) 
                throw new Exception(response.Error);

            _tokenHandler.ResetUserToken();

            return true;
        }

        public async Task<string> Register(NewUserData newUser)
        {
            var response = await _client.PostAsJsonAsync("Account/Register", newUser);
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }
    }
}
