﻿using CourierConsoleClient.ConsoleUIExtensions;
using CourierConsoleClient.Interfaces;

namespace CourierConsoleClient.Services
{
    public class NewMenu
    {
        private IEnumerable<IMenuItem>? _items;

        public NewMenu(IEnumerable<IMenuItem> menuItems)
        {
            _items = menuItems;
        }

        internal IMenuItem GetMenuItem()
        {
            var availableItemNames = _items?
                .Where(x => x.CanExecute())
                .OrderBy(x => x.ItemRank)
                .Select(x => x.ItemName)
                .ToArray()
                ?? throw new NullReferenceException("The menu is not initializied.");

            var key = ConsoleExtensions.ChooseMenuItemName(availableItemNames);

            return _items.First(x => x.ItemName == key);
        }
    }
}
