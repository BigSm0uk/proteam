﻿namespace CourierConsoleClient.Services
{
    public class ClientInfo
    {
        public static string Customer = nameof(Customer);
        public static string Courier = nameof(Courier);
        public static string[] ClientTypes => new[] { Customer, Courier };

        public static ClientInfo CreateCustomer() => new ClientInfo
        {
            Scopes = "customerApi orderApi",
            Role = Customer
        };

        public static ClientInfo CreateCourier() => new ClientInfo()
        {
            Scopes = "courierApi orderApi",
            Role = Courier
        };

        public static ClientInfo Create(string client)
        {
            if (client == Customer) return CreateCustomer();
            if (client == Courier) return CreateCourier();
            throw new ArgumentException("Нет такого типа клиента", nameof(client));
        }

        protected ClientInfo() { }

        public string Scopes { get; private set; } = null!;
        public string Role { get; private set; } = null!;
    }

    public class CustomerClient : ClientInfo { }
    public class CourierClient : ClientInfo { }

}