﻿namespace CourierConsoleClient
{
    internal static class CommandOptions
    {
        public static readonly (string Name, int Rank) Login = ("Войти", 0);
        public static readonly (string Name, int Rank) Logout = ("Выйти", 999);
        public static readonly (string Name, int Rank) Register = ("Регистрация", 1);
        public static readonly (string Name, int Rank) ShowUserInfo = ("Информация о текущем пользователе", 997);
        public static readonly (string Name, int Rank) MyOrders = ("Мои заказы", 100);
        public static readonly (string Name, int Rank) MyDeliveries = ("Мои доставки", 100);
        public static readonly (string Name, int Rank) ShowOrderDetails = ("Показать детали заказа", 110);
        public static readonly (string Name, int Rank) CreateOrder = ("Создать новый заказ", 120);
        public static readonly (string Name, int Rank) AcceptOrder = ("Принять заказ", 130);
        public static readonly (string Name, int Rank) StartDelivery = ("Начать доставку", 140);
        public static readonly (string Name, int Rank) FinishDelivery = ("Завершить доставку", 150);
        public static readonly (string Name, int Rank) Back = ("Назад", 998);
        public static readonly (string Name, int Rank) Exit = ("Закрыть", 1000);
    }
}
