﻿namespace CourierConsoleClient
{
    public class ScopedRegistrationAttribute : Attribute { }

    public class SingletonRegistrationAttribute : Attribute { }

    public class TransientRegistrationAttribute : Attribute { }

    public class ServiceRegistrationAttribute : Attribute { }
}
