﻿using CourierConsoleClient.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CourierConsoleClient
{
    public static class ServiceExtensions
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            Type scopedRegistration = typeof(ScopedRegistrationAttribute);
            Type singletonRegistration = typeof(SingletonRegistrationAttribute);
            Type transientRegistration = typeof(TransientRegistrationAttribute);
            Type serviceRegistration = typeof(ServiceRegistrationAttribute);

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(y => y.IsDefined(scopedRegistration, false) 
                    || y.IsDefined(singletonRegistration, false) 
                    || y.IsDefined(transientRegistration, false) 
                    && !y.IsInterface)
                .Select(s => new 
                {
                    Service = s.GetInterfaces()
                        .FirstOrDefault(i => i.IsDefined(serviceRegistration, false)),
                    Implementation = s
                })
                .Where(z => z.Service != null);

            foreach(var type in types)
            {
                if(type.Implementation.IsDefined(scopedRegistration, false))
                {
                    services.AddScoped(type.Service, type.Implementation);
                }
                if (type.Implementation.IsDefined(singletonRegistration, false))
                {
                    services.AddSingleton(type.Service, type.Implementation);
                }
                if (type.Implementation.IsDefined(transientRegistration, false))
                {
                    services.AddTransient(type.Service, type.Implementation);
                }
            }
        }

        public static void AddMenuItems(this IServiceCollection services)
        {
            Type scopedRegistration = typeof(ScopedRegistrationAttribute);
            Type singletonRegistration = typeof(SingletonRegistrationAttribute);
            Type transientRegistration = typeof(TransientRegistrationAttribute);
            Type serviceRegistration = typeof(ServiceRegistrationAttribute);

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(y => y.IsDefined(scopedRegistration, false) 
                    || y.IsDefined(singletonRegistration, false) 
                    || y.IsDefined(transientRegistration, false) 
                    && !y.IsInterface)
                .Select(s => new 
                {
                    Service = s.GetInterfaces()
                        .FirstOrDefault(i => i == typeof(IMenuItem)),
                    Implementation = s
                })
                .Where(z => z.Service != null);

            foreach(var type in types)
            {
                if(type.Implementation.IsDefined(scopedRegistration, false))
                {
                    services.AddScoped(type.Implementation);
                }
                if (type.Implementation.IsDefined(singletonRegistration, false))
                {
                    services.AddSingleton(type.Implementation);
                }
                if (type.Implementation.IsDefined(transientRegistration, false))
                {
                    services.AddTransient(type.Implementation);
                }
            }
        }
    }
}
