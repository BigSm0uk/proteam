﻿namespace CourierConsoleClient.Interfaces
{
    [ServiceRegistration]
    public interface ITokenHandler
    {
        public void SetUserToken(string token);

        public void ResetUserToken();

        public Guid UserId { get; }

        public string Scopes { get; }

        bool IsLogin { get; }
        string? Token { get; }
        string? Role { get; }
    }
}
