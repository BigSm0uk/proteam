﻿using CourierConsoleClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierConsoleClient.Interfaces
{
    [ServiceRegistration]
    public interface IUserInterface
    {
        public UserData GetUserData();

        public NewUserData GetNewUserData();

        public void ShowUserInfo();

        CreateOrderRequest GetCreateOrderRequest();
        void ShowMessageAndWait(string message);
    }
}
