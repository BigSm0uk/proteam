﻿namespace CourierConsoleClient.Interfaces
{

    [ServiceRegistration]
    public interface IMenuItem
    {
        public string ItemName => Options.Name;
        public int ItemRank => Options.Rank;
        public (string Name, int Rank) Options { get; } 
        public void Execute();
        public bool CanExecute();
    }
}