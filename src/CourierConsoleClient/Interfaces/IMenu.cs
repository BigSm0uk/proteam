﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierConsoleClient.Interfaces
{
    [ServiceRegistration]
    public interface IMenu
    {
        public void ExecuteMenuItem();
    }
}
