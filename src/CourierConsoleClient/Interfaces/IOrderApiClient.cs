﻿using CourierConsoleClient.Models;

namespace CourierConsoleClient.Interfaces
{
    [ServiceRegistration]    
    public interface IOrderApiClient
    {
        Guid CreateOrder<T>(T content);
        IEnumerable<Order> GetAcceptedOrders();
        IEnumerable<Order> GetAvailableOrders();
        IEnumerable<Order> GetMyOrders();
        string GetOrderDetails(Guid orderId);
        bool AcceptOrder(Guid orderId);
        bool StartDelivery(Guid orderId);
        bool FinishDelivery(Guid orderId);
        IEnumerable<Order> GetLoadedOrders();
    }
}
