﻿using System.Net.Http.Json;
using System.Text;

namespace CourierConsoleClient.Interfaces
{
    public abstract class ServiceApi
    {
        internal HttpClient _client;

        public async Task<String> HttpPost(string url, string content, string mediaType = "application/json") //post-запрос на сервер и получение данных в видн json-строки
        {
            HttpContent httrContent = new StringContent(content, UnicodeEncoding.UTF8, mediaType);
            using var response = await _client.PostAsync(url, httrContent);
            var responseContent = await response.Content.ReadAsStringAsync();
            return responseContent;
        }

        //get-запрос на получение данных в виде сущности
        public async Task<T> HttpGet<T>(string url) 
        {
            using var response = await _client.GetAsync(url);
            var responseContent = await response.Content.ReadFromJsonAsync<T>();
            return responseContent;
        }
    }
}
