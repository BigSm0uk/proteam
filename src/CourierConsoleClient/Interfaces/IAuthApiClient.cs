﻿using CourierConsoleClient.Models;

namespace CourierConsoleClient.Interfaces
{
    [ServiceRegistration]
    public interface IAuthApiClient
    {
        public Task<string> Register(NewUserData newUser);

        public Task<string> Login(UserData user);

        Task<bool> TryLogout();
    }
}
