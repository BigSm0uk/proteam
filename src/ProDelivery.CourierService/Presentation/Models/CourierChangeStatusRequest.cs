﻿using ProDelivery.CourierService.Core.Enums.Courier;

namespace ProDelivery.CourierService.Presentation.Models
{
    public class CourierChangeStatusRequest
    {
        public CourierStatus Status { get; set; }
    }
}
