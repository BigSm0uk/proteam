﻿using ProDelivery.CourierService.Core.Enums.Courier;
using ProDelivery.CourierService.Core.Transports.Queries;

namespace ProDelivery.CourierService.Presentation.Models
{
    public record CreateCourierRequest
    {
        public string FirstName { get; init; } = null!;

        public string LastName { get; init; } = null!;

        public string? Patronym { get; init; } = null!;

        public DateTimeOffset BirthDate { get; init; }

        public string Email { get; init; } = null!;

        public string PhoneNumber { get; init; } = null!;

        public string Login { get; init; } = null!;

        public string Password { get; init; } = null!;

        public double LoadCapacity { get; internal set; }

        public List<TransportDTO> AvailableTransport { get; set; }
        public CourierStatus CurrentStatus { get; set; }

    }

}
