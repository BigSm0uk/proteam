﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.CourierService.Core.Couriers.Commands;
using ProDelivery.CourierService.Core.Couriers.Queries;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Presentation.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CourierController : 
        ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public CourierController(IMapper mapper, IMediator mediator) =>
            (_mapper, _mediator) = (mapper, mediator);

        /// <summary>
        /// Получить данные Курьера по его Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet("id")]
        public async Task<ActionResult<CourierDTO>> GetCourierById(Guid id)
            => await _mediator.Send(new GetCourierQuery { Id = id });

        /// <summary>
        /// Получить данные всех Курьеров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CourierListDTO>> GetAllASync()
            => await _mediator.Send(new GetCourierListQuery());


        /// <summary>
        /// Добавить Курьера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateCourierAsync(CreateCourierCommand request)
            => await _mediator.Send(request);

        /// <summary>
        /// Редактировать Курьера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<CourierDTO>> UpdateCourierAsync(UpdateCourierCommand request)
        => await _mediator.Send(request);

        /// <summary>
        /// Удалить Курьера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteCourierAsync(DeleteCourierCommand request)
        => await _mediator.Send(request);


        //[HttpPut]
        //public async Task<IActionResult> ChangeStatusAsync(Guid id, CourierChangeStatusRequest request)
        //{
        //    var courier = await _context.Couriers.FirstOrDefaultAsync(c => c.Id == id);
        //    if (courier == null)
        //        return NotFound("Курьер не найден.");

        //    courier.CurrentStatus = request.Status;
        //    await _context.SaveChangesAsync();
        //    return NoContent();
        //}
    }
}
