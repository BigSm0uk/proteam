using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.CourierService.Core.Transports.Queries;

namespace ProDelivery.CourierService.Presentation.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class TransportController :
        ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public TransportController(IMapper mapper, IMediator mediator) =>
            (_mapper, _mediator) = (mapper, mediator);

        /// <summary>
        /// �������� ������ ���� ����� ����������
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<TransportListDTO>> GetAllASync()
            => await _mediator.Send(new GetTransportListQuery());

    }
}