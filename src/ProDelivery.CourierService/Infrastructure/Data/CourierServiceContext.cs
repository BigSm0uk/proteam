﻿using ProDelivery.Common.Core;
using ProDelivery.Common.Infrastructure.Data.Config;
using ProDelivery.CourierService.Infrastructure.Data.Config;

namespace ProDelivery.CourierService.Infrastructure.Data
{
    public class CourierServiceContext : DbContext
    {
        public DbSet<Courier> Couriers { get; set; }
        public DbSet<Transport> Transports { get; set; }
        public DbSet<CourierTransport> CourierTransport { get; set; }


        public CourierServiceContext(DbContextOptions<CourierServiceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CourierConfiguration());
            builder.ApplyConfiguration(new PersonConfiguration());
            builder.ApplyConfiguration(new TransportConfiguration());
            builder.Entity<Person>().ToTable("PersonalData");
            builder.Entity<Courier>().ToTable("Couriers");
            builder.Entity<Transport>().ToTable("Transports");

            builder.Entity<CourierTransport>()
                .HasKey(ct => new { ct.CourierId, ct.TransportId });
            builder.Entity<CourierTransport>()
                .HasOne(ct => ct.Courier)
                .WithMany(c => c.CourierTransports)
                .HasForeignKey(c => c.CourierId);
            builder.Entity<CourierTransport>()
                .HasOne(ct => ct.Transport)
                .WithMany(c => c.CourierTransports)
                .HasForeignKey(c => c.TransportId);

            base.OnModelCreating(builder);
        }
    }
}
