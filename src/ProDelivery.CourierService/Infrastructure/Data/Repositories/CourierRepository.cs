﻿using AutoMapper;
using ProDelivery.Common.Infrastructure.Data.Repositories;
using ProDelivery.CourierService.Core.IRepositories;
using ProDelivery.CourierService.Core.Transports.Queries;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Infrastructure.Data.Repositories
{
    public class CourierRepository : EfRepository<Courier>, ICourierRepository
    {
        private readonly CourierServiceContext _context;
        private readonly IMapper _mapper;
        public CourierRepository(CourierServiceContext context, IMapper mapper) : base(context)
        {
            _context = context;
            _mapper = mapper;
        }

        Task<Guid> ICourierRepository.CreateCourierTranspAsync(Courier courier)
        {

            _context.Couriers.Add(new Courier
            {
                Id = courier.Id,
                FirstName = courier.FirstName,
                LastName = courier.LastName,
                Email = courier.Email,
                PhoneNumber = courier.PhoneNumber,
                Passport = courier.Passport,
                CurrentStatus = courier.CurrentStatus,
                Latitude = courier.Latitude,
                LoadCapacity = courier.LoadCapacity,
                Rating = courier.Rating,
                BirthDate = courier.BirthDate,
                HireDate = courier.HireDate,
                IsActive = courier.IsActive,
                LeavingDate = courier.LeavingDate,
            });

            var transps = _context.Transports.ToList();

            if (courier.CourierTransports.Count > 0)
            {
                foreach (var ct in courier.CourierTransports)
                {
                    if (transps.FirstOrDefault(x => x.Id == ct.TransportId) != null)
                    {
                        _context.CourierTransport.Add(ct);
                    }
                }
            }

            return Task.FromResult(courier.Id);

        }

        Task<CourierDTO> ICourierRepository.GetCourierById(Guid courierId)
        {
            var courier = _context.Couriers.FirstOrDefault(c => c.Id == courierId);

            CourierDTO? courierResponse = null;

            if (courier != null)
            {
                courierResponse = _mapper.Map<CourierDTO>(courier);

                var courTrans = _context.CourierTransport.Where(c => c.CourierId == courier.Id).ToList();
                var trans = _context.Transports.ToList();

                if (courTrans.Any() && trans.Any())
                {
                    foreach (var c in courTrans)
                    {
                        var t = trans.FirstOrDefault(t => t.Id == c.TransportId);
                        var at = new TransportListItemDTO
                        {
                            Id = c.TransportId,
                            Name = t.Name,
                        };

                        courierResponse.AvailableTransport.Add(at);
                    }
                }
            }

            return Task.FromResult(courierResponse);

        }
    }
}
