using ProDelivery.CourierService.Core.IRepositories;

namespace ProDelivery.CourierService.Infrastructure.Data.Repositories
{
    public class TransportRepository : ITransportRepository
    {
        private readonly CourierServiceContext _context;
        public TransportRepository(CourierServiceContext context)
        {
            _context = context;
        }

        IQueryable<Transport> ITransportRepository.GetAll() => _context.Transports;

    }
}
