﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProDelivery.CourierService.Core;
using ProDelivery.CourierService.Core.Enums.Courier;

namespace ProDelivery.CourierService.Infrastructure.Data.Config
{
    public class CourierConfiguration : IEntityTypeConfiguration<Courier>
    {
        public void Configure(EntityTypeBuilder<Courier> builder)
        {
            builder.Property(x => x.LoadCapacity)
                .IsRequired();
            builder.Property(x => x.CurrentStatus)
                .IsRequired()
                .HasDefaultValue(CourierStatus.Offline);
            builder.Property(x => x.Latitude)
                .IsRequired(false);
            builder.Property(x => x.Longitude)
                .IsRequired(false);
            builder.Property(x => x.Passport)
                .HasMaxLength(200)
                .IsRequired();
            builder.Property(x => x.Rating)
                .IsRequired(false);
            builder.Property(x => x.HireDate)
                .IsRequired();
            builder.Property(x => x.LeavingDate)
                .IsRequired(false);

            // Будет выделено в сущность
            //builder.Ignore(x => x.AvailableVehicles);

        }
    }
}
