﻿using ProDelivery.Common.Infrastructure.Data;
using ProDelivery.CourierService.Core.Enums.Courier;

namespace ProDelivery.CourierService.Infrastructure.Data
{
    public class CourierServiceContextSeed : Seed<CourierServiceContext>
    {
        public CourierServiceContextSeed(CourierServiceContext context,
            ILogger<CourierServiceContextSeed> logger)
            : base(context, logger) { }

        protected override void OnDatabaseSeeding(SeedBuilder builder)
        {
            builder.ChooseTable(x => x.Couriers).AddEntities(PreconfiguredCouriers);
            builder.ChooseTable(x => x.Transports).AddEntities(PreconfiguredTransports);
        }

        private IEnumerable<Transport> PreconfiguredTransports => new List<Transport>
        {
            new()
            {
                Name = "Foot"
            },
            new()
            {
                Name = "Bicycle"
            },
            new()
            {
                Name = "Car"
            },
            new()
            {
                Name = "Truck"

            },
        };

        private IEnumerable<Courier> PreconfiguredCouriers => new List<Courier>
        {
            new()
            {
                FirstName = "Ibragim",
                LastName = "Govorin",
                BirthDate = new DateTime(2010, 10, 10),
                Email = "govoibr@amail.uu",
                PhoneNumber = "4815162342",
                IsActive = true,
                Rating = 1.0f,
                LoadCapacity = 500,
                CurrentStatus = CourierStatus.Busy,
                Passport = "0412 440550 отделом МВД по Ленинскому р-ну г. Ипатово 22.02.2024",
                HireDate = new DateTimeOffset()
            },
            new()
            {
                FirstName = "Alesha",
                LastName = "Nikitovich",
                BirthDate = new DateTime(1991, 12, 30),
                Email = "alealesha@bmail.qu",
                PhoneNumber = "+79011231122",
                IsActive = true,
                Rating = 0.0f,
                LoadCapacity = 2,
                //CurrentStatus = CourierStatus.Offline,
                Passport = "0333 333333 отделом УЕФА Атомного р-на г. Несуществуйск 15.10.2011",
                HireDate = new DateTimeOffset()
            }
        };
    }
}
