using MediatR;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.Common.Extensions;
using ProDelivery.Common.Middlewares;
using ProDelivery.CourierService.Core.IRepositories;
using ProDelivery.CourierService.Infrastructure.Data;
using ProDelivery.CourierService.Infrastructure.Data.Repositories;
using ProDelivery.CourierService.MappingProfiles;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers(options
    => options.SuppressAsyncSuffixInActionNames = true);
builder.Services.AddCommonDependencies();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Courier API", Version = "v1" });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

// Data configuration 
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection")
    ?? "Data Source=ProDeliveryDB.sqlite";
builder.Services.AddSqlite<CourierServiceContext>(connectionString);
builder.Services.AddScoped<ICourierRepository, CourierRepository>();
builder.Services.AddScoped<ISeed<CourierServiceContext>, CourierServiceContextSeed>();
builder.Services.AddScoped<ITransportRepository, TransportRepository>();

builder.Services.AddScoped<ISeed<CourierServiceContext>, CourierServiceContextSeed>();

var app = builder.Build();

// Configure the HTTP request pipeline.

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("v1/swagger.json", "Courier API V1");
    });

    app.UseDatabaseSeeding<CourierServiceContext>();
}
app.UseHttpStatusCodeExceptionMiddleware();


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
