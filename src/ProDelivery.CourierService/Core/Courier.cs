﻿using ProDelivery.Common.Core;
using ProDelivery.CourierService.Core.Enums.Courier;

namespace ProDelivery.CourierService.Core
{
    /// <summary>
    /// Карточка курьера
    /// </summary>
    public class Courier : Person
    {
        /// <summary>
        /// 1 Грузоподъемность курьера. 
        /// </summary>
        public double LoadCapacity { get; set; }


        // TODO: выделить средства передвижения в отдельную сущность, доступную сущности курьера
        /// <summary>
        /// 2 Доступные средства передвижения
        /// </summary>
        //public List<Vehicle> AvailableVehicles { get; set; }

        public ICollection<CourierTransport> CourierTransports { get; set; } = new List<CourierTransport>();

        /// <summary>
        /// 3 Текущий статус курьера
        /// </summary>
        public CourierStatus CurrentStatus { get; set; }

        //Пока закоментил. В теории, можно сделать сущность, содержащую информацию о местоположении. Однако же, я думаю, одних координат хватит.
        // 4 Метоположение курьера
        //public Guid RouteId { get; set; }

        /// <summary>
        /// 5 Широта текущей локации
        /// </summary>
        public double? Latitude { get; set; }

        /// <summary>
        /// 6 Долгота текущей локации
        /// </summary>
        public double? Longitude { get; set; }

        //Вычисляемое поле. Не вижу смысла хранить в БД, если уже есть BirthDate
        //public int Age { get; set; }

        /// <summary>
        /// 7 Информация о паспорте
        /// </summary>
        public string Passport { get; set; }

        /// <summary>
        /// 8 Текущий рейтинг курьера
        /// </summary>
        public float? Rating { get; set; }

        /// <summary>
        /// 9 Дата найма курьера
        /// </summary>
        public DateTimeOffset HireDate { get; set; }

        /// <summary>
        /// 10 Дата увольнения
        /// </summary>
        public DateTimeOffset? LeavingDate { get; set; }
    }
}