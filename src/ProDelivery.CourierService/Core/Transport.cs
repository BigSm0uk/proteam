﻿using ProDelivery.Common.Core;

namespace ProDelivery.CourierService.Core
{
    public class Transport: BaseEntity
    {
        public string Name { get; set; }
        public ICollection<CourierTransport> CourierTransports { get; set; }

    }
}
