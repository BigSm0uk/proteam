using MediatR;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ProDelivery.CourierService.Core.IRepositories;

namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class GetTransportListQueryHandler
        : IRequestHandler<GetTransportListQuery, TransportListDTO>
    {
        private readonly ITransportRepository _repository;
        private readonly IMapper _mapper;

        public GetTransportListQueryHandler(ITransportRepository repository, IMapper mapper)
            => (_repository, _mapper) = (repository, mapper);

        public async Task<TransportListDTO> Handle(GetTransportListQuery request, CancellationToken ct)
        {
            var transports = _repository
                .GetAll()
                .ProjectTo<TransportListItemDTO>(_mapper.ConfigurationProvider)
                .ToList();

            return new TransportListDTO { Transports = transports };
        }
    }
}
