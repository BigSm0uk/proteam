namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class TransportListItemDTO
    {
        public Guid Id { get; init; }

        public string Name { get; init; } = null!;

    }
}
