using MediatR;
using ProDelivery.CourierService.Core.Transports.Queries;

namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class GetTransportListQuery
        : IRequest<TransportListDTO>
    {

    }
}
