namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class TransportListDTO
    {
        public IList<TransportListItemDTO> Transports { get; set; } = null!;
    }
}
