namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class TransportDTO
    {
        public Guid id { get; set; }

        public string Name { get; set; }
    }
}
