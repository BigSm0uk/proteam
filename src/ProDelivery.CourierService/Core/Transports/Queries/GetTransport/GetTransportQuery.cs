using MediatR;

namespace ProDelivery.CourierService.Core.Transports.Queries
{
    public class GetTransportQuery : IRequest<TransportDTO>
    {
        public Guid Id { get; set; }
    }
}