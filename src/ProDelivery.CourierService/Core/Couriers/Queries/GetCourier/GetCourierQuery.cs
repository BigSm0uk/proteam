using MediatR;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class GetCourierQuery : IRequest<CourierDTO>
    {
        public Guid Id { get; set; }
    }
}
