using AutoMapper;
using MediatR;
using ProDelivery.Common.Exceptions;
using ProDelivery.CourierService.Core.IRepositories;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class GetCourierQueryHandler : IRequestHandler<GetCourierQuery, CourierDTO>
    {
        private readonly ICourierRepository _repository;
        private readonly IMapper _mapper;

        public GetCourierQueryHandler(ICourierRepository repository, IMapper mapper)
            => (_repository, _mapper) = (repository, mapper);

        public async Task<CourierDTO> Handle(GetCourierQuery request, CancellationToken ct)
        {
            //var Courier = await _repository.GetById(request.Id);
            var courierResponse = await _repository.GetCourierById(request.Id);

            if (courierResponse == null || courierResponse.Id != request.Id)
                throw new NotFoundException(nameof(Courier), request.Id);

            return courierResponse;
        }
    }
}
