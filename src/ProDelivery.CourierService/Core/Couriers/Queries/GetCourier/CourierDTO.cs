using ProDelivery.CourierService.Core.Enums.Courier;
using ProDelivery.CourierService.Core.Transports.Queries;

namespace ProDelivery.CourierService.Presentation.Models
{
    public class CourierDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; init; } = null!;

        public string LastName { get; init; } = null!;

        public string? Patronym { get; init; } = null!;

        public DateTimeOffset BirthDate { get; init; }

        public string Email { get; init; } = null!;

        public string PhoneNumber { get; init; } = null!;

        public string Login { get; init; } = null!;

        public string Password { get; init; } = null!;



        public double LoadCapacity { get; internal set; }

        //public List<Transport> AvailableVehicles { get; set; } = null!;
        public List<TransportListItemDTO> AvailableTransport { get; set; } = new List<TransportListItemDTO>();

        public CourierStatus CurrentStatus { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string Passport { get; set; } = null!;

        public string Rating { get; set; } = null!;

        public string HireDate { get; set; } = null!;

        public string LeavingDate { get; set; } = null!;
    }
}
