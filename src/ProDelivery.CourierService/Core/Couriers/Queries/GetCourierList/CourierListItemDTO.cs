﻿using ProDelivery.CourierService.Core.Enums.Courier;

namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class CourierListItemDTO
    {
        public Guid Id { get; init; }

        public string FirstName { get; init; } = null!;

        public string LastName { get; init; } = null!;

        public string PhoneNumber { get; init; } = null!;

        public CourierStatus Status { get; init; }
    }
}
