using MediatR;

namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class GetCourierListQuery
        : IRequest<CourierListDTO>
    {

    }
}
