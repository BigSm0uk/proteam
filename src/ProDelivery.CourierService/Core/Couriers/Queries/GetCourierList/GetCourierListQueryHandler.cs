using MediatR;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ProDelivery.CourierService.Core.IRepositories;

namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class GetCourierListQueryHandler
        : IRequestHandler<GetCourierListQuery, CourierListDTO>
    {
        private readonly ICourierRepository _repository;
        private readonly IMapper _mapper;

        public GetCourierListQueryHandler(ICourierRepository repository, IMapper mapper)
            => (_repository, _mapper) = (repository, mapper);

        public async Task<CourierListDTO> Handle(GetCourierListQuery request, CancellationToken ct)
        {
            var couriers = _repository
                .GetAll()
                .ProjectTo<CourierListItemDTO>(_mapper.ConfigurationProvider)
                .ToList();

            return new CourierListDTO { Couriers = couriers };
        }
    }
}
