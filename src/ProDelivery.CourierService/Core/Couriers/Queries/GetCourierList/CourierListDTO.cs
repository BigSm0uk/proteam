namespace ProDelivery.CourierService.Core.Couriers.Queries
{
    public class CourierListDTO
    {
        public IList<CourierListItemDTO> Couriers { get; set; } = null!;
    }
}
