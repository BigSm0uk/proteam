using MediatR;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class DeleteCourierCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }
}
