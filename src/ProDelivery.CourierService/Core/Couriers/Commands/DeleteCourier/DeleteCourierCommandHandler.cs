using AutoMapper;
using MediatR;
using ProDelivery.CourierService.Core.IRepositories;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class DeleteCourierCommandHandler : IRequestHandler<DeleteCourierCommand, Unit>
    {
        private readonly ICourierRepository _repository;
        private readonly IMapper _mapper;

        public DeleteCourierCommandHandler(ICourierRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        public async Task<Unit> Handle(DeleteCourierCommand request, CancellationToken ct)
        {
            _repository.Delete(request.Id);
            await _repository.SaveChangesAsync();
            return Unit.Value;
        }
    }
}