using AutoMapper;
using MediatR;
using ProDelivery.CourierService.Core.IRepositories;
using System.Diagnostics;
using ProDelivery.CourierService.MappingProfiles;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class CreateCourierCommandHandler
        : IRequestHandler<CreateCourierCommand, Guid>
    {
        private readonly ICourierRepository _repository;
        private readonly IMapper _mapper;

        public CreateCourierCommandHandler(ICourierRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        public async Task<Guid> Handle(CreateCourierCommand request, CancellationToken ct)
        {
            Debug.Print($"{request} accepted by handler");

            var Courier = _mapper.Map<Courier>(request);

            foreach (var t in request.AvailableTransports)
            {
                Courier.CourierTransports.Add(new CourierTransport
                {
                    Id = Guid.NewGuid(),
                    CourierId = Courier.Id,
                    TransportId = t.id,
                });
            }

            await _repository.CreateCourierTranspAsync(Courier);
            await _repository.SaveChangesAsync();

            return Courier.Id;
        }
    }
}
