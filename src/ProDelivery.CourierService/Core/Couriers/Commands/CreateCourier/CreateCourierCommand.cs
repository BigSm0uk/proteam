using MediatR;
using ProDelivery.CourierService.Core.Enums.Courier;
using ProDelivery.CourierService.Core.Transports.Queries;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class CreateCourierCommand : IRequest<Guid>
    {
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Patronym { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public DateTimeOffset BirthDate { get; set; }
        public string Login { get; set; } = null!;
        public string Password { get; set; } = null!;

        public double LoadCapacity { get; set; }
        //public List<Vehicle> AvailableVehicles { get; set; }
        public List<TransportDTO> AvailableTransports { get; set; }
        public CourierStatus CurrentStatus { get; set; }
        public string Passport { get; set; } = null!;
        public DateTimeOffset HireDate { get; set; }

    }

}
