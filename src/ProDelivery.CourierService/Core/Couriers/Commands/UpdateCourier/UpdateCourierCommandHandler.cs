using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProDelivery.Common.Exceptions;
using ProDelivery.CourierService.Core.IRepositories;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class UpdateCourierCommandHandler
        : IRequestHandler<UpdateCourierCommand, CourierDTO>
    {
        private readonly ICourierRepository _repository;
        private readonly IMapper _mapper;

        public UpdateCourierCommandHandler(ICourierRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        public async Task<CourierDTO> Handle(UpdateCourierCommand request, CancellationToken cancellationToken)
        {
            var Courier = await _repository.GetByIdAsync(request.Id);

            if (Courier == null || Courier.Id != request.Id) 
                throw new NotFoundException(nameof(Courier), request.Id);

            _mapper.Map<Courier>(request);

            var response = _mapper.Map<CourierDTO>(_repository.Update(Courier));
            await _repository.SaveChangesAsync();

            return response;
        }
    }
}
