using MediatR;
using ProDelivery.CourierService.Core.Enums.Courier;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Core.Couriers.Commands
{
    public class UpdateCourierCommand : IRequest<CourierDTO>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronym { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public double LoadCapacity { get; set; }
        public List<Vehicle> AvailableVehicles { get; set; }
        public CourierStatus CurrentStatus { get; set; }
        public string Passport { get; set; }

    }
}
