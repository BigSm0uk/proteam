using ProDelivery.Common.Core;

namespace ProDelivery.CourierService.Core
{
    public class CourierTransport : BaseEntity
    {
        public Guid CourierId { get; set; }
        public Courier Courier { get; set; }


        public Guid TransportId { get; set; }
        public Transport Transport { get; set; }
    }
}
