﻿using ProDelivery.Common.Core.IRepositories;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.Core.IRepositories
{
    public interface ICourierRepository : IEfRepository<Courier>
    {

        Task<Guid> CreateCourierTranspAsync(Courier customer);

        Task<CourierDTO> GetCourierById(Guid courierId);

    }
}
