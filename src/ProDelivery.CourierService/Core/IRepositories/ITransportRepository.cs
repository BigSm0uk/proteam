namespace ProDelivery.CourierService.Core.IRepositories
{
    public interface ITransportRepository
    {
        public IQueryable<Transport> GetAll();
    }
}