﻿namespace ProDelivery.CourierService.Core.Enums.Courier
{
    public enum CourierStatus
    {
        Offline,
        Free,
        Busy,
        InDelivery
    }
}
