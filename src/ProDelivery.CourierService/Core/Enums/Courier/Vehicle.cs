﻿namespace ProDelivery.CourierService.Core.Enums.Courier
{
    public enum Vehicle
    {
        car,
        bicycle,
        truck,
        byFoot,
    }
}
