using AutoMapper;
using ProDelivery.CourierService.Core.Couriers.Commands;
using ProDelivery.CourierService.Core.Couriers.Queries;
using ProDelivery.CourierService.Core.Transports.Queries;
using ProDelivery.CourierService.Presentation.Models;

namespace ProDelivery.CourierService.MappingProfiles
{
    public class CourierProfile : Profile
    {
        public CourierProfile()
        {
            CreateMap<Courier, CourierListItemDTO>();
            CreateMap<Transport, TransportListItemDTO>();
            CreateMap<TransportDTO, Transport>();

            CreateMap<Courier, CourierDTO>();
            CreateMap<CreateCourierCommand, Courier>();
            CreateMap<UpdateCourierCommand, Courier>();
        }
    }
}
