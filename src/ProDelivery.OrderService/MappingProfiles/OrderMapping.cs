﻿using AutoMapper;
using ProDelivery.OrderService.Core.Orders.Commands.CreateOrder;
using ProDelivery.OrderService.Core.Orders.Commands.UpdateOrder;
using ProDelivery.OrderService.Core.Orders.Queries.GetOrdersList;
using ProDelivery.OrderService.Core.Orders.Queries.GetSingleOrderById;

namespace ProDelivery.OrderService.MappingProfiles
{
    public class OrderMapping : Profile
    {
        public OrderMapping()
        {
            CreateMap<CreateOrderCommand, Order>();
            CreateMap<UpdateOrderCommand, Order>()
                //.IgnoreAllPropertiesWithAnInaccessibleSetter()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<Order, OrderListDTO>();
            CreateMap<Order, SingleOrderDTO>();
        }
    }
}
