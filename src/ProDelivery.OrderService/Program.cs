using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.OpenApi.Models;
using ProDelivery.Common.Behaviors;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.Common.Extensions;
using ProDelivery.Common.Infrastructure.Services;
using ProDelivery.Common.Middlewares;
using ProDelivery.OrderService.Core.IRepositories;
using ProDelivery.OrderService.Helpers;
using ProDelivery.OrderService.Infrastructure.Data;
using ProDelivery.OrderService.Infrastructure.Data.Repositories;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System.Reflection;
using System.Text.Json.Serialization;

#region Logging

Log.Logger = new LoggerConfiguration()
.MinimumLevel.Debug()
.MinimumLevel.Override("System", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Information)
.Enrich.FromLogContext()
.WriteTo.Console(theme: AnsiConsoleTheme.Literate)
.CreateLogger();

#endregion
try
{
    #region Base

    var builder = WebApplication.CreateBuilder(args);

    builder.Services
        .AddControllers(options
            => options.SuppressAsyncSuffixInActionNames = true)
        .AddJsonOptions(options
            => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

    #endregion

    #region Tools

    builder.Services.AddLogging(c =>
    {
        c.ClearProviders();
        c.AddSerilog();
    });
    builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());
    builder.Services.AddMediatR(c => c.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
    builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

    #endregion

    #region Identity

    builder.Services.AddSingleton<ICurrentUserService, CurrentUserService>();
    builder.Services.AddHttpContextAccessor();

    builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
        options.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
    })
        .AddIdentityServerAuthentication(options =>
        {
            options.ApiName = "orderApi";
            options.Authority = builder.Configuration["IDENTITY_SERVER_URL"] ?? "http://localhost:6001";
            options.RequireHttpsMetadata = false;
            options.LegacyAudienceValidation = true;
            
        });

    builder.Services.AddAuthorization(opt =>
    {
        opt.AddPolicy("Order", policy =>
        {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("scope", "orderApi");
        });
        opt.AddPolicy("Customer", policy =>
        {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("scope", "orderApi");
            policy.RequireClaim("scope", "customerApi");
            policy.RequireClaim("role", "Customer");
        });
        opt.AddPolicy("Courier", policy =>
        {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("scope", "orderApi");
            policy.RequireClaim("scope", "courierApi");
            policy.RequireClaim("role", "Courier");
        });
    });

    #endregion

    #region Swagger

    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v3", new OpenApiInfo { Title = "Orders API", Version = "v3" });
        c.SchemaFilter<EnumSchemaFilter>();
        var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        c.AddSecurityDefinition("oauth2",
            new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.OAuth2,
                Flows = new OpenApiOAuthFlows
                {
                    Password = new OpenApiOAuthFlow
                    {
                        //AuthorizationUrl = new Uri("https://localhost:6001/connect/authorize"),
                        TokenUrl = new Uri("http://localhost:6001/connect/token"),
                        Scopes = new Dictionary<string, string>
                        {
                                { "orderApi", "Order API" },
                                { "customerApi", "Order API" }
                        },
                    }
                },
            });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "oauth2"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }
            });

        //c.AddSecurityDefinition($"AuthToken",
        //    new OpenApiSecurityScheme
        //    {
        //        In = ParameterLocation.Header,
        //        Type = SecuritySchemeType.Http,
        //        BearerFormat = "JWT",
        //        Scheme = "bearer",
        //        Name = "Authorization",
        //        Description = "Authorization token"
        //    });

        //c.AddSecurityRequirement(new OpenApiSecurityRequirement
        //{
        //    {
        //        new OpenApiSecurityScheme
        //        {
        //            Reference = new OpenApiReference
        //            {
        //                Type = ReferenceType.SecurityScheme,
        //                Id = $"AuthToken"
        //            }
        //        },
        //        new string[] { }
        //    }
        //});
    });

    #endregion

    #region Data

    builder.Services.AddDbContext<OrderServiceContext>(options =>
    {
        var connectionString = builder.Configuration.GetConnectionString("PostgresConnection");
        if (connectionString == null)
            options.UseSqlite(builder.Configuration.GetConnectionString("SqliteConnection"));
        else
        {
            options.UseNpgsql(connectionString);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
    });
    builder.Services.AddScoped<IOrderRepository, OrderRepository>();
    builder.Services.AddScoped<ISeed<OrderServiceContext>, OrderServiceContextSeed>();

    #endregion

    #region Pipeline

    var app = builder.Build();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("v3/swagger.json", "Orders API V3");
            c.OAuthClientId("console");
            c.OAuthClientSecret("secret");
        });
        
        app.UseDatabaseSeeding<OrderServiceContext>();
    }
    app.UseHttpStatusCodeExceptionMiddleware();


    app.UseHttpsRedirection();

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers()
        .RequireAuthorization("Order");

    app.Run();

    #endregion
}
#region Catch

catch (Exception ex)
{
    Log.Fatal(ex, "Fatal application error.");
}
finally
{
    Log.CloseAndFlush();
}

#endregion

public partial class Program { }