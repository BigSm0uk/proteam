﻿using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetOrdersList
{
    public record OrderListDTO
    {
        public Guid Id { get; init; }

        public string Title { get; init; }

        public string Description { get; init; }

        public Guid OrderPositionBatchId { get; init; }

        public Guid CustomerId { get; init; }

        public Guid? CourierId { get; init; }

        public OrderStatus Status { get; init; }

        public IEnumerable<Guid> CargoIds { get; init; }
    }
}
