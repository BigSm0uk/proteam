﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.OrderService.Core.IRepositories;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetOrdersList
{
    public class GetOrdersListQueryHandler : IRequestHandler<GetOrdersListQuery, IEnumerable<OrderListDTO>>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _userService;

        public GetOrdersListQueryHandler(
            IOrderRepository orderRepository, IMapper mapper, ICurrentUserService userService)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
            _userService = userService;
        }

        // TODO: Как разделять запросы от курьера и кастомера ??? 
        public async Task<IEnumerable<OrderListDTO>> Handle(GetOrdersListQuery request, CancellationToken cancellationToken)
        {
            var ordersQuery = _orderRepository.GetAll();

            if (!string.IsNullOrEmpty(request.SearchTerm))
            {
                var searchTermLower = request.SearchTerm.ToLower();
                ordersQuery = ordersQuery.Where(order
                    => order.Title.ToLower().Contains(searchTermLower));
            }

            if (request.OrderStatus.HasValue)
                ordersQuery = ordersQuery.Where(order => order.Status == request.OrderStatus.Value);

            // пользователь может получать только свои заказы 
            if (request.CustomerId.HasValue && request.CustomerId.Value == _userService.UserId)
                ordersQuery = ordersQuery.Where(order => order.CustomerId == request.CustomerId.Value);

            if (request.CourierId.HasValue && request.CourierId.Value == _userService.UserId)
                ordersQuery = ordersQuery.Where(order => order.CourierId == request.CourierId.Value);

            if (request.CargoIds != null && request.CargoIds.Any())
                ordersQuery = ordersQuery.Where(order => request.CargoIds.Contains(order.Id));

            return await ordersQuery.ProjectTo<OrderListDTO>(_mapper.ConfigurationProvider)
                                    .ToListAsync(cancellationToken);
        }
    }
}
