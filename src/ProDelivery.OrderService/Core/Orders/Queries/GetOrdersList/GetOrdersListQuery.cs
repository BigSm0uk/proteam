﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetOrdersList
{
    public class GetOrdersListQuery : IRequest<IEnumerable<OrderListDTO>>
    {
        // TODO: Дописать сценарии фильтрации, если такие появятся
        public Guid? CustomerId { get; init; }

        public Guid? CourierId { get; init; }

        public OrderStatus? OrderStatus { get; init; }

        public string? SearchTerm { get; init; }

        public IEnumerable<Guid>? CargoIds { get; init; }
    }
}
