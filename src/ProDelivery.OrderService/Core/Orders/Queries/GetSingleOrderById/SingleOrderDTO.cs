﻿using EasyCaching.Core.Interceptor;
using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetSingleOrderById
{
    public class SingleOrderDTO : ICachable
    {
        public Guid Id { get; init; }

        public string Title { get; init; }

        public string Description { get; init; }

        public double WeightKg { get; init; }

        public double DeliveryCost { get; init; }

        public Guid OrderPositionBatchId { get; init; }

        public Guid CustomerId { get; init; }

        public Guid? CourierId { get; init; }

        public OrderStatus Status { get; init; }

        public DateTimeOffset CreateDate { get; init; }

        public DateTimeOffset? EstimateDelieryDate { get; init; }

        public DateTimeOffset? LoadingDate { get; init; }

        public DateTimeOffset? DeliveryDate { get; init; }

        public IEnumerable<Guid> CargoIds { get; init; }

        public string CacheKey => Id.ToString();
    }
}
