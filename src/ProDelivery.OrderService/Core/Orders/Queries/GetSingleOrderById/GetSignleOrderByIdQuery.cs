﻿using MediatR;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetSingleOrderById
{
    public class GetSignleOrderByIdQuery : IRequest<SingleOrderDTO>
    {
        public Guid OrderId { get; init; }
    }
}
