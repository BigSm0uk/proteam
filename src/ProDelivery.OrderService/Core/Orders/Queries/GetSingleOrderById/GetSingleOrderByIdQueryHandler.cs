﻿using AutoMapper;
using MediatR;
using ProDelivery.OrderService.Core.IRepositories;

namespace ProDelivery.OrderService.Core.Orders.Queries.GetSingleOrderById
{
    public class GetSingleOrderByIdQueryHandler : IRequestHandler<GetSignleOrderByIdQuery, SingleOrderDTO>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        public GetSingleOrderByIdQueryHandler(IOrderRepository orderRepository, IMapper mapper)
            => (_orderRepository, _mapper) = (orderRepository, mapper);

        public async Task<SingleOrderDTO> Handle(GetSignleOrderByIdQuery request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetByIdAsync(request.OrderId);
            return _mapper.Map<SingleOrderDTO>(order);
        }
    }
}
