﻿using AutoMapper;
using MediatR;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.OrderService.Core.IRepositories;

namespace ProDelivery.OrderService.Core.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, Guid>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public CreateOrderCommandHandler(IOrderRepository orderRepository, IMapper mapper, ICurrentUserService currentUserService)
        {
            (_orderRepository, _mapper) = (orderRepository, mapper);
            _currentUserService = currentUserService;
        }

        public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            //if (request.CustomerId != _currentUserService.UserId)
            //    throw new Exception("request.CustomerId != _currentUserService.UserId");

            var orderModel = _mapper.Map<Order>(request);
            orderModel.CustomerId = _currentUserService.UserId;
            orderModel.CreateDate = DateTimeOffset.Now;

            return await _orderRepository.CreateOrder(orderModel);
        }
    }
}
