﻿using MediatR;

namespace ProDelivery.OrderService.Core.Orders.Commands.CreateOrder
{
    public class CreateOrderCommand : IRequest<Guid>
    {
        public string? Title { get; init; }

        public string Description { get; init; }

        public double? WeightKg { get; init; }

        public double DeliveryCost { get; init; }

        public Guid CustomerId { get; init; }
    }
}
