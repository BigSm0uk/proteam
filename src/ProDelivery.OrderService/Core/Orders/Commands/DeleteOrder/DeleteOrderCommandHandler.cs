﻿using AutoMapper;
using MediatR;
using ProDelivery.OrderService.Core.IRepositories;

namespace ProDelivery.OrderService.Core.Orders.Commands.DeleteOrder
{
    public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, Unit>
    {
        private readonly IOrderRepository _orderRepository;

        public DeleteOrderCommandHandler(IOrderRepository orderRepository)
            => _orderRepository = orderRepository;

        public async Task<Unit> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            await _orderRepository.DeleteOrder(request.Id);
            return Unit.Value;
        }
    }
}
