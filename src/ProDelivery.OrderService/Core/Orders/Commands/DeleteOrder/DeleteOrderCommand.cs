﻿using MediatR;

namespace ProDelivery.OrderService.Core.Orders.Commands.DeleteOrder
{
    public class DeleteOrderCommand : IRequest<Unit>
    {
        /// <summary>
        /// Идентификатор удаляемого заказа
        /// </summary>
        public Guid Id { get; init; }
    }
}
