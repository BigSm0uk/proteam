﻿using MediatR;
using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Core.Orders.Commands.UpdateOrder
{
    public class UpdateOrderCommand : IRequest<Order>
    {
        public Guid Id { get; init; }

        public string? Title { get; init; }

        public string? Description { get; init; }

        public double? WeightKg { get; init; }

        public double? DeliveryCost { get; init; }

        public Guid? CourierId { get; init; }

        public OrderStatus? Status { get; init; }
    }
}
