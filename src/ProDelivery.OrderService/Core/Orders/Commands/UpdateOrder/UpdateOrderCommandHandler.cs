﻿using AutoMapper;
using MediatR;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.Common.Exceptions;
using ProDelivery.OrderService.Core.Enums;
using ProDelivery.OrderService.Core.IRepositories;

namespace ProDelivery.OrderService.Core.Orders.Commands.UpdateOrder
{
    public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, Order>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public UpdateOrderCommandHandler(IOrderRepository orderRepository, 
            IMapper mapper, 
            ICurrentUserService currentUserService)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }

        public async Task<Order> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            var order = _orderRepository.GetById(request.Id)
                ?? throw new NotFoundException(nameof(Order), request.Id);

            if (request.Status == OrderStatus.Accepted && request.CourierId != _currentUserService.UserId)
            {
                throw new InvalidOperationException(
                    "Для принятия заказа id курьера должен совпадать с id текущего пользователя");
            }

            _mapper.Map(request, order);

            await _orderRepository.SaveChangesAsync();

            return order;
        }
    }
}
