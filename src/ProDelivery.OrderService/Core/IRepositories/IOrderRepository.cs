﻿using ProDelivery.Common.Core.IRepositories;

namespace ProDelivery.OrderService.Core.IRepositories
{
    public interface IOrderRepository: IEfRepository<Order>
    {
        public Task<Guid> CreateOrder(Order entity);

        public Task<Order> UpdateOrder(Order entity);

        public Task DeleteOrder(Guid entityId);
    }
}
