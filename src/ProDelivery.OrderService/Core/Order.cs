﻿using ProDelivery.Common.Core;
using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Core
{
    public class Order : BaseEntity
    {
        /// <summary>
        /// Название заказа
        /// </summary>
        public string? Title { get; set; }

        /// <summary>
        /// Описание заказа
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Общий вес заказа
        /// </summary>
        public double? WeightKg { get; set; }

        /// <summary>
        /// Стоимость доставки
        /// </summary>
        public double DeliveryCost { get; set; }

        /// <summary>
        /// Идентификатор геопозиции заказа 
        /// </summary>
        public Guid? OrderPositionBatchId { get; set; }

        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Идентификатор курьера
        /// </summary>
        public Guid? CourierId { get; set; }

        /// <summary>
        /// Статус заказа
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Дата создания заказа
        /// </summary>
        public DateTimeOffset CreateDate { get; set; }

        /// <summary>
        /// Приблизительная дата прибытия
        /// </summary>
        public DateTimeOffset? EstimateDelieryDate { get; set; }

        /// <summary>
        /// Дата отгрузки заказа
        /// </summary>
        public DateTimeOffset? LoadingDate { get; set; }

        /// <summary>
        /// Дата фактической доставки
        /// </summary>
        public DateTimeOffset? DeliveryDate { get; set; }

        /// <summary>
        /// Состав заказа
        /// </summary>
        public IEnumerable<Guid> CargoIds { get; set; }

    }
}
