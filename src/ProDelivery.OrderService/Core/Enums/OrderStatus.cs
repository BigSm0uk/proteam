﻿using System.ComponentModel;

namespace ProDelivery.OrderService.Core.Enums
{
    public enum OrderStatus
    {
        Created,
        Accepted,
        Canceled,
        Suspended,
        ReturnedToSender,
        Loaded,
        Delivered
    }
}
