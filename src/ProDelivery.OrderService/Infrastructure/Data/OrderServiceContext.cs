﻿using System.Reflection;

namespace ProDelivery.OrderService.Infrastructure.Data
{
    public class OrderServiceContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        public OrderServiceContext(DbContextOptions<OrderServiceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(builder);
        }
    }
}
