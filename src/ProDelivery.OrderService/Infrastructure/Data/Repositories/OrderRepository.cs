﻿using ProDelivery.Common.Infrastructure.Data.Repositories;
using ProDelivery.OrderService.Core.IRepositories;
using ProDelivery.OrderService.Infrastructure.Data;

namespace ProDelivery.OrderService.Infrastructure.Data.Repositories
{
    public class OrderRepository : EfRepository<Order>, IOrderRepository
    {
        public OrderRepository(OrderServiceContext context) : base(context)
        { }

        public async Task<Guid> CreateOrder(Order entity)
        {
            var orderId = Create(entity);
            await SaveChangesAsync();
            return orderId;
        }

        public async Task<Order> UpdateOrder(Order entity)
        {
            var order = Update(entity);
            await SaveChangesAsync();
            return order;
        }

        public async Task DeleteOrder(Guid entityId)
        {
            Delete(entityId);
            await SaveChangesAsync();
        }
    }
}
