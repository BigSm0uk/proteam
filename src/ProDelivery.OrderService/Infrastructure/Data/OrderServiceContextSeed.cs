﻿using ProDelivery.Common.Infrastructure.Data;
using ProDelivery.OrderService.Core.Enums;

namespace ProDelivery.OrderService.Infrastructure.Data
{
    public class OrderServiceContextSeed : Seed<OrderServiceContext>
    {
        public OrderServiceContextSeed(OrderServiceContext context,
            ILogger<OrderServiceContextSeed> logger)
            : base(context, logger) { }

        protected override void OnDatabaseSeeding(SeedBuilder builder)
        {
            builder.ChooseTable(x => x.Orders).AddEntities(PreconfiguredOrders);
        }

        private IEnumerable<Order> PreconfiguredOrders => new List<Order>
        {
            new()
            {
                Title = "Доставка небольшой запчасти",
                Description = "Нужно забрать УБЛ из пункта выдачи OZON и доставить в с/ц",
                WeightKg = .200,
                DeliveryCost = 250.0,
                CustomerId = Guid.Parse("daceffc1-2626-4b08-8885-cba2a10edbf8"),
                Status = OrderStatus.Created,
                CreateDate = DateTimeOffset.Now,

            },
            new()
            {
                Title = "Доставка кота покупателю",
                Description = "Нужно отвезти кота на адрес (кот в переноске)",
                WeightKg = 4.100,
                DeliveryCost = 600.0,
                CustomerId = Guid.Parse("e7cf2a0a-74e0-4765-90d5-2ca532be37bf"),
                Status = OrderStatus.Accepted,
                CreateDate = DateTimeOffset.Now - TimeSpan.FromHours(4)
            }
        };
    }
}
