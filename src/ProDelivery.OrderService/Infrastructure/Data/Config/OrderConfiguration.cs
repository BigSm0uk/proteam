﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProDelivery.OrderService.Core;

namespace ProDelivery.OrderService.Infrastructure.Data.Config
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id).IsUnique();
            builder.Property(x => x.Title)
                .HasMaxLength(20)
                .IsRequired(false);
            builder.Property(x => x.Description)
                .HasMaxLength(200)
                .IsRequired();
            builder.Property(x => x.WeightKg)
                .IsRequired(false);
            builder.Property(x => x.DeliveryCost)
                .IsRequired();
            builder.Property(x => x.OrderPositionBatchId)
                .IsRequired(false);
            builder.Property(x => x.CustomerId)
                .IsRequired();
            builder.Property(x => x.CourierId)
                .IsRequired(false);
            builder.Property(x => x.Status)
                .IsRequired();
            builder.Property(x => x.CreateDate)
                .IsRequired();
            builder.Property(x => x.EstimateDelieryDate)
                .IsRequired(false);
            builder.Property(x => x.LoadingDate)
                .IsRequired(false);
            builder.Property(x => x.DeliveryDate)
                .IsRequired(false);

            // Будет сущность cargo ????
            builder.Ignore(x => x.CargoIds);
        }
    }
}
