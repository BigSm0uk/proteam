﻿using IdentityModel;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.OrderService.Core.Enums;
using ProDelivery.OrderService.Core.Orders.Commands.CreateOrder;
using ProDelivery.OrderService.Core.Orders.Commands.DeleteOrder;
using ProDelivery.OrderService.Core.Orders.Commands.UpdateOrder;
using ProDelivery.OrderService.Core.Orders.Queries.GetOrdersList;
using ProDelivery.OrderService.Core.Orders.Queries.GetSingleOrderById;
using System.Net;
using System.Security.Claims;

namespace ProDelivery.OrderService.Presentation.Controllers
{
    // TODO: надо растаскивать методы для кастомера и курьера
    // по разным контроллерам или даже микросервисам 
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #region Customer

        /// <summary>
        /// Найти заказы пользователя.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize("Customer")]
        public async Task<ActionResult> MyOrders()
        {
            var sub = User.FindFirstValue(JwtClaimTypes.Subject);

            if (sub is null)
            {
                return BadRequest("sub claim is null");
            }

            var query = new GetOrdersListQuery
            {
                CustomerId = Guid.Parse(sub)
            };

            var response = await _mediator.Send(query);
            return Ok(response);
        }

        /// <summary>
        /// Создать заказ.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize("Customer")]
        public async Task<ActionResult> CreateOrderAsync(CreateOrderCommand request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }

        #endregion

        /// <summary>
        /// Редактировать заказ.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize("Courier")]
        public async Task<ActionResult> UpdateOrderAsync(UpdateOrderCommand request)
        {
            var sub = User.FindFirstValue(JwtClaimTypes.Subject);

            if (sub is null)
            {
                return BadRequest("sub claim is null");
            }

            var response = await _mediator.Send(request);
            return Ok(response);
        }

        /// <summary>
        /// Удалить заказ.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> DeleteOrderAsync([FromQuery] DeleteOrderCommand request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }

        /// <summary>
        /// Найти заказы согласно условиям фильтрации.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetOrdersList([FromQuery] GetOrdersListQuery request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }

        /// <summary>
        /// Получить данные по заказу.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetSingleOrderById([FromQuery] GetSignleOrderByIdQuery request)
        {
            var response = await _mediator.Send(request);
            return Ok(response);
        }
    }
}
