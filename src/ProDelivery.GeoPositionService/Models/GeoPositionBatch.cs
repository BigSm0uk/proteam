﻿using ProDelivery.Common.Core;

namespace ProDelivery.GeoPositionService.Models
{
    public class GeoPositionBatch : BaseEntity
    {
        /// <summary>
        /// Широта точки отгрузки
        /// </summary>
        public double PickUpLatitude { get; set; }

        /// <summary>
        /// Долгота точки отгрузки
        /// </summary>
        public double PickUpLongitude { get; set; }

        /// <summary>
        /// Широта точки текущего местоположения
        /// </summary>
        public double CurrentLatitude { get; set; }

        /// <summary>
        /// Долгота точки текущего местоположения
        /// </summary>
        public double CurrentLongitude { get; set; }

        /// <summary>
        /// Широта точки назначения
        /// </summary>
        public double DestinationLatitude { get; set; }

        /// <summary>
        /// Долгота точки назначения
        /// </summary>
        public double DestinationLongitude { get; set; }

    }
}
