﻿using ProDelivery.Common.Core;

namespace ProDelivery.GeoPositionService.Models
{
    public class GeoPositionPoint : BaseEntity
    {
        /// <summary>
        /// Широта
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Описание местоположения
        /// </summary>
        public string Description { get; set; }
    }
}
