﻿using Microsoft.Extensions.DependencyInjection;
using ProDelivery.Common.Infrastructure.RabbitMq;

namespace ProDelivery.Common.Extensions
{
    public static class RabbitMqServiceCollectionExtensions
    {
        public static IServiceCollection AddRabbitMqListener(this IServiceCollection services)
        {
            return services.AddHostedService<RabbitMqListener>();
        }

        public static IServiceCollection AddRabbitMqSender(this IServiceCollection services)
        {
            return services.AddScoped<IRabbitMqSevice, RabbitMqService>();

        }
    }
}