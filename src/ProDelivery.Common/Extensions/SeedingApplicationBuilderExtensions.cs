﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using ProDelivery.Common.Core.Abstractions;

namespace ProDelivery.Common.Extensions
{
    public static class SeedingApplicationBuilderExtensions
    {
        /// <summary>
        /// Initializes or updates the database by starting the migration at runtime.
        /// </summary>
        public static IApplicationBuilder UseDatabaseSeeding<TContext>(this IApplicationBuilder builder)
            where TContext : DbContext
        {
            using var scope = builder.ApplicationServices.CreateScope();
            try
            {
                var seed = scope.ServiceProvider.GetRequiredService<ISeed<TContext>>();
                seed.StartSeeding();
            }
            catch (InvalidOperationException)
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<InvalidOperationException>>();
                logger.LogWarning("Database seeding class is not found. " +
                    "If you need to seed database, then implement Seed<TContext> and use" +
                    "'IServiceCollection.AddDatabaseSeeding<TContext>' in the application startup code.");

                scope.ServiceProvider.GetRequiredService<TContext>().Database.Migrate();
            }
            return builder;
        }

        /// <summary>
        /// Initializes development database.
        /// </summary>
        /// <typeparam name="TContext">Type of db-context</typeparam>
        /// <param name="builder">Application builder</param>
        /// <param name="seedDb">Delegate for seeding database</param>
        /// <returns></returns>
        public static IApplicationBuilder InitializeDatabase<TContext>(
            this IApplicationBuilder builder, Action<TContext>? seedDb = null)
            where TContext : DbContext
        {
            using var scope = builder.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<TContext>();
            if (context.Database.EnsureCreated())
                seedDb?.Invoke(context);

            return builder;
        }
    }
}
