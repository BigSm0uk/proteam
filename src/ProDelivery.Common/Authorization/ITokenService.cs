﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDelivery.Common.Authorization
{
    public interface ITokenService
    {
        Task<TokenResponse> GetToken(AuthorizationData data);
    }
}
