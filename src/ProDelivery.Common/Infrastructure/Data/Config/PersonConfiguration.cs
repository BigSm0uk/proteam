﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProDelivery.Common.Core;

namespace ProDelivery.Common.Infrastructure.Data.Config
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            // TODO: делать везде nullable поля, пока не устоялась схема БД
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id).IsUnique();

            builder.Property(x => x.FirstName)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.Property(x => x.LastName)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.Property(x => x.Patronym)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.Property(x => x.BirthDate)
                .IsRequired();

            builder.Property(x => x.Email)
                .HasMaxLength(100)
                .IsRequired(false);

            builder.Property(x => x.PhoneNumber)
                .HasMaxLength(12)
                .IsRequired(false);

            builder.Property(x => x.IsActive)
                .IsRequired();
        }
    }
}
