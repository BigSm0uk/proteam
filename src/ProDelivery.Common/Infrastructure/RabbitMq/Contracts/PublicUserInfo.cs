﻿namespace ProDelivery.Common.Infrastructure.RabbitMq.Contracts
{
    // TODO: хранить контракты в отдельной библиотеке 
    public record PublicUserInfo
    {
        public Guid UserId { get; init; } 
        public string? FirstName { get; init; }
        public string? LastName { get; init; }
        public string? Patronym { get; init; }
        public string? Email { get; init; }
        public string? PhoneNumber { get; init; }
    }
}
