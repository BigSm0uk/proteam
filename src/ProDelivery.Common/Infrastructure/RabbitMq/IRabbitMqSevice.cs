﻿namespace ProDelivery.Common.Infrastructure.RabbitMq

{
    public interface IRabbitMqSevice
    {
        void SendMessage(object obj);
        void SendMessage(string message);
    }
}