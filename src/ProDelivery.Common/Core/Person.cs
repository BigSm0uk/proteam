﻿namespace ProDelivery.Common.Core
{
    public abstract class Person : BaseEntity
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string? Patronym { get; set; }

        /// <summary>
        /// Дата рождения пользователя
        /// </summary>
        public DateTimeOffset BirthDate { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Пользователь активен
        /// </summary>
        public bool IsActive { get; set; }
    }
}
