﻿namespace ProDelivery.Common.Core.Abstractions
{
    public interface ICurrentUserService
    {
        Guid UserId { get; }
    }
}
