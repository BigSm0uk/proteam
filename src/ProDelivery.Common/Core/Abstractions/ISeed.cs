﻿using Microsoft.EntityFrameworkCore;

namespace ProDelivery.Common.Core.Abstractions
{
    public interface ISeed<TContext> where TContext : DbContext
    {
        void StartSeeding();
    }
}
