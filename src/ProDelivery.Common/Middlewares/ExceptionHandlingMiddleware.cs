﻿using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProDelivery.Common.Exceptions;

namespace ProDelivery.Common.Middlewares;

public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(
        HttpContext context,
        ILogger<ExceptionHandlingMiddleware> logger,
        IWebHostEnvironment webHostEnvironment)
    {
        context.Request.EnableBuffering();
        try
        {
            await _next(context);
        }
        catch (Exception exception)
        {
            await HandleExceptionAsync(context, exception, logger, webHostEnvironment);
        }
    }

    private static Task HandleExceptionAsync(
        HttpContext context,
        Exception exception,
        ILogger<ExceptionHandlingMiddleware> logger,
        IWebHostEnvironment webHostEnvironment)
    {
        logger.LogError(exception, exception.Message);

        context.Response.ContentType = "application/json";
        context.Response.StatusCode = (int)(exception switch
        {
            NotFoundException => HttpStatusCode.NotFound,
            _ => HttpStatusCode.InternalServerError
        });

        var errorMessageDetails = webHostEnvironment.IsProduction()
            ? "Ошибка, пожалуйста, обратитесь к разработчику этого сервиса"
            : exception.Message;

        var result = JsonConvert.SerializeObject(new { error = errorMessageDetails });
        return context.Response.WriteAsync(result);
    }
}

public static class HttpStatusCodeExceptionMiddlewareExtensions
{
    public static void UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
    {
        builder.UseMiddleware<ExceptionHandlingMiddleware>();
    }
}