﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace ProDelivery.Common.Middlewares;

public class CachingMiddleware //TODO: Придумать/найти алгоритм по инвалидации кэша
{
    private readonly RequestDelegate _next;
    private readonly HashSet<string> _cacheKeys = new();

    public CachingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(
        HttpContext context,
        IDistributedCache distributedCache,
        ILogger<CachingMiddleware> logger)
    {
        // по-умолчанию за ключ принимаем id
        var key = context.Request.Query["id"].ToString();

        // В итоге у нас сумасшедщий кэш, который заполняется и очищается как попало 
        if (context.Request.Method != "GET")
        {
            // если меняются данные в базе, то на всякий случай очищаем весь кэш
            foreach (var k in _cacheKeys)
                await distributedCache.RemoveAsync(k);
            _cacheKeys.Clear();

            // если есть id, то удаляем связанные с ним данные 
            if (!key.IsNullOrEmpty())
                await distributedCache.RemoveAsync(key);

            await _next(context);
        }
        else
        {
            if (key.IsNullOrEmpty())
            {
                // если нет id, то ключ - путь до действия в контроллере
                key = context.Request.RouteValues["action"]?.ToString();
                // сохраняем ключ для использования при очистке кэша
                _cacheKeys.Add(key!);
            }
            context.Request.EnableBuffering();
            var responseStream = context.Response.Body;
            var cache = await distributedCache.GetAsync(key!); //пробуем найти данные по ключу
            if (cache != null)
            {
                logger.LogInformation("Берем данные из кэша");
                context.Response.Body = responseStream;
                context.Response.Headers.Append("Content-Type", "application/json; chatrset=utf-8");
                context.Response.Headers.Append("Source", "Cache"); // маркер, чтобы понять что данные из кэша 
                await context.Response.Body.WriteAsync(cache);
            }
            else
            {
                logger.LogInformation("Кладем данные в кэш");
                await using var ms = new MemoryStream();
                context.Response.Body = ms;
                await _next(context);// запускаем пайплайн дальше
                await distributedCache.SetAsync(key, ms.ToArray(), new DistributedCacheEntryOptions//кладем данные по ключу
                {
                    SlidingExpiration = TimeSpan.FromSeconds(10) //Настраиваем время жизни кэша
                });
                context.Response.Body = responseStream;
                context.Response.Headers.Append("Source", "Database"); // маркер, чтобы понять что данные из бд 
                await context.Response.Body.WriteAsync(ms.ToArray());
            }
        }
    }
}

public static class HttpCachingMiddlewareExtensions
{
    public static void UseHttpCachingMiddleware(this IApplicationBuilder builder)
    {
        builder.UseMiddleware<CachingMiddleware>();
    }
}