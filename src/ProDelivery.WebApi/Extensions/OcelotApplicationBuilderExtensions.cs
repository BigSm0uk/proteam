﻿using Ocelot.Middleware;

namespace ProDelivery.ApiGateway.Extensions
{
    public static class OcelotApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseOcelotApiGateway(this IApplicationBuilder app)
        {
            //app.UsePathBase("/gateway");
            app.UseSwaggerForOcelotUI();
            //app.UseSwaggerForOcelotUI(opt =>
            //{
            //    opt.DownstreamSwaggerEndPointBasePath = "/gateway/swagger/docs";
            //    opt.PathToSwaggerGenerator = "/swagger/docs";
            //});
            app.UseOcelot();
            return app;
        }
    }
}
