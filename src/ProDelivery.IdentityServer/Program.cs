﻿using IdentityServer4;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.Common.Extensions;
using ProDelivery.IdentityServer;
using ProDelivery.IdentityServer.Data;
using ProDelivery.IdentityServer.Models;
using ProDelivery.IdentityServer.Services;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Reflection;

#region Logger configuring

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
    .MinimumLevel.Override("System", LogEventLevel.Warning)
    .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
    .Enrich.FromLogContext()
    // uncomment to write to Azure diagnostics stream
    //.WriteTo.File(
    //    @"D:\home\LogFiles\Application\identityserver.txt",
    //    fileSizeLimitBytes: 1_000_000,
    //    rollOnFileSizeLimit: true,
    //    shared: true,
    //    flushToDiskInterval: TimeSpan.FromSeconds(1))
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Code)
    .WriteTo.File("IdentityServerLog.log")
    .CreateLogger();
Log.Information($"\n\n === START OF A NEW APPLICATION LOG === \n");

#endregion

try
{
    #region Base

    var appBuilder = WebApplication.CreateBuilder();
    var services = appBuilder.Services;
    var configuration = appBuilder.Configuration;
    services.AddControllersWithViews();
    services.AddDatabaseDeveloperPageExceptionFilter();

    #endregion

    #region Tools

    services.AddLogging(c => { c.ClearProviders(); c.AddSerilog(); });
    services.AddAutoMapper(Assembly.GetExecutingAssembly());
    services.AddMediatR(c => c.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

    #endregion

    #region Data

    services.AddDbContext<ApplicationDbContext>(options =>
    {
        var connectionString = configuration.GetConnectionString("PostgresConnection");
        if (connectionString == null)
            options.UseSqlite(configuration.GetConnectionString("DefaultConnection"));
        else
            options.UseNpgsql(connectionString);
    });

    services.AddScoped<ApplicationDbContextSeed>();
    services.AddSingleton(DataFactory.ApplicationUsers);

    #endregion

    #region MassTransit

    //services.AddMassTransit(c =>
    //{
    //    c.UsingRabbitMq((context, config) =>
    //    {
    //        // For testing:
    //        // docker run -p 15672:15672 -p 5672:5672 -d rabbitmq:3-management
    //        config.Host(appBuilder.Configuration["RABBITMQ_HOST"] ?? "localhost", "/", h =>
    //        {
    //            h.Username("guest");
    //            h.Password("guest");
    //        });
    //        config.ConfigureEndpoints(context);
    //    });
    //});

    #endregion

    #region Identity

    services.AddIdentity<ApplicationUser, IdentityRole>(options =>
    {
        options.Password.RequiredLength = 4;
        options.Password.RequireDigit = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireUppercase = false;
    })
        .AddEntityFrameworkStores<ApplicationDbContext>()
        .AddDefaultTokenProviders();

    var identityBuilder = services.AddIdentityServer(options =>
    {
        options.Events.RaiseErrorEvents = true;
        options.Events.RaiseInformationEvents = true;
        options.Events.RaiseFailureEvents = true;
        options.Events.RaiseSuccessEvents = true;

        // see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
        options.EmitStaticAudienceClaim = true;

        // При запуске из докера явно указываем имя сервера для ключа iss в токене, 
        // иначе оно будет взят localhost из запроса.
        var iss = appBuilder.Configuration["IDENTITY_SERVER_URL"];
        if (!string.IsNullOrEmpty(iss))
            options.IssuerUri = iss;

        options.Cors.CorsPolicyName = "AllowAll";
        options.Cors.CorsPaths.Add("/account/register");
    })
        .AddInMemoryIdentityResources(Config.IdentityResources)
        .AddInMemoryApiScopes(Config.ApiScopes)
        .AddInMemoryClients(Config.Clients)
        .AddInMemoryApiResources(Config.ApiResources)
        .AddAspNetIdentity<ApplicationUser>()
        .AddProfileService<IdentityProfileService>()
        // not recommended for production - you need to store your key material somewhere secure
        .AddDeveloperSigningCredential();

    services.ConfigureApplicationCookie(config =>
    {
        config.Cookie.Name = "ProDelivery.Identity.Cookie";
        //config.LoginPath = "/Auth/Login";
        //config.LogoutPath = "/Auth/Logout";
    });

    services.AddCors(cors
        => cors.AddPolicy("AllowAll", policy =>
        {
            policy.AllowAnyOrigin();
            policy.AllowAnyHeader();
            policy.AllowAnyMethod();
            policy.SetIsOriginAllowedToAllowWildcardSubdomains();
        }));

    #endregion

    #region Google
    services.AddAuthentication()
        .AddGoogle(options =>
        {
            options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

            // register your IdentityServer with Google at https://console.developers.google.com
            // enable the Google+ API
            // set the redirect URI to https://localhost:5001/signin-google
            options.ClientId = "copy client ID from Google here";
            options.ClientSecret = "copy client secret from Google here";
        });
    #endregion

    #region Pipeline

    var app = appBuilder.Build();


    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        app.UseMigrationsEndPoint();

        app.InitializeDatabase<ApplicationDbContext>();
        app.AddApplicationUsers();
    }

    app.UseStaticFiles();

    app.UseRouting();
    app.UseCors("AllowAll");
    app.UseHttpsRedirection();
    app.UseIdentityServer();
    app.UseAuthorization();
    app.MapDefaultControllerRoute();

    Log.Information("Starting host...");
    app.Run();

    #endregion
}

#region Catch

catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly.");
}
finally
{
    Log.CloseAndFlush();
}

#endregion

public partial class Program { }