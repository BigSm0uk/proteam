﻿using ProDelivery.IdentityServer.Data;

namespace ProDelivery.IdentityServer
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder AddApplicationUsers(this IApplicationBuilder builder)
        {
            using var scope = builder.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContextSeed>();
            context.SeedData();

            return builder;
        }
    }
}
