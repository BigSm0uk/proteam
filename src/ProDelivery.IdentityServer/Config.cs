﻿using IdentityServer4;
using IdentityServer4.Models;

namespace ProDelivery.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                //new IdentityResource("roles", new[] { "role" })
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("customerApi", "Customer API"),
                new ApiScope("courierApi", "Courier API"),
                new ApiScope("orderApi", "Order API")
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
        {
            new ApiResource("customerApi"),
            new ApiResource("courierApi"),
            new ApiResource("orderApi"),
            new ApiResource("orderingApi"),
            new ApiResource("shippingApi"),
        };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "customer_service",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("customer_service_secret".Sha256()) },
                    AllowedScopes = { "customerApi" }
                },
                new Client
                {
                    ClientId = "mvc",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,

                    // where to redirect to after login
                    RedirectUris = { "https://localhost:7025/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:7025/signout-callback-oidc" },

                    AllowOfflineAccess = true,

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "customerApi",
                        "roles"
                    },
                },
                new Client
                {
                    ClientId = "console",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedCorsOrigins =
                    { 
                        // Courier Api
                        "http://localhost:5010", 
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess = true,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "courierApi",
                        "customerApi",
                        "orderApi",
                        //"roles"
                    },
                    AllowAccessTokensViaBrowser = true,
                    
                },
                new Client
                {
                    ClientId = "swagger",
                    RequireClientSecret = false,
                    
                    AllowedCorsOrigins = 
                    { 
                        // Ordering Api
                        "http://localhost:5051",
                        // Shipping Api
                        "http://localhost:5052",

                        // Courier Api
                        "http://localhost:5010",
                        "https://localhost:7218",

                        // Customer Api
                        "http://localhost:5020",
                        "https://localhost:7298",

                        // Order Api
                        "http://localhost:5030",
                        "https://localhost:7110",

                        // Api Gateway
                        "http://localhost:5050",
                        "http://localhost:5000",
                        "https://localhost:7300",
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "courierApi",
                        "customerApi",
                        "orderApi",
                    },
                },
                new Client
                {
                    ClientId = "react",
                    RequireClientSecret = false,
                    
                    AllowedCorsOrigins = 
                    { 
                        // React Web Client
                        "http://localhost:5173",
                        "http://localhost:5174",
                        "http://localhost:5175",
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "courierApi",
                        "customerApi",
                        "orderApi",
                    },
                }
            };
    }
}