﻿namespace ProDelivery.IdentityServer.Models
{
    public static class ApplicationRoles
    {
        public static string Customer = "Customer";
        public static string Courier = "Courier";
        public static string Admin = "Admin";
    }
}
