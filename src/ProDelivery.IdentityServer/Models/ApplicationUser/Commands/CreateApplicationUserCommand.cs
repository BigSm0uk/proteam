﻿using MediatR;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProDelivery.IdentityServer.Models
{
    public class CreateApplicationUserCommand : IRequest<Guid>
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string ReturnUrl { get; set; }

        [Required]
        public string Role { get; set; }

    }
}
