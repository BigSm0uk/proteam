﻿using AutoMapper;
using IdentityModel;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Identity;
using ProDelivery.Common.Infrastructure.RabbitMq.Contracts;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace ProDelivery.IdentityServer.Models
{
    public class CreateApplicationUserCommandHandler
        : IRequestHandler<CreateApplicationUserCommand, Guid>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly IBus _bus;

        public CreateApplicationUserCommandHandler(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IMapper mapper )
            //IBus bus)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            //_bus = bus;
        }

        public async Task<Guid> Handle(CreateApplicationUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<ApplicationUser>(request);

            var result = await _userManager.CreateAsync(user, request.Password);

            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }

            result = await _userManager.AddClaimsAsync(user, new Claim[]
            {
                    new Claim(JwtClaimTypes.Role, user.Role)
            });

            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }

            _ = PublishNewUserInfoAsync(user);
                    return Guid.Parse(user.Id);
        }

        private async Task PublishNewUserInfoAsync(ApplicationUser user)
        {
            var userInfo = new PublicUserInfo
            {
                UserId = Guid.Parse(user.Id),
                FirstName = user.FirstName,
                LastName = user.LastName,
                Patronym = user.Patronym,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
            };
            //await _bus.Publish(userInfo);
        }
    }
}
