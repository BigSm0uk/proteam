﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using MvcClient.Models;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace MvcClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        
        public HomeController(ILogger<HomeController> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> CallApi()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var content = await client.GetStringAsync("https://localhost:7298/api/v1/Customer");
            //var content = await client.GetStringAsync("https://localhost:7300/customerservice/v1/Customer");

            ViewBag.Json = content;
            return View("json");
        }

        public async Task<IActionResult> GetToken()
        {

            #region не канает
            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://localhost:6001/");
            ////client.DefaultRequestHeaders
            ////      .Accept
            ////      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "connect/token");
            //request.Content = new StringContent("client_id=customer_service&client_secret=customer_service_secret&grant_type=client_credentials&scope=customerApi",
            //                                    Encoding.UTF8,
            //                                    "application/x-www-form-urlencoded");//CONTENT-TYPE header
            //var result = await client.SendAsync(request); 
            #endregion

            var authClient = _httpClientFactory.CreateClient();
            var discoveryDocumetn = await authClient.GetDiscoveryDocumentAsync("https://localhost:6001");

            var tokenResponse = await authClient.RequestPasswordTokenAsync(
                new PasswordTokenRequest
                {
                    Address = discoveryDocumetn.TokenEndpoint, // /connect/token
                    ClientId = "test",
                    ClientSecret = "secret",
                    Scope = "customerApi roles",
                    UserName = "Alice",
                    Password = "Pass123$"
                });

            var token = tokenResponse.AccessToken;
            return Ok(token);

            //var tokenResponse = await authClient.RequestAuthorizationCodeTokenAsync(
            //    new AuthorizationCodeTokenRequest
            //    {
            //        Address = discoveryDocumetn.TokenEndpoint, // /connect/token
            //        ClientId = "mvc",
            //        ClientSecret = "secret",
            //        GrantType = "authorization_code",
            //        Code = "code", // непонятный чё за код 
            //        RedirectUri = "https://localhost:7025/signin-oidc"
            //    });

        }
    }
}
