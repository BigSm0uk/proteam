using IdentityServer4.AccessTokenValidation;
using MassTransit;
using MediatR;
using Microsoft.OpenApi.Models;
using ProDelivery.Common.Core.Abstractions;
using ProDelivery.Common.Extensions;
using ProDelivery.Common.Middlewares;
using ProDelivery.CustomerService.Core.IRepositories;
using ProDelivery.CustomerService.Infrastructure.Data;
using ProDelivery.CustomerService.Infrastructure.Data.Repositories;
using ProDelivery.CustomerService.Infrastructure.RabbitMq;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using Serilog;
using System.Reflection;
using Microsoft.Extensions.Caching.Redis;
using ProDelivery.Common.Behaviors;
using Microsoft.IdentityModel.Logging;
using System.Reflection.PortableExecutable;
using EasyCaching.Core;

#region Logging

Log.Logger = new LoggerConfiguration()
.MinimumLevel.Debug()
.MinimumLevel.Override("System", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Debug)
.MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Warning)
.Enrich.FromLogContext()
.WriteTo.Console(theme: AnsiConsoleTheme.Literate)
.WriteTo.File("CustomerApiLog.log")
.CreateLogger();
Log.Information($"\n\n === START OF A NEW APPLICATION LOG === \n");

#endregion
try
{
    #region Base

    var builder = WebApplication.CreateBuilder(args);
    builder.Services.AddControllers(options
        => options.SuppressAsyncSuffixInActionNames = true);

    #endregion

    #region Tools

    builder.Services.AddLogging(c =>
    {
        c.ClearProviders();
        c.AddSerilog();
    });
    builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());
    builder.Services.AddMediatR(c => c.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
    builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

    #endregion

    #region MassTransit

    //builder.Services.AddMassTransit(c =>
    //{
    //    c.AddConsumer<PublicUserInfoConsumer>();
    //    c.UsingRabbitMq((context, config) =>
    //    {
    //        // For testing:
    //        // docker run -p 15672:15672 -p 5672:5672 -d rabbitmq:3-management
    //        config.Host(builder.Configuration["RABBITMQ_HOST"] ?? "localhost", "/", h =>
    //            {
    //                h.Username("guest");
    //                h.Password("guest");
    //            });
    //        config.ConfigureEndpoints(context);
    //    });
    //});

    #endregion

    #region Identity

    builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
        options.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
    })
        .AddIdentityServerAuthentication(options =>
        {
            options.ApiName = "customerApi";
            options.Authority = builder.Configuration["IDENTITY_SERVER_URL"] ?? "http://localhost:6001";
            options.RequireHttpsMetadata = false;
            options.LegacyAudienceValidation = true;
        });

    builder.Services.AddAuthorization(opt =>
    {
        opt.AddPolicy("ApiScope", policy =>
        {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("scope", "customerApi");
        });
    });

    builder.Services.AddCors(options =>
        {
            // this defines a CORS policy called "default"
            options.AddPolicy("default", policy =>
                {
                    policy//.WithOrigins("https://localhost:5003")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
                });
        });

    #endregion

    #region Swagger

    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "Customer API", Version = "v1" });
        var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        c.AddSecurityDefinition("oauth2",
            new OpenApiSecurityScheme
            {
                //OpenIdConnectUrl = new Uri("http://localhost:6001/.well-known/openid-configuration"),
                Type = SecuritySchemeType.OAuth2,
                Flows = new OpenApiOAuthFlows
                {
                    Password = new OpenApiOAuthFlow
                    {
                        //AuthorizationUrl = new Uri("http://localhost:6001/connect/authorize"),
                        TokenUrl = new Uri(new Uri("http://localhost:6001"), "/connect/token"),
                        Scopes = new Dictionary<string, string>
                        {
                            {"customerApi", "Customer API"}
                        },
                    }
                },

            });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "oauth2"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }
            });
    });

    #endregion

    #region Data

    builder.Services.AddDbContext<CustomerServiceContext>(options =>
    {
        var connectionString = builder.Configuration.GetConnectionString("PostgresConnection");
        if (connectionString == null)
            options.UseSqlite(builder.Configuration.GetConnectionString("SqliteConnection"));
        else
        {
            options.UseNpgsql(connectionString);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
    });
    builder.Services.AddScoped<ICustomerRepository, CustomerRepository>();
    builder.Services.AddScoped<ISeed<CustomerServiceContext>, CustomerServiceContextSeed>();
    builder.Services.AddMemoryCache();
    builder.Services.AddDistributedMemoryCache();
    //builder.Services.AddDistributedRedisCache(options =>
    //{
    //    options.Configuration = "redis";
    //    options.InstanceName = "Caching";
    //});

    #endregion

    #region Pipeline

    var app = builder.Build();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("v1/swagger.json", "Customer API V1");
            c.OAuthClientId("console");
            c.OAuthClientSecret("secret");
        });
        // Enable to show PII (personally identifiable information) in exceptions
        IdentityModelEventSource.ShowPII = true;

        app.InitializeDatabase<CustomerServiceContext>(context =>
        {
            context.AddRange(DataFactory.PreconfiguredCustomers);
            context.SaveChanges();
        });
    }
    app.UseHttpStatusCodeExceptionMiddleware();

    app.UseHttpCachingMiddleware();

    app.UseHttpsRedirection();

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();
        //.RequireAuthorization("ApiScope");

    app.UseCors("default");

    app.Run();

    #endregion
}
#region Catch

catch (Exception ex)
{
    Log.Fatal(ex, "Fatal application error.");
}
finally
{
    Log.CloseAndFlush();
}

#endregion

public partial class Program { }