﻿using AutoMapper;
using MassTransit;
using MediatR;
using ProDelivery.Common.Infrastructure.RabbitMq.Contracts;
using ProDelivery.CustomerService.Core.Customers.Commands;

namespace ProDelivery.CustomerService.Infrastructure.RabbitMq
{
    public class PublicUserInfoConsumer : IConsumer<PublicUserInfo>
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public PublicUserInfoConsumer(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<PublicUserInfo> context)
        {
            await _mediator.Send(
                _mapper.Map<CreateCustomerCommand>(context.Message),
                context.CancellationToken);
        }
    }
}
