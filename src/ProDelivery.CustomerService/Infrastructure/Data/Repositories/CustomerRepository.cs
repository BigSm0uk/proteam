﻿using ProDelivery.Common.Infrastructure.Data.Repositories;
using ProDelivery.CustomerService.Core.IRepositories;
using ProDelivery.CustomerService.Infrastructure.Data;

namespace ProDelivery.CustomerService.Infrastructure.Data.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(CustomerServiceContext context) : base(context)
        {
        }

        public Task<Guid> CreateCustomer(Customer entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteCustomer(Guid entityId)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Customer> GetActiveCustomers()
            => _commonContext.Set<Customer>().Where(c => c.IsActive == true);

        public async Task<Customer> UpdateCustomer(Customer entity)
        {
            var customer = Update(entity);
            //await SaveChangesAsync();
            return customer;
        }
    }
}

