﻿using ProDelivery.Common.Infrastructure.Data;

namespace ProDelivery.CustomerService.Infrastructure.Data
{
    public class CustomerServiceContextSeed : Seed<CustomerServiceContext>
    {
        public CustomerServiceContextSeed(CustomerServiceContext context,
            ILogger<CustomerServiceContextSeed> logger)
            : base(context, logger) { }

        protected override void OnDatabaseSeeding(SeedBuilder builder)
        {
            builder.ChooseTable(x => x.Customers).AddEntities(DataFactory.PreconfiguredCustomers);
        }
    }

    public class DataFactory
    {
        public static IEnumerable<Customer> PreconfiguredCustomers => new List<Customer>
        {
            new()
            {
                Id = Guid.Parse("13c0af06-8cc4-49d4-a60b-b956e21b0a62"),
                FirstName = "Vasilij",
                LastName = "Pupkin",
                BirthDate = new DateTime(2000, 01, 22),
                Email = "somemail@email.li",
                PhoneNumber = "+100500",
                IsActive = true,
                Rating = 5.0f,
                SignUpDate = new DateTimeOffset()
            },
            new()
            {
                Id = Guid.Parse("446d92bd-d6e7-4b76-9b4d-e4e5b51c7a2d"),
                FirstName = "Alexandr",
                LastName = "XXX",
                BirthDate = new DateTime(1970, 01, 01),
                Email = "ale_xxx@cmail.com",
                PhoneNumber = "+89991115432",
                IsActive = true,
                Rating = 4.99f,
                SignUpDate = new DateTimeOffset()
            }
        };
    }
}
