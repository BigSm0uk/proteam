﻿using ProDelivery.Common.Core;
using ProDelivery.Common.Infrastructure.Data.Config;
using ProDelivery.CustomerService.Infrastructure.Data.Config;

namespace ProDelivery.CustomerService.Infrastructure.Data
{
    public class CustomerServiceContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public CustomerServiceContext(DbContextOptions<CustomerServiceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomerConfiguration());
            builder.ApplyConfiguration(new PersonConfiguration());
            builder.Entity<Person>().ToTable("PersonalData");
            builder.Entity<Customer>().ToTable("Customers");
            base.OnModelCreating(builder);
        }
    }
}
