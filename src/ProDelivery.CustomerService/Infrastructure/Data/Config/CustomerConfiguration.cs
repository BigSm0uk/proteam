﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.CustomerService.Core;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProDelivery.CustomerService.Infrastructure.Data.Config
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(x => x.Rating)
                .IsRequired(false);
            builder.Property(x => x.SignUpDate)
                .IsRequired();
        }
    }
}
