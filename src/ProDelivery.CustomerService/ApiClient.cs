﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.CustomerService.Presentation.Models;
using System.Diagnostics;

namespace ProDelivery.CustomerService
{
    public class ApiClient : IApiClient
    {
        public async Task<IActionResult> CallApi(string url, JsonContent content)
        {
            using (var client = new HttpClient())
            {
                var result = await client.PostAsync(url, content);

                if (result.IsSuccessStatusCode)
                {
                    Debug.WriteLine("Register is success");
                    return new OkResult();
                }
                return new BadRequestResult();
            }
        }

    }
}
