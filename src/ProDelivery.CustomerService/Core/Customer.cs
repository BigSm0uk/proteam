﻿using ProDelivery.Common.Core;

namespace ProDelivery.CustomerService.Core
{
    public class Customer : Person
    {
        /// <summary>
        /// Рейтинг заказчика
        /// </summary>
        public float? Rating { get; set; }

        /// <summary>
        /// Дата регистрации заказчика 
        /// </summary>
        public DateTimeOffset SignUpDate { get; set; }
    }
}
