﻿using ProDelivery.Common.Core.IRepositories;

namespace ProDelivery.CustomerService.Core.IRepositories
{
    public interface ICustomerRepository: IEfRepository<Customer>
    {
        public Task<Guid> CreateCustomer(Customer entity);

        public Task<Customer> UpdateCustomer(Customer entity);

        public Task DeleteCustomer(Guid entityId);
    }
}
