﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using ProDelivery.Common.Exceptions;
using ProDelivery.CustomerService.Core.IRepositories;
using System.Text.Json;

namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class GetCustomerQueryHandler : IRequestHandler<GetCustomerQuery, CustomerDTO> // Как вариант можно переписать каждый Handler для работы с кэшом
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public GetCustomerQueryHandler(ICustomerRepository repository, IMapper mapper, IDistributedCache distributedCache)
            => (_repository, _mapper, _distributedCache) = (repository, mapper, distributedCache);

        public async Task<CustomerDTO> Handle(GetCustomerQuery request, CancellationToken ct)
        {
            Customer? customer = null;

            var customerString = await _distributedCache.GetStringAsync(request.Id.ToString());

            if(customerString != null) customer = JsonSerializer.Deserialize<Customer>(customerString);

            if(customer == null)
            {
                customer = await _repository.GetByIdAsync(request.Id);
                if (customer == null || customer.Id != request.Id) throw new NotFoundException(nameof(Customer), request.Id);
                customerString = JsonSerializer.Serialize(customer);
                await _distributedCache.SetStringAsync(customer.Id.ToString(), customerString, new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(10)
                });
            }
            
            return _mapper.Map<CustomerDTO>(_repository.Update(customer));            
        }
    }
}
