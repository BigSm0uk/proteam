﻿using MediatR;

namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class GetCustomerQuery : IRequest<CustomerDTO>
    {
        public Guid Id { get; set; }
    }
}
