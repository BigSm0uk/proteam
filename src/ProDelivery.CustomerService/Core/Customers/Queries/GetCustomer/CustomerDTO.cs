﻿namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class CustomerDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string? Patronym { get; set; }

        public DateTimeOffset BirthDate { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsActive { get; set; }

        public string Login { get; set; }

        public float? Rating { get; set; }

        public DateTimeOffset SignUpDate { get; set; }
    }
}
