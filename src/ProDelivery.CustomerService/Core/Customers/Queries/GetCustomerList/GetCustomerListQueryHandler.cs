﻿using MediatR;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ProDelivery.CustomerService.Core.IRepositories;

namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class GetCustomerListQueryHandler
        : IRequestHandler<GetCustomerListQuery, CustomerListDTO>
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;

        public GetCustomerListQueryHandler(ICustomerRepository repository, IMapper mapper)
            => (_repository, _mapper) = (repository, mapper);

        public async Task<CustomerListDTO> Handle(GetCustomerListQuery request, CancellationToken ct)
        {
            var customers = _repository
                .GetAll()
                .ProjectTo<CustomerListItemDTO>(_mapper.ConfigurationProvider)
                .ToList();
            
            return new CustomerListDTO { Customers = customers };
        }
    }
}
