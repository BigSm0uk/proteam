﻿using MediatR;

namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class GetCustomerListQuery
        : IRequest<CustomerListDTO>
    {

    }
}
