﻿namespace ProDelivery.CustomerService.Core.Customers.Queries
{
    public class CustomerListDTO
    {
        public IList<CustomerListItemDTO> Customers { get; set; }
    }
}
