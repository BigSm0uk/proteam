﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProDelivery.Common.Exceptions;
using ProDelivery.CustomerService.Core.Customers.Queries;
using ProDelivery.CustomerService.Core.IRepositories;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class UpdateCustomerCommandHandler
        : IRequestHandler<UpdateCustomerCommand, CustomerDTO>
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;

        public UpdateCustomerCommandHandler(ICustomerRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        // TODO: разобраться почему тесты не проходят 
        public async Task<CustomerDTO> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            //var customer = await _repository.GetById(request.Id);

            //if (customer == null || customer.Id != request.Id) 
            //    throw new NotFoundException(nameof(Customer), request.Id);            

            var customer = _mapper.Map<Customer>(request);

            customer = await _repository.UpdateCustomer(customer);

            var response = _mapper.Map<CustomerDTO>(customer);

            return response;
        }
    }
}
