﻿using MediatR;
using ProDelivery.CustomerService.Core.Customers.Queries;
using System.Text.Json.Serialization;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class UpdateCustomerCommand : IRequest<CustomerDTO>
    {
        [JsonIgnore]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronym { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
