﻿using AutoMapper;
using MediatR;
using ProDelivery.CustomerService.Core.IRepositories;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class CreateCustomerCommandHandler
        : IRequestHandler<CreateCustomerCommand, Guid>
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;
        private Customer customer;

        public CreateCustomerCommandHandler(ICustomerRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        public async Task<Guid> Handle(CreateCustomerCommand request, CancellationToken ct)
        {
            customer = _mapper.Map<Customer>(request);

            if (customer == null)
                throw new NullReferenceException($"{nameof(customer)} is null!");

            // TODO: инкапсулировать поведение внутрь модели 
            customer.SignUpDate = DateTimeOffset.UtcNow;

                 var response =   _repository.Create(customer);
                    await _repository.SaveChangesAsync();
            return response;
        }
    }
}
