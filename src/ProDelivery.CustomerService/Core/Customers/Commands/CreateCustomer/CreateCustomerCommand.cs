﻿using MediatR;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class CreateCustomerCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronym { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTimeOffset BirthDate { get; set; }
    }
}
