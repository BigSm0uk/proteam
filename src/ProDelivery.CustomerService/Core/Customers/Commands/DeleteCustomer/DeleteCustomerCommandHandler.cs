﻿using AutoMapper;
using MediatR;
using ProDelivery.CustomerService.Core.IRepositories;
using ProDelivery.CustomerService.Presentation.Models;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, Unit>
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;

        public DeleteCustomerCommandHandler(ICustomerRepository repository, IMapper mapper)
        => (_repository, _mapper) = (repository, mapper);

        public async Task<Unit> Handle(DeleteCustomerCommand request, CancellationToken ct)
        {
            _repository.Delete(request.Id);
            await _repository.SaveChangesAsync();
            return Unit.Value;
        }
    }
}
