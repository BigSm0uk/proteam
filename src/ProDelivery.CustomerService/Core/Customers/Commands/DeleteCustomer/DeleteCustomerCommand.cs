﻿using MediatR;

namespace ProDelivery.CustomerService.Core.Customers.Commands
{
    public class DeleteCustomerCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }
}
