﻿using Microsoft.AspNetCore.Mvc;

namespace ProDelivery.CustomerService
{
    public interface IApiClient
    {
        public Task<IActionResult>CallApi(string url, JsonContent content);
    }
}
