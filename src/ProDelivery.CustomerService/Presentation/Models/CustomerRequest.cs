﻿namespace ProDelivery.CustomerService.Presentation.Models
{
    public class CustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Patronym { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
