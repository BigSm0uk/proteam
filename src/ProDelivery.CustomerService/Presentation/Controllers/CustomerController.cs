﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.CustomerService.Core.Customers.Queries;

namespace ProDelivery.CustomerService.Presentation.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
            => _mediator = mediator;

        /// <summary>
        /// Получить данные пользователя по его Id
        /// </summary>
        /// <param name="id">id заказчика, например 
        /// <example>13c0af06-8cc4-49d4-a60b-b956e21b0a62</example></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerDTO>> GetCustomerById([FromQuery] Guid id)
            => Ok(await _mediator.Send(new GetCustomerQuery { Id = id }));

        /// <summary>
        /// Получить список пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerListDTO>> GetAllAsync()
        => await _mediator
            .Send(new GetCustomerListQuery());

        /// <summary>
        /// Получить ошибку, для теста middleware
        /// </summary>
        /// <returns>exception</returns>
        [HttpGet("error")]
        public async Task<ActionResult<CustomerListDTO>> GetError()
            => throw new Exception("Some error");

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="request">Данные для создания нового пользователя</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateCustomerCommand request)
        => Ok(await _mediator.Send(request));

        /// <summary>
        /// Редактировать пользователя
        /// </summary>
        /// <param name="id">id заказчика, например 
        /// <example>13c0af06-8cc4-49d4-a60b-b956e21b0a62</example></param>
        /// <param name="request">Данные для обновления пользователя</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<CustomerDTO>> UpdateCustomerAsync(
            [FromQuery] Guid id, UpdateCustomerCommand request)
        {
            request.Id = id;
            return Ok(await _mediator.Send(request));
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id">id заказчика, например 
        /// <example>13c0af06-8cc4-49d4-a60b-b956e21b0a62</example></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteCustomerAsync([FromQuery] Guid id)
            => Ok(await _mediator.Send(new DeleteCustomerCommand { Id = id }));
    }
}
