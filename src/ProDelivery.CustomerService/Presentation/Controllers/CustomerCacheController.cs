﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;


namespace ProDelivery.CustomerService.Presentation.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomerCacheController : ControllerBase //Тут локальный кэш - не будет доступен между микросервисами
    {
        private readonly IMemoryCache _memoryCache;
        private readonly string Key = "CacheKeyName";

        public CustomerCacheController(IMemoryCache memoryCache)
            =>  _memoryCache = memoryCache;

        [HttpPut("[action]")]
        public IActionResult SetCache(string? data , int seconds = 10)
        {
            data ??= DateTime.Now.ToString("F");
            _memoryCache.Set(Key, data, DateTime.Now.AddSeconds(seconds));
            return Ok($"Data in cache for {seconds}");
        }
        [HttpGet("[action]")]
        public IActionResult GetCache()
        {
            var IsExist = !_memoryCache.TryGetValue(Key, out string? value);
            var message = IsExist ? 
                "No data in cache" :
                $"Data in cache: {value}";
            return Ok(message);
        }
    }
}

