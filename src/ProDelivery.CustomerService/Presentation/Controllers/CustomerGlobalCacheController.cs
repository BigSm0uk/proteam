﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace ProDelivery.CustomerService.Presentation.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomerGlobalCacheController : ControllerBase // Тут глобальный кэш - виден между микросервисами
    {
        private readonly IDistributedCache _distributedCache;
        private readonly string Key = "CacheKeyName";

        public CustomerGlobalCacheController(IDistributedCache distributedCache)
            => _distributedCache = distributedCache;

        [HttpPut("[action]")]
        public async Task<IActionResult> SetCache(string? data , int seconds = 10)
        {
            data ??= DateTime.Now.ToString("F");
            await _distributedCache.SetStringAsync(Key, data, new DistributedCacheEntryOptions()
            {
                SlidingExpiration= TimeSpan.FromSeconds(seconds),
            });
            return Ok($"Data in cache for {seconds}");
        }
        [HttpGet("[action]")]
        public async Task<IActionResult> GetCache()
        {
            var value = await _distributedCache.GetStringAsync(Key, HttpContext.RequestAborted);
            var message = value is not null ? $"Data in cache: {value}" : "No data in cache";
            return Ok(message);
        }
    }
}


