﻿using AutoMapper;
using ProDelivery.Common.Infrastructure.RabbitMq.Contracts;
using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.CustomerService.Core.Customers.Queries;
using ProDelivery.CustomerService.Presentation.Models;

namespace ProDelivery.CustomerService.MappingProfiles
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, CustomerListItemDTO>();
            CreateMap<Customer, CustomerDTO>();
            CreateMap<CreateCustomerCommand, Customer>();
            CreateMap<Customer, CustomerAuthRegistrRequest>()
                //.ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Login))
                //.ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password))
                //.ForMember(dest => dest.ConfirmPassword, opt => opt.MapFrom(src => src.Password))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))                
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => "customer"));
            CreateMap<CreateCustomerCommand, Customer>();
            CreateMap<UpdateCustomerCommand, Customer>().ReverseMap();

            // RabbitMq
            CreateMap<PublicUserInfo, CreateCustomerCommand>();
        }
    }
}
