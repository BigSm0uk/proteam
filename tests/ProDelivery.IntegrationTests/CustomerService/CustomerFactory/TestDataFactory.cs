﻿using ProDelivery.CustomerService.Core;

namespace ProDelivery.IntegrationTests.CustomerService.CustomerFactory;

internal class TestDataFactory 
{
    public static IEnumerable<Customer> Customers => new List<Customer>
        {
            new()
            {
                Id = Guid.Parse("13c0af06-8cc4-49d4-a60b-b956e21b0a10"),
                FirstName = "TestFirstName1",
                LastName = "TestLastName1",
                BirthDate = new DateTime(2000, 01, 01),
                Email = "Test@Email.1",
                PhoneNumber = "000001",
                IsActive = false,
                Rating = 1.0f,
            },
            new()
            {
                Id = Guid.Parse("13c0af06-8cc4-49d4-a60b-b956e21b0a20"),
                FirstName = "TestFirstName2",
                LastName = "TestLastName2",
                BirthDate = new DateTime(2000, 01, 02),
                Email = "Test@Email.2",
                PhoneNumber = "000002",
                IsActive = true,
                Rating = 2.0f,
            },
        };
}
