﻿using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProDelivery.Common.Extensions;
using ProDelivery.CustomerService.Infrastructure.Data;
using Xunit;

namespace ProDelivery.IntegrationTests.CustomerService.CustomerFactory;

public  class CustomerApiFactory : WebApplicationFactory<Program>, IAsyncLifetime
{
    private const string _postgresPort = "5432";
    private readonly IContainer _container;

    public CustomerApiFactory()
    {
        Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", Environments.Development);

        // up postgres container before tests
        _container = new ContainerBuilder()
          .WithImage("postgres:14.3-alpine")
          //.WithNetwork("customerApiTests")
          .WithPortBinding(_postgresPort, assignRandomHostPort: true)
          .WithEnvironment("POSTGRES_PASSWORD", "root")
          .WithWaitStrategy(Wait.ForUnixContainer())
          .Build();
    }

    public async Task InitializeAsync() => await _container.StartAsync();
    public new async Task DisposeAsync() => await _container.DisposeAsync();

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        var hostPort = _container.GetMappedPublicPort(_postgresPort).ToString();
        var connection = $"Server=localhost;Database=CustomerApiDb;Username=postgres;Password=root;Port={hostPort}";

        builder.ConfigureTestServices(services =>
        {
            services.RemoveDbContext<CustomerServiceContext>();
            services.AddDbContext<CustomerServiceContext>(options =>
            {
                options.UseNpgsql(connection);
                AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            });
            services.EnsureDbCreated<CustomerServiceContext>(context =>
            {
                context.AddRange(TestDataFactory.Customers);
                context.SaveChanges();
            });
        });
    }
}
