﻿using System.Data.Common;
using System.Net;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using ProDelivery.CustomerService.Core;
using ProDelivery.CustomerService.Infrastructure.Data;
using Xunit;

namespace ProDelivery.IntegrationTests.CustomerService.Controller.Get
{
    public class GetCustomerQueryHandlerTests
    {
        private readonly HttpClient _client;

        #region Конструктор тестов
        public GetCustomerQueryHandlerTests()
        {
            var webHost = new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var dbContextDescriptor = services.SingleOrDefault(
                        d => d.ServiceType ==
                             typeof(DbContextOptions<CustomerServiceContext>));

                    services.Remove(dbContextDescriptor);

                    var dbConnectionDescriptor = services.SingleOrDefault(
                        d => d.ServiceType ==
                             typeof(DbConnection));

                    services.Remove(dbConnectionDescriptor);
                    services.AddSingleton<DbConnection>(container =>
                    {
                        var connection = new NpgsqlConnection("Server=postgres;Database=testDb;Username=admin;Password=root;Port=5432");
                        connection.Open();

                        return connection; 
                    });
                    services.AddDbContext<CustomerServiceContext>((container, options) =>
                    {
                        var connection = container.GetRequiredService<DbConnection>();
                        options.UseNpgsql(connection);
                    });
                });
            });
            var dbContext = webHost.Services.CreateScope().ServiceProvider.GetService<CustomerServiceContext>();  
            dbContext?.AddRange(
                new Customer
                {
                    Id = Guid.Parse("4E998B00-DCD0-4326-9F2F-91FE4A30C32B"),
                    FirstName = "null",
                    LastName = "null",
                    Patronym = "null",
                    BirthDate = DateTimeOffset.Now,
                    Email = "null",
                    PhoneNumber = "null",
                    IsActive = false,
                    Rating = null,
                    SignUpDate = DateTimeOffset.Now
                },
                new Customer
                {
                    Id = Guid.Parse("F26CE421-C595-4FB2-BC35-5EFDDEA495B6"),
                    FirstName = "null2",
                    LastName = "null2",
                    Patronym = "null2",
                    BirthDate = DateTimeOffset.Now,
                    Email = "null2",
                    PhoneNumber = "null2",
                    IsActive = false,
                    Rating = null,
                    SignUpDate = DateTimeOffset.Now
                }
            );
            dbContext!.SaveChangesAsync();
            _client = webHost.CreateClient();
        }

        #endregion
        
        [Fact]
        public async Task Handle_ShouldReturn_Success()
        {
            // Arrange
            
            //Act
            var httpResponseMessage = await _client.GetAsync("api/v1/Customer");
            //Assert
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);
        } 
    }
}