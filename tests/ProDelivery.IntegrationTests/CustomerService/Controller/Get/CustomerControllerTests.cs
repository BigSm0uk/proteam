﻿using FluentAssertions;
using ProDelivery.CustomerService.Core.Customers.Queries;
using ProDelivery.IntegrationTests.CustomerService.CustomerFactory;
using System.Net;
using System.Net.Http.Json;
using Xunit;

namespace ProDelivery.IntegrationTests.CustomerService.Controller.Get;

public class CustomerControllerTests : IClassFixture<CustomerApiFactory>
{
    private readonly HttpClient _client;

    public CustomerControllerTests(CustomerApiFactory factory)
    {
        _client = factory.CreateClient();
    }

    [Fact]
    public async Task GetAll_ShouldReturnSuccess()
    {
        // Arrange

        //Act
        var response = await _client.GetAsync("api/v1/Customer/GetAll");

        //Assert
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadFromJsonAsync<CustomerListDTO>();
        content.Should().NotBeNull();
        content!.Customers.Count.Should().Be(2);
        content!.Customers.First().FirstName.Should().Be("TestFirstName1");

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }
}
