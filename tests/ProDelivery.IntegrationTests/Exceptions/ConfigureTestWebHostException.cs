﻿using System.Runtime.Serialization;

namespace ProDelivery.IntegrationTests.Exceptions
{
    [Serializable]
    internal class ConfigureTestWebHostException : Exception
    {
        public ConfigureTestWebHostException()
        {
        }

        public ConfigureTestWebHostException(string? message) : base(message)
        {
        }

        public ConfigureTestWebHostException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ConfigureTestWebHostException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}