﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.CustomerService.Infrastructure.Data;
using ProDelivery.CustomerService.Presentation.Controllers;
using ProDelivery.CustomerService.Presentation.Models;
using Xunit;
namespace ProDelivery.UnitTests.CustomerService.Controller;

public class CustomerControllerTests : IClassFixture<TestFixture<CustomerServiceContext>>
{
    private readonly CustomerController _controller;

    public CustomerControllerTests(TestFixture<CustomerServiceContext> testFixture)
    {
        var serviceProvider = testFixture.ServiceProvider;
        _controller = new CustomerController
            (serviceProvider.GetRequiredService<IMediator>());
    }

    [Fact]
    public void CreateNewCustomer_Should_Return_NotNull()
    {
        var createCommand = new CreateCustomerCommand
        {
            FirstName = "Иван",
            LastName = "Иванов",
            Patronym = "Иванович",
            BirthDate = DateTimeOffset.Parse("1990-01-01"),
            PhoneNumber = "89507895612",
            Email = "ivanovii@mail.ru",
        };

        //Act
        var result = _controller.CreateCustomerAsync(createCommand);
        var customerDto = _controller.GetAllAsync().Result.Value;
        var customer = customerDto?.Customers.FirstOrDefault(c => c.Email == createCommand.Email);

        //Assert
        Assert.Null(result.Exception);
        Assert.NotNull(customer);
        Assert.True(customer.FirstName == createCommand.FirstName);
        Assert.True(customer.LastName == createCommand.LastName);
        Assert.True(customer.Email == createCommand.Email);
    }
}