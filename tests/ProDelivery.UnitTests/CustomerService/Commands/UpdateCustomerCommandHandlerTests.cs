﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.CustomerService.Core;
using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.CustomerService.Infrastructure.Data;
using ProDelivery.CustomerService.Infrastructure.Data.Repositories;
using ProDelivery.UnitTests.CustomerService.Commands.Builders;
using ProDelivery.UnitTests.CustomerService.CustomerFactory;
using ProDelivery.UnitTests.CustomerService.Directors;
using Xunit;

namespace ProDelivery.UnitTests.CustomerService.Commands;

public class UpdateCustomerCommandHandlerTests 
    : TestCommandBase, IClassFixture<TestFixture<CustomerServiceContext>>
{
    private readonly UpdateCustomerCommandHandler _updateCustomerCommandHandler;
    private readonly IMapper _mapper;

    public UpdateCustomerCommandHandlerTests(TestFixture<CustomerServiceContext> testFixture)
    {
        var serviceProvider = testFixture.ServiceProvider;
        _updateCustomerCommandHandler = new UpdateCustomerCommandHandler(
            new CustomerRepository(_context), serviceProvider.GetRequiredService<IMapper>());
        _mapper = serviceProvider.GetRequiredService<IMapper>();
    }

    [Fact]
    public void Handle_IfCustomerNull_ShouldReturnNotFoundException()
    {
        //Arrange
        var command = new UpdateCustomerCommand();

        //Act
        var result = _updateCustomerCommandHandler.Handle(command, CancellationToken.None);

        //Assert
        result.Exception.Should().NotBeNull();
    }

    [Fact]
    public void Handle_IfBadRequestId_ShouldReturnNotFoundException()
    {
        //Arrange
        var command = new Fixture().Build<UpdateCustomerCommand>().Create();

        //Act
        var result = _updateCustomerCommandHandler.Handle(command, CancellationToken.None);

        //Assert
        result.Exception.Should().NotBeNull();
    }

    [Fact]
    public void UpdateCustomerPropertiesByRequest_IfBadRequest_ShouldReturnNotFoundException()
    {
        //Arrange
        var command = new Fixture().Build<UpdateCustomerCommand>().Create();

        //Act
        var result = _updateCustomerCommandHandler.Handle(command, CancellationToken.None);

        //Assert
        result.Exception.Should().NotBeNull();
    }

    [Fact]
    public void Handle_IfRepositoryUpdate_ShouldReturnSuccess()
    {
        //Arrange
        var builder = new UpdateCustomerCommandBuilder();
        var director = new CustomerUpdateDirector(builder);
        director.WithAllProperty();
        var command = builder.Build();

        //Act
        var customerDto = _updateCustomerCommandHandler.Handle(command, CancellationToken.None).Result;

        //Assert
        var expectedCustomer = _mapper.Map<UpdateCustomerCommand, Customer>(command,
            op => op.AfterMap((src, dest) => dest.SignUpDate = customerDto.SignUpDate));

        var actualCustomer = _context.Customers.FirstOrDefault(x => x.Id == command.Id);

        (actualCustomer?.SignUpDate).Should().NotBe(default); 

        Assert.Equivalent(expectedCustomer, actualCustomer);
    }
}