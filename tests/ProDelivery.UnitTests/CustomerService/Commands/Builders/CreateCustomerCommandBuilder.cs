﻿using ProDelivery.CustomerService.Core.Customers.Commands;
namespace ProDelivery.UnitTests.CustomerService.Commands.Builders;

public class CreateCustomerCommandBuilder
{
    private readonly CreateCustomerCommand _createCustomerCommand = new();

    public CreateCustomerCommand Build()
    {
        return _createCustomerCommand;
    }

    public CreateCustomerCommandBuilder WithFirstName()
    {
        _createCustomerCommand.FirstName = "FirstName";
        return this;
    }

    public CreateCustomerCommandBuilder WithLastName()
    {
        _createCustomerCommand.LastName = "LastName";
        return this;
    }

    public CreateCustomerCommandBuilder WithPatronym()
    {
        _createCustomerCommand.Patronym = "Patronym";
        return this;
    }

    public CreateCustomerCommandBuilder WithEmail()
    {
        _createCustomerCommand.Email = "Email";
        return this;
    }

    public CreateCustomerCommandBuilder WithPhoneNumber()
    {
        _createCustomerCommand.PhoneNumber = "+79588386515";
        return this;
    }

    public CreateCustomerCommandBuilder WithBirthDate()
    {
        _createCustomerCommand.BirthDate = DateTimeOffset.Now;
        return this;
    }
}