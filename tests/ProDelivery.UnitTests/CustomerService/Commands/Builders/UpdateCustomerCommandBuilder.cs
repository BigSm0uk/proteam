﻿using ProDelivery.CustomerService.Core.Customers.Commands;

namespace ProDelivery.UnitTests.CustomerService.Commands.Builders;

public class UpdateCustomerCommandBuilder 
{
    private readonly UpdateCustomerCommand _updateCustomerCommand = new();
    
    public UpdateCustomerCommand Build()
    {
        return _updateCustomerCommand;
    }

    public UpdateCustomerCommandBuilder WithFirstName()
    {
        _updateCustomerCommand.FirstName = "FirstName";
        return this;
    }

    public UpdateCustomerCommandBuilder WithLastName()
    {
        _updateCustomerCommand.LastName = "LastName";
        return this;
    }

    public UpdateCustomerCommandBuilder WithPatronym()
    {
        _updateCustomerCommand.Patronym = "Patronym";
        return this;
    }

    public UpdateCustomerCommandBuilder WithEmail()
    {
        _updateCustomerCommand.Email = "Email";
        return this;
    }

    public UpdateCustomerCommandBuilder WithPhoneNumber()
    {
        _updateCustomerCommand.PhoneNumber = "+79588386515";
        return this;
    }

    public UpdateCustomerCommandBuilder WithBirthDate()
    {
        _updateCustomerCommand.BirthDate = DateTimeOffset.Now;
        return this;
    }

    public UpdateCustomerCommandBuilder WithLogin()
    {
        _updateCustomerCommand.Login = "Login";
        return this;
    }

    public UpdateCustomerCommandBuilder WithPassword()
    {
        _updateCustomerCommand.Password = "Password";
        return this;
    }
    public UpdateCustomerCommandBuilder WithActive(bool active = false)
    {
        _updateCustomerCommand.IsActive = active;
        return this;
    }
    public UpdateCustomerCommandBuilder WithId(Guid id )
    {
        _updateCustomerCommand.Id = id;
        return this;
    }
}