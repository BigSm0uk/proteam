﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.CustomerService.Core;
using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.CustomerService.Infrastructure.Data;
using ProDelivery.CustomerService.Infrastructure.Data.Repositories;
using ProDelivery.UnitTests.CustomerService.Commands.Builders;
using ProDelivery.UnitTests.CustomerService.CustomerFactory;
using ProDelivery.UnitTests.CustomerService.Directors;
using Xunit;

namespace ProDelivery.UnitTests.CustomerService.Commands;

public class CreateCustomerCommandHandlerTests 
    : TestCommandBase, IClassFixture<TestFixture<CustomerServiceContext>>
{
    private readonly CreateCustomerCommandHandler _createCustomerCommandHandler;
    private readonly IMapper _mapper;

    public CreateCustomerCommandHandlerTests(TestFixture<CustomerServiceContext> testFixture)
    {
        var serviceProvider = testFixture.ServiceProvider;
        _createCustomerCommandHandler = new CreateCustomerCommandHandler(
            new CustomerRepository(_context), serviceProvider.GetRequiredService<IMapper>());
        _mapper = serviceProvider.GetRequiredService<IMapper>();
    }

    [Fact]
    public void Handle_IfMapIsNotSuccess_ShouldReturnSuccess()
    {
        //Arrange
        var command = null as CreateCustomerCommand;

        //Act
        var exception = _createCustomerCommandHandler
            .Handle(command, CancellationToken.None).Exception?.InnerException;
        
        //Assert
        exception.Should().BeOfType<NullReferenceException>();
    }

    [Fact]
    public void Handle_IfRepositoryUpdate_ShouldReturnSuccess()
    {
        //Arrange
        var builder = new CreateCustomerCommandBuilder();
        var customerDirector = new CustomerCreateDirector(builder);
        customerDirector.WithAllProperty();
        var command = builder.Build();

        //Act
        var resultGuid = _createCustomerCommandHandler.Handle(command, CancellationToken.None).Result;

        //Assert
        resultGuid.Should().NotBe(Guid.Empty);

        var actualCustomer = _context.Customers.FirstOrDefault(x => x.Id == resultGuid);
        actualCustomer.SignUpDate.Should().NotBe(default);

        var expectedCustomer = _mapper.Map<CreateCustomerCommand, Customer>(command,
            op => op.AfterMap((src, dest) =>
            {
                dest.Id = resultGuid;
                dest.SignUpDate = actualCustomer.SignUpDate;
            }));

        Assert.Equivalent(expectedCustomer, actualCustomer);
    }
}