﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.CustomerService.Core;
using ProDelivery.CustomerService.Infrastructure.Data;

namespace ProDelivery.UnitTests.CustomerService.CustomerFactory;

public static class CustomerContextFactory
{
    public static CustomerServiceContext Create()
    {
        var options = new DbContextOptionsBuilder<CustomerServiceContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
        var context = new CustomerServiceContext(options);
        context.Database.EnsureCreated();
        context.Customers.AddRange(
            new Customer
            {
                Id = Guid.Parse("4E998B00-DCD0-4326-9F2F-91FE4A30C32B"),
                FirstName = "null",
                LastName = "null",
                Patronym = "null",
                BirthDate = DateTimeOffset.Now,
                Email = "null",
                PhoneNumber = "null",
                IsActive = false,
                Rating = null,
                SignUpDate = DateTimeOffset.Now
            },
            new Customer
            {
                Id = Guid.Parse("F26CE421-C595-4FB2-BC35-5EFDDEA495B6"),
                FirstName = "null2",
                LastName = "null2",
                Patronym = "null2",
                BirthDate = DateTimeOffset.Now,
                Email = "null2",
                PhoneNumber = "null2",
                IsActive = false,
                Rating = null,
                SignUpDate = DateTimeOffset.Now
            }
        );
        context.SaveChanges();
        return context;
    }

    public static void Destroy(CustomerServiceContext context)
    {
        context.Database.EnsureDeleted();
        context.Dispose();
    }
}