﻿using ProDelivery.CustomerService.Infrastructure.Data;

namespace ProDelivery.UnitTests.CustomerService.CustomerFactory;

public class TestCommandBase : IDisposable
{
    protected readonly CustomerServiceContext _context;

    public TestCommandBase()
    {
        _context = CustomerContextFactory.Create();
    }

    public void Dispose()
    {
        CustomerContextFactory.Destroy(_context);
    }
}