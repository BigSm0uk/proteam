﻿using ProDelivery.UnitTests.CustomerService.Commands.Builders;

namespace ProDelivery.UnitTests.CustomerService.Directors;

public class CustomerCreateDirector
{
    private readonly CreateCustomerCommandBuilder _createCustomerCommandBuilder;

    public CustomerCreateDirector(CreateCustomerCommandBuilder createCustomerCommandBuilder)
    {
        _createCustomerCommandBuilder = createCustomerCommandBuilder;
    }

    public void WithAllProperty()
    {
        _createCustomerCommandBuilder.WithEmail().WithPatronym().WithBirthDate().WithFirstName()
            .WithLastName().WithPhoneNumber();
    }
}