﻿using ProDelivery.CustomerService.Core.Customers.Commands;
using ProDelivery.UnitTests.CustomerService.Commands.Builders;

namespace ProDelivery.UnitTests.CustomerService.Directors;

public class CustomerUpdateDirector
{
    private readonly UpdateCustomerCommandBuilder _updateCustomerCommandBuilder;

    public CustomerUpdateDirector(UpdateCustomerCommandBuilder updateCustomerCommandBuilder)
    {
        _updateCustomerCommandBuilder = updateCustomerCommandBuilder;
    }
    public void WithAllProperty()
    {
        _updateCustomerCommandBuilder.WithEmail().WithLogin().WithPassword().WithPatronym().WithBirthDate().WithFirstName()
            .WithLastName().WithPhoneNumber().WithId(new Guid("4E998B00-DCD0-4326-9F2F-91FE4A30C32B")).WithActive();
    }
}