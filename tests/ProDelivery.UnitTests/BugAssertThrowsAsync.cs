using Xunit;

namespace TestProject1
{
    public class BugAssertThrowsAsync
    {
        [Fact]
        public void Test1()
        {
            Assert.ThrowsAsync<NullReferenceException>( async () => await YaNePadayuAsync());
        }

        [Fact]
        public void Test2()
        {
            Assert.Throws<NullReferenceException>(() =>  YaNePadayu());
        }

        Guid YaNePadayu()
        {
            return Guid.NewGuid();
        }

        async Task<Guid> YaNePadayuAsync()
        {
            return await Task.FromResult(Guid.NewGuid());
        }
    }
}