﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.Common.Core.IRepositories;

namespace ProDelivery.UnitTests
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection<TContext>(IConfigurationRoot configuration, string serviceName, IServiceCollection serviceCollection = null)
            where TContext : DbContext
        {
            var assembly = typeof(TContext).Assembly;

            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration)
                .ConfigureInMemoryContext<TContext>()
                .ConfigureRepository<TContext>()
                .AddAutoMapper(assembly)
                .AddMediatR(x => x.RegisterServicesFromAssembly(assembly));
            return serviceCollection;
        }

        private static IServiceCollection ConfigureInMemoryContext<T>(this IServiceCollection services)
            where T : DbContext
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<T>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });
            services.AddTransient<DbContext, T>();
            return services;
        }

        private static IServiceCollection ConfigureRepository<T>(this IServiceCollection services)
            where T : DbContext
        {
            typeof(T).Assembly.DefinedTypes
                .Where(t => t.IsClass)
                .Where(t => !t.IsAbstract)
                .Where(t => t.ImplementedInterfaces
                    .Where(i => i.IsGenericType)
                    .Any(t => t.GetGenericTypeDefinition() == typeof(IEfRepository<>)))
                .Select(t => new
                {
                    Abstruction = t.ImplementedInterfaces
                        .Where(i => !i.IsGenericType)
                        .Where(i => i.Name.EndsWith("Repository"))
                        .Single(),
                    Implementaion = t
                })
                .ToList()
                .ForEach(r => services.AddScoped(r.Abstruction, r.Implementaion));

            // Этот способ ломается, когда меняется namespace,
            // а также требует внимания, если нужно добавить репозиторий. 
            // var context = Convert.ToString(typeof(T));
            //switch (context)
            //{
            //    case "ProDelivery.CustomerService.Infractructure.CustomerServiceContext":
            //        services.AddScoped<ICustomerRepository, CustomerRepository>();
            //        break;
            //    case "ProDelivery.CourierService.Infractructure.CourierServiceContext":
            //        services.AddScoped<ICourierRepository, CourierRepository>();
            //        break;
            //    case "ProDelivery.OrderService.Infractructure.OrderServiceContext":
            //        services.AddScoped<IOrderRepository, OrderRepository>();
            //        break;
            //}
            return services;
        }
    }
}
