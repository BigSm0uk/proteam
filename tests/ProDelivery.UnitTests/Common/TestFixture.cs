﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ProDelivery.UnitTests
{
    public class TestFixture<T> : IDisposable
        where T : DbContext
    {
        public IServiceProvider ServiceProvider { get; set; }

        public TestFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceCollection = Configuration.GetServiceCollection<T>(configuration, "Tests");
            var serviceProvider = serviceCollection
                .BuildServiceProvider();
            ServiceProvider = serviceProvider;
        }

        public void Dispose()
        {
            
        }
    }
}
