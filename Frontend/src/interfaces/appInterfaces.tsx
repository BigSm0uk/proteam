export interface IFullRegistrationData {
  email: string;
  password: string;
  confirmPassword: string;
  username: string;
  phoneNumber: string;
  role: string;
}

export interface ILoginForm {
  username: string;
  password: string;
}

export interface IToken {
  access_token: string;
}

export interface IOrder {
  id: string;
  title: string;
  description: string;
  deliveryCost: number;
  status: string;
}

export interface IDelivery {
  cost: number;
  id: string;
  orderDescription: string;
  status: string;
}
export interface IshortOrder {
  id: string;
  title: string;
  deliveryCost: number;
  status: string;
}
export interface IuserInfo {
  sub: string;
  role: string;
}
