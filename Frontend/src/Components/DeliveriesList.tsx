import { Grid, Typography, Box, Button, Stack } from "@mui/material";
import { IDelivery } from "../interfaces/appInterfaces";
import { DeliveriesListItem } from "./DeliveriesListItem";
import { useEffect, useState } from "react";
import { useDeliveries } from "../hooks/useDeliveries";
import { useAppSelector } from "../hooks/hooks";

export const DeliveriesList = () => {

  const token = useAppSelector((state) => state.user.user.token);
  const [deliveries, setDeliveries] = useState<IDelivery[]>([]);
  const getDeliveries = async () => {
    console.log("getting deliveries...");
    const DeliveriesFromBack = await useDeliveries(token!);
    setDeliveries(DeliveriesFromBack);
  };

  useEffect(() => {
    getDeliveries();
  }, []);

  return (
    <Box sx={{ width: '50%', minWidth: 500, maxWidth: 800 }} >
      <Stack direction="row" margin={2} justifyContent="space-between">
      <Typography hidden={deliveries.length < 1} component="h1" variant="h5">Ваши доставки : </Typography>
      <Typography hidden={deliveries.length > 0} component="h1" variant="h5">У вас ещё нет принятых заказов : </Typography>
      <Button variant="outlined" onClick={getDeliveries}>Обновить</Button>
        </Stack>
      <Grid container spacing={0.5}>
        {deliveries?.map((delivery) => (
          <DeliveriesListItem {...delivery} key={delivery.id} />
          ))}
      </Grid>
    </Box>
  );
};