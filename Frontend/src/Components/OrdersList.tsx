import { Grid, Typography, Box, Button, Stack, Divider } from "@mui/material";
import { IOrder } from "../interfaces/appInterfaces";
import { OrdersListItem } from "./OrdersListItem";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks/hooks";
import { useOrders } from "../hooks/useOrders";
import { useNavigate } from "react-router-dom";
import { removeUser } from "../store/slices/userSlice";

export const OrdersList = () => {
  const token = useAppSelector((state) => state.user.user.token);
  const [orders, setOrders] = useState<IOrder[]>([]);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const getOrders = async () => {
    console.log("getting deliveries...");
    const OrdersFromBack = await useOrders(token!);
    setOrders(OrdersFromBack);
  };

  useEffect(() => {
    getOrders();
  }, []);

  return (
    <Box sx={{ width: "50%", minWidth: 500, maxWidth: 800 }}>
      <Stack
        direction="row"
        spacing={0.5}
        margin={2}
        divider={<Divider orientation="vertical" flexItem />}
      >
        <div>
          <Typography hidden={orders.length < 1} component="h1" variant="h5">
            Ваши заказы :{" "}
          </Typography>
          <Typography hidden={orders.length > 0} component="h1" variant="h5">
            У вас нет заказов :{" "}
          </Typography>
        </div>

        <Button variant="outlined" onClick={getOrders}>
          Обновить
        </Button>
        <Button
          variant="outlined"
          onClick={() => {
            navigate("/Authorisation");
            dispatch(removeUser());
          }}
        >
          Выйти
        </Button>
      </Stack>
      <Grid container spacing={0.5}>
        {orders?.map((orders) => (
          <OrdersListItem {...orders} key={orders.id} />
        ))}
      </Grid>
    </Box>
  );
};
