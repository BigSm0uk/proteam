import { useEffect, useState } from "react";
import {
  Grid,
  Card,
  CardHeader,
  Avatar,
  CardContent,
  Typography,
  CardActions,
  Button,
  Collapse,
  Box,
} from "@mui/material";
import { green, red } from "@mui/material/colors";
import { useCardSyles } from "../Styles/CardStyle";
import CurrencyRubleIcon from "@mui/icons-material/CurrencyRuble";
import ky from "ky";
import { useAppSelector } from "../hooks/hooks";
import { IOrder } from "../interfaces/appInterfaces";
import { HubConnectionBuilder } from "@microsoft/signalr";

export const OrdersListItem = (orderFromProps: IOrder) => {
  const { card, action } = useCardSyles();
  const [{ id, status, title, description, deliveryCost }, setOrder] =
    useState<IOrder>(orderFromProps);
  const [isExpanden, setExpanded] = useState(false);
  const token = useAppSelector((state) => state.user.user.token);
  useEffect(() => {
    setOrder(orderFromProps);
    console.log(orderFromProps);
  }, [orderFromProps]);

  const getOrder = async (id: string) => {
    await ky
      .get(`http://localhost:5050/ordering/v1/Orders/${id}`, {
        headers: {
          Authorization: "Bearer " + token
        },
      })
      .json()
      .then((response) => setOrder(response as IOrder));
  };

  // не реализовано на бэке
  const updateOrder = async (newStatus: string) => {
    await ky
      .put(`http://localhost:5050/ordering/v1/Orders/${id}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
        json: { status: newStatus },
      })
      .json()
      .then();
  };

  async function removeOrder() {
    await ky
      .delete(`http://localhost:5050/ordering/v1/Orders/${id}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
      })
      .then(console.log)
      .then(() =>
        setOrder({ id, status: "Deleted", title, description, deliveryCost })
      );
  }

  useEffect(() => {
    setUpSignalRConnection().then((con) => {
      console.log(con.connectionId);
    });
  }, []);

  const setUpSignalRConnection = async () => {
    let connection = new HubConnectionBuilder()
      .withUrl(`http://localhost:5051/hubs/ordershub?userId=Customer`)
      .withAutomaticReconnect()
      .build();

    connection.on('Message', (message: string) => {
      console.log('Message', message);
    });

    connection.on('orderUpdated', async () => {
      await getOrder(id);
    });

    try {
      await connection.start();
    } catch (err) {
      console.log(err);
    }
    return connection;
  };


  return (
    <Grid item xs={12} md={12} lg={12}>
      <Card variant="outlined" className={card}>
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              O
            </Avatar>
          }
          title="User name"
          subheader="September 14, 2016"
        />
        <CardContent>
          <Typography variant="body2">
            {title}
          </Typography>
          <Typography sx={{ color: green[500] }} variant="body2">
            {status}
          </Typography>
        </CardContent>
        <CardActions disableSpacing className={action}>
          <Button
            disabled={status !== "Delivered"}
            sx={{ marginRight: "auto" }}
            size="small"
            color="success"
            onClick={async () => {
              await removeOrder();
            }}
          >
            Удалить заказ
          </Button>
          {/* <Button
            disabled={status !== "Loaded"}
            sx={{ marginRight: "auto" }}
            size="small"
            color="success"
            onClick={async () => {
              console.warn("NOT IMPLEMENTED canceling order...");
              //await updateOrder('Canceled');
            }}
          >
            Отменить заказ
          </Button> */}
          <Box>
            <Typography>
              {deliveryCost} <CurrencyRubleIcon />
            </Typography>
          </Box>
          <Button
            size="small"
            onClick={async () => {
              setExpanded(!isExpanden);
              if (!description) getOrder(id);
            }}
          >
            Подробнее
          </Button>
        </CardActions>
        <Collapse in={isExpanden} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Описание:</Typography>
            <Typography paragraph>{description}</Typography>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};
