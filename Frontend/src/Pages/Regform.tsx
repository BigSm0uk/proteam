import { IFullRegistrationData } from "../interfaces/appInterfaces";
import "../index.css";
import { useForm } from "react-hook-form";
import Avatar from "@mui/material/Avatar";
import LockOutlinedIcon from "@mui/icons-material/Castle";
import {
  Box,
  Container,
  Grid,
  TextField,
  ThemeProvider,
  Typography,
  createTheme,
} from "@material-ui/core";
import {
  Button,
  FormControl,
  Link,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import { useAuth } from "../hooks/use-auth";
import { Navigate, useNavigate } from "react-router-dom";
import { useRegistration } from "../hooks/useRegistration";
import React from "react";

const defaultTheme = createTheme();

export default function Regform() {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
    watch,
  } = useForm<IFullRegistrationData>({
    mode: "onBlur",
  });
  const [value, setValue] = React.useState<string>("");
  const handleChange = (event: SelectChangeEvent) => {
    setValue(event.target.value as string);
  };

  const { isAuth } = useAuth();
  const navigate = useNavigate();

  return !isAuth ? (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Регистрация
          </Typography>
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>

          <Box
            component="form"
            onSubmit={handleSubmit((data) =>
              useRegistration({ ...data, role: value }, navigate)
            )}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  {...register("username", {
                    required: true,
                    minLength: {
                      value: 2,
                      message: "Минимум 2 символов!",
                    },
                  })}
                  error={!!errors?.username}
                  autoComplete="First name"
                  required
                  fullWidth
                  label="Логин"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("email", {
                    required: "Поле обязтельно к заполнению!",
                    pattern: {
                      message: "Введите правильный email",
                      value: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
                    },
                  })}
                  error={!!errors?.email}
                  helperText={errors?.email?.message}
                  required
                  fullWidth
                  label="Электронная почта"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("phoneNumber", {
                    required: "Поле обязтельно к заполнению!",
                    pattern: {
                      message: "Введите правильный номер телефона",
                      value: /[0-9]{10}$/,
                    },
                    maxLength: 10,
                  })}
                  error={!!errors?.phoneNumber}
                  helperText={errors?.phoneNumber?.message}
                  required
                  fullWidth
                  label="Номер телефона"
                  name="phoneNumber"
                  autoComplete="phoneNumber"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("password", {
                    required: "Поле обязтельно к заполнению!",
                    pattern: {
                      message:
                        "Минимум 5 символов, хотя бы с одной буквой, цифрой и специальным знаком!",
                      value:
                        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
                    },
                  })}
                  required
                  fullWidth
                  name="password"
                  label="Пароль"
                  autoComplete="new-password"
                  error={!!errors?.password}
                  helperText={errors?.password?.message}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  {...register("confirmPassword", {
                    required: "Поле обязтельно к заполнению!",
                    validate: (val: string) => {
                      if (watch("password") !== val) {
                        return "Пароли не совпадают!";
                      }
                    },
                    pattern: {
                      message:
                        "Минимум 5 символов, хотя бы с одной буквой, цифрой и специальным знаком!",
                      value:
                        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
                    },
                  })}
                  required
                  fullWidth
                  name="confirmPassword"
                  label="Повторите Ваш пароль"
                  autoComplete="new-password"
                  error={!!errors?.confirmPassword}
                  helperText={errors?.confirmPassword?.message}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Пользователь
                  </InputLabel>
                  <Select
                    value={value}
                    label="Пользователь"
                    onChange={handleChange}
                  >
                    <MenuItem value={"Courier"}>Курьер</MenuItem>
                    <MenuItem value={"Customer"}>Заказчик</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                disabled={!isValid || !value}
              >
                Зарегистрироваться
              </Button>
            </Grid>

            <Grid container>
              <Grid item xs></Grid>
              <Grid item>
                <Link href="../Authorisation" variant="body2">
                  {"Уже есть аккаунт? Войдите"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  ) : (
    <Navigate to="/HomePage" />
  );
}
