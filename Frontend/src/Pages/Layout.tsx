import { NavLink, Navigate, Outlet } from "react-router-dom";
import { useAuth } from "../hooks/use-auth";

const Layout = () => {
  const { isAuth, isCourier, isCustomer } = useAuth();

  return (
    <>
      {/* {isCourier && isAuth ? <Navigate to="HomePage" /> : <></>}
      {isCustomer && isAuth ? <Navigate to="NewShipment" /> : <></>} */}
      <header>
        <ul>
          {isCourier && isAuth ? (
            <li>
              <NavLink to="/HomePage">Заказы</NavLink>
            </li>
          ) : (
            <></>
          )}
          {isCustomer && isAuth ? (
            <li>
              <NavLink to="/NewShipment">Создать заказ</NavLink>
            </li>
          ) : (
            <></>
          )}
          {isCustomer && isAuth ? (
            <li>
              <NavLink to="/OrdersHistory">История заказов</NavLink>
            </li>
          ) : (
            <></>
          )}
          {!isAuth ? (
            <li>
              <NavLink to="/Authorisation">Авторизация</NavLink>
            </li>
          ) : (
            <></>
          )}
          {isAuth && isCourier ? (
            <li>
              <NavLink to="/PersonalAccount">Личный кабинет</NavLink>
            </li>
          ) : (
            <></>
          )}
          {!isAuth ? (
            <li>
              <NavLink to="/Registration">Регистрация</NavLink>
            </li>
          ) : (
            <></>
          )}
        </ul>
      </header>
      <Outlet />
    </>
  );
};

export { Layout };
