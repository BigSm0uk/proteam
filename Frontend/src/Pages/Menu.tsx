import Regform from "./Regform.tsx";
import NotFound from "./NotFound.tsx";
import { Routes, Route } from "react-router-dom";
import Authorisation from "./Authorisation.tsx";
import { Layout } from "./Layout.tsx";
import { HomePage } from "./HomePage.tsx";
import { PersonalAccount } from "./PersonalAccount.tsx";
import { OrdersHistory } from "./OrdersHistory.tsx";
import NewShipment from "./newShipment.tsx";

export default function Menu() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="Registration" element={<Regform />} />
          <Route path="HomePage" element={<HomePage />} />
          <Route path="OrdersHistory" element={<OrdersHistory />} />
          <Route path="Authorisation" element={<Authorisation />} />
          <Route path="PersonalAccount" element={<PersonalAccount />} />
          <Route path="NewShipment" element={<NewShipment />} />
          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>

      {/* <footer className="footer">2023 ©</footer> */}
    </div>
  );
}
