import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useForm } from "react-hook-form";
import { Navigate } from "react-router-dom";
import { useAuth } from "../hooks/use-auth";
import { ILoginForm } from "../interfaces/appInterfaces";
import { useLogin } from "../hooks/useLogin";
import { useAppDispatch } from "../hooks/hooks";

function Copyright(props: any) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      <Link color="inherit" href="https://mui.com/">
        ProDelivery
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

export default function Authorisation() {
  const {
    register,
    formState: { errors, isValid },
    handleSubmit,
  } = useForm<ILoginForm>({
    mode: "onBlur",
  });
  const dispatch = useAppDispatch();
  const { isAuth } = useAuth();

  return !isAuth ? (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Авторизация
          </Typography>
          <Box
            component="form"
            onSubmit={handleSubmit((data) => {
              useLogin(data, dispatch);
            })}
            noValidate
            sx={{ mt: 1 }}
          >
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  {...register("username", {
                    required: true,
                    minLength: {
                      value: 2,
                      message: "Минимум 2 символов!",
                    },
                  })}
                  error={!!errors?.username}
                  autoComplete="First name"
                  required
                  fullWidth
                  label="Логин"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  {...register("password", {
                    required: "Поле обязтельно к заполнению!",
                    pattern: {
                      message:
                        "Минимум 5 символов, хотя бы с одной буквой, цифрой и специальным знаком!",
                      value:
                        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
                    },
                  })}
                  required
                  fullWidth
                  name="password"
                  label="Пароль"
                  autoComplete="new-password"
                  error={!!errors?.password}
                  helperText={errors?.password?.message}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Запомнить меня"
                  sx={{ mt: -2, mb: 3 }}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: -5, mb: 3 }}
                  disabled={!isValid}
                >
                  Войти
                </Button>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item>
                <Link href="../Registration" variant="body2">
                  {"Нет аккаунта? Зарегистрируйтесь"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  ) : (
    <Navigate to="/" />
  );
}
