import { ThemeProvider } from "@emotion/react";
import { createTheme } from "@mui/material";
import { OrdersList } from "../Components/OrdersList";

export const OrdersHistory = () => {
  const defaultTheme = createTheme();

  return (
    <ThemeProvider theme={defaultTheme}>
      <OrdersList />
    </ThemeProvider>
  );
};
