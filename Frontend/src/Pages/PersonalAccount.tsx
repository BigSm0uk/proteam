import { ThemeProvider } from "@emotion/react";
import { createTheme } from "@mui/material";
import { DeliveriesList } from "../Components/DeliveriesList";

export const PersonalAccount = () => {
  const defaultTheme = createTheme();

  return (
    <ThemeProvider theme={defaultTheme}>
      <DeliveriesList />
    </ThemeProvider>
  );
};
