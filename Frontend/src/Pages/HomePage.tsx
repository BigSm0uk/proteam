import { ThemeProvider, Typography, createTheme } from "@material-ui/core";
import Avatar from "@mui/material/Avatar";
import LockOutlinedIcon from "@mui/icons-material/Castle";
import { Navigate, useNavigate } from "react-router-dom";
import { Box, Button, Grid } from "@mui/material";
import { removeUser } from "../store/slices/userSlice";
import { useAppDispatch } from "../hooks/hooks";
import { useAuth } from "../hooks/use-auth";
import { OrderList } from "../Components/OrderCard";
import { useHotOrders } from "../hooks/useOrders";
import { useEffect, useState } from "react";
import { IOrder } from "../interfaces/appInterfaces";

const HomePage = () => {
  const Click = async () => {
    const OrdersFromBack = await useHotOrders();
    setOrders(OrdersFromBack);
  };
  const dispatch = useAppDispatch();
  const defaultTheme = createTheme();
  const { isAuth } = useAuth();
  const navigate = useNavigate();
  const [orders, setOrders] = useState<IOrder[]>([]);
  useEffect(() => {
    Click();
  }, []);

  return isAuth ? (
    <ThemeProvider theme={defaultTheme}>
      <Typography component="h1" variant="h5">
        Список заказов
      </Typography>
      <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
        <LockOutlinedIcon />
      </Avatar>
      <Button
        onClick={() => {
          navigate("/Authorisation");
          dispatch(removeUser());
        }}
      >
        Log out
      </Button>
      <Box sx={{ width: "50%", minWidth: 500, maxWidth: 800 }}>
        <Grid container spacing={0.5}>
          <Button onClick={Click}>Обновить</Button>
          {orders?.map((order) => (
            <OrderList {...order} key={order.id} />
          ))}
        </Grid>
      </Box>
    </ThemeProvider>
  ) : (
    <Navigate to="/Authorisation" />
  );
};

export { HomePage };
