import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface IuserAccesData {
  token: string | null;
  role: string | null;
}

const initialState: IuserAccesData = {
  token: null,
  role: null,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<IuserAccesData>) {
      state.token = action.payload.token;
      state.role = action.payload.role;
    },
    removeUser(state) {
      state.token = null;
      state.role = null;
    },
  },
});

export const { setUser, removeUser } = userSlice.actions;

export default userSlice.reducer;
