import "./index.css";
import Menu from "./Pages/Menu.tsx";

export default function App() {
  return (
    <div>
      <Menu />
    </div>
  );
}
