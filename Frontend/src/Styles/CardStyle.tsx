import { makeStyles } from "@material-ui/core/styles";
export const useCardSyles = makeStyles(() => ({
  card: {
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  content: {},
  header: {},
  action: {
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  collaps: {},
}));
