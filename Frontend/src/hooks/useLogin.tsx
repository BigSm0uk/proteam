import ky from "ky";
import { ILoginForm, IToken } from "../interfaces/appInterfaces";
import { AppDispatch } from "../store";
import { useUserInfo } from "./useUserInfo";

export const useLogin = async (data: ILoginForm, dispatch: AppDispatch) => {
  const { username, password } = data;
  try {
    const token = (await ky
      .post("http://localhost:6001/connect/token", {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          //"Authorization": "Bearer {token}"
        },
        body: `grant_type=password&username=${username}&password=${password}&scope=openid%20profile%20customerApi%20courierApi%20orderApi&client_id=react`,
      })
      .json()) as IToken;
    await useUserInfo(token.access_token, dispatch);
  } catch (err) {}
};
