import ky from "ky";
import { IFullRegistrationData } from "../interfaces/appInterfaces";
import { NavigateFunction } from "react-router-dom";

export const useRegistration = async (
  data: IFullRegistrationData,
  navigate: NavigateFunction
) => {
  try {
    await ky
      .post("http://localhost:6001/Account/Register", { json: data })
      .then((result) => result.json().then(console.log))
      .then(() => navigate("../Authorisation"));
  } catch (err) {
    console.log(err);
  }
};
