import { useAppSelector } from "./hooks";

export function useAuth() {
  const { token, role } = useAppSelector((state) => state.user.user);
  return {
    isAuth: !!token,
    token,
    isCourier: role === "Courier",
    isCustomer: role === "Customer",
  };
}
