import ky from "ky";
import { IDelivery } from "../interfaces/appInterfaces";

export const useDeliveries = async (token: string): Promise<IDelivery[]> => {
  const api = ky.create({
    prefixUrl: "http://localhost:5050/shipping/v1/Deliveries",
  });
  const response: IDelivery[] = await api
    .get("", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .json();
  return response;
};
