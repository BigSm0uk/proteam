import ky from "ky";
import { IuserInfo } from "../interfaces/appInterfaces";
import { AppDispatch } from "../store";
import { setUser } from "../store/slices/userSlice";

export const useUserInfo = async (token: string, dispatch: AppDispatch) => {
  try {
    const userInfo = (await ky
      .get("http://localhost:6001/connect/userinfo", {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
      .json()) as IuserInfo;

    dispatch(setUser({ token: token, role: userInfo.role }));
  } catch (err) {}
};
