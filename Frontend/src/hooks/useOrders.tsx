import ky from "ky";
import { IOrder } from "../interfaces/appInterfaces";

export const useHotOrders = async (): Promise<IOrder[]> => {
  const api = ky.create({
    prefixUrl: "http://localhost:5050/ordering/v1/HotOrders/",
  });
  const response: IOrder[] = await api
    .get("", {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .json();
  return response;
};

export const useOrders = async (token: string): Promise<IOrder[]> => {
  const api = ky.create({
    prefixUrl: "http://localhost:5050/ordering/v1/Orders/",
  });
  const response: IOrder[] = await api
    .get("", {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .json();
  return response;
};
