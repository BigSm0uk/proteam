﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.DataAccess;

public interface IDbInitializer
{
    void MigrateDb();
    void SeedDb();
}

public class OrderingDbInitializer : IDbInitializer
{
    private const int OrdersCount = 5;
    private readonly DbContext _context;
    private readonly IRedisRepository<OrderDto> _redisRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<OrderingDbInitializer> _logger;

    public OrderingDbInitializer(
        ILogger<OrderingDbInitializer> logger,
        OrderingDbContext context,
        IRedisRepository<OrderDto> redisRepository,
        IMapper mapper)
    {
        _context = context;
        _redisRepository = redisRepository;
        _mapper = mapper;
        _logger = logger;
    }

    public void SeedDb()
    {
        if (_context.Database.EnsureCreated())
        {
            _logger.LogInformation("Database is created.");

            var orders = OrderingDataFactory.GenerateOrders(OrdersCount);

            _context.AddRange(orders);
            _context.SaveChanges();

            _redisRepository.SetRangeAsync(orders
                .Where(x => x.Status == OrderStatus.Created)
                .Select(_mapper.Map<Order, OrderDto>));
        }
        else
        {
            _logger.LogInformation("Database already exists.");
        }
    }

    public void MigrateDb()
    {
        if (_context.Database.EnsureCreated())
        {
            _logger.LogInformation("Database is created.");
        }
        else
        {
            _logger.LogInformation("Database already exists.");
            _context.Database.Migrate();
            _logger.LogInformation("Database is migrated.");
        }
    }
}
