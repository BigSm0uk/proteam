﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.DataAccess;

namespace ProDelivery.Ordering.DependencyInjection;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Регистрирует подключение к базе данных.
    /// </summary>
    public static IServiceCollection AddDataAccess(this IServiceCollection services,
        IConfiguration configuration)
    {
        services
            .AddScoped<DbContext, OrderingDbContext>()
            .AddScoped(typeof(IEfRepository<>), typeof(EfRepository<>))
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
            .AddScoped(typeof(IOrderRepository), typeof(OrderRepository))
            .AddDbContext<OrderingDbContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("OrderingDb");
                if (connectionString == null)
                    options.UseSqlite("Data Source=OrderingApi.db");
                else
                {
                    options.UseNpgsql(connectionString);
                    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
                }
            })
            .AddScoped<IDbInitializer, OrderingDbInitializer>()
            .AddScoped(typeof(IRedisRepository<>), typeof(RedisRepository<>))
            .Configure<RedisRepositoryOptions>(
                x => x.Configuration = configuration.GetConnectionString("RedisHost"));

        return services;
    }
}
