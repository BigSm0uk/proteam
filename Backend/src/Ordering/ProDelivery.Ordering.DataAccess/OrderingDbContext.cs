﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Ordering.Core.Domain;
using System.Reflection;

namespace ProDelivery.Ordering.DataAccess;

public class OrderingDbContext : DbContext
{
    public DbSet<Order> Orders { get; private set; }

    public OrderingDbContext(DbContextOptions<OrderingDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(builder);
    }
}
