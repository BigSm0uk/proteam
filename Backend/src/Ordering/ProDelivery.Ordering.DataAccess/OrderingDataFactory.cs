﻿using Bogus;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.DataAccess;

internal class OrderingDataFactory
{
    internal class FakeOrder : Order
    {
        public FakeOrder() : base() { }
    }

    public static List<Order> GenerateOrders(int count, int seed = 0)
    {
        var orderIdsEnumerator = GetPreconfiguredOrderId();

        var orderStatusEnumerator = GetInQuantity(OrderStatus.Created, 3);

        return new Faker<FakeOrder>("ru")
            .RuleFor(x => x.Id, (f, x) => orderIdsEnumerator.GetNextOrRandom(
                () => f.Random.Guid()))
            .RuleFor(x => x.Status, (f, x) => orderStatusEnumerator.GetNextOrRandom(
                () => f.Random.ArrayElement(Enum.GetValues<OrderStatus>())))
            .RuleFor(x => x.CreateDate, (f, x) => f.Date.RecentOffset())
            .RuleFor(x => x.DeliveryCost, (f, x) => f.Random.Int(2, 75) * 20)
            .RuleFor(x => x.Description, (f, x) => f.Lorem.Lines(1))
            .RuleFor(x => x.UserId, (f, x) => Guid.Parse(f.Random.ArrayElement(PreconfiguredCustomerIds)))
            .RuleFor(x => x.Title, (f, x) => x.Description[..(x.Description.Length < 20 ? x.Description.Length : 20)])
            .UseSeed(1)
            .Generate(count)
            .Cast<Order>()
            .ToList();
    }

    static string[] PreconfiguredCustomerIds =
    {
        "00000000-0000-0000-0000-000000000001",
        "00000000-0000-0000-0000-000000000003",
    };

    static string[] PreconfiguredCourierIds =
    {
        "00000000-0000-0000-0000-000000000002",
        "00000000-0000-0000-0000-000000000004",
    };

    static IEnumerator<Guid> GetPreconfiguredOrderId()
    {
        yield return Guid.Parse("00000000-0000-0000-0000-000000000010");
        yield return Guid.Parse("00000000-0000-0000-0000-000000000020");
        yield return Guid.Parse("00000000-0000-0000-0000-000000000030");
    }

    static IEnumerator<T> GetInQuantity<T>(T value, int quantity)
    {
        while (quantity-- > 0) yield return value;
    }
}

public static class EnumeratorExtensions
{
    public static T GetNext<T>(this IEnumerator<T> enumerator)
    {
        if (enumerator.MoveNext())
            return enumerator.Current;
        else
            throw new Exception("Preconfigured items are over.");
    }

    public static T GetNextOrRandom<T>(this IEnumerator<T> enumerator, Func<T> random)
    {
        if (enumerator.MoveNext())
            return enumerator.Current;
        else
            return random();
    }
}