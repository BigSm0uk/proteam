﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.DataAccess;

public class EfRepository<T> : IEfRepository<T> where T : BaseEntity
{
    private readonly DbContext _context;
    private readonly DbSet<T> _set;

    public EfRepository(DbContext context)
    {
        _context = context;
        _set = context.Set<T>();
    }

    public Guid Create(T entity)
    {
        _set.Add(entity);
        _context.SaveChanges();
        return entity.Id;
    }

    public async Task<Guid> CreateAsync(T entity)
    {
        await _set.AddAsync(entity);
        await _context.SaveChangesAsync();
        return entity.Id;
    }

    public T? GetById(Guid id)
    {
        return _set.Find(new object[] { id });
    }

    public async Task<T?> GetByIdAsync(Guid id)
    {
        return await _set.FindAsync(new object[] { id });
    }

    public void Delete(Guid id)
    {
        var entity = _set.Find(new object[] { id });

        if (entity != null)
        {
            _set.Remove(entity);
            _context.SaveChanges();
        }
    }

    public async Task DeleteAsync(Guid id)
    {
        var entity = await _set.FindAsync(new object[] { id });

        if (entity != null)
        {
            _set.Remove(entity);
            await SaveChangesAsync();
        }
    }

    public virtual IQueryable<T> GetAll()
    {

        return _set;
    }

    public async Task SaveChangesAsync()
    {
        await _context.SaveChangesAsync();
    }

    public void Update(T entity)
    {
        _set.Update(entity);
        _context.SaveChanges();        
    }

    public async Task UpdateAsync(T entity)
    {
        _set.Update(entity);
        await _context.SaveChangesAsync();
    }
}
