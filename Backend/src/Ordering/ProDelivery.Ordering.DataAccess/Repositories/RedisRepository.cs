﻿using Microsoft.Extensions.Options;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Exceptions;
using StackExchange.Redis;
using System.Text.Json;

namespace ProDelivery.Ordering.DataAccess;

public class RedisRepository<T> : IRedisRepository<T> where T : IRedisDbContract
{
    private readonly IDatabase _db;
    private readonly string _key;
    private readonly RedisRepositoryOptions _options;

    public RedisRepository(IOptions<RedisRepositoryOptions> optionsAccessor)
    {
        _options = optionsAccessor?.Value
            ?? throw new ArgumentNullException(nameof(optionsAccessor));

        var redis = ConnectionMultiplexer.Connect(_options.Configuration);
        _db = redis.GetDatabase();
        _key = RedisContractKeys.AvailableOrders;
    }

    public async Task<IEnumerable<T>> GetAllAsync()
    {
        var data = await _db.HashValuesAsync(_key);

        return data.Select(
            x => JsonSerializer.Deserialize<T>(x)
            ?? throw new NullReferenceException("JsonSerializer returned null"));
    }

    public async Task<IEnumerable<TMap>> GetAllAsAsync<TMap>()
    {
        var data = await _db.HashValuesAsync(_key);

        return data.Select(
            x => JsonSerializer.Deserialize<TMap>(x)
            ?? throw new NullReferenceException("JsonSerializer returned null"));
    }

    public async Task<TMap> GetAsAsync<TMap>(string id)
    {
        var value = await _db.HashGetAsync(_key, id);

        if (!value.HasValue)
            throw new NotFoundException(nameof(T), id);

        var dto = JsonSerializer.Deserialize<TMap>(value)!;

        return dto;
    }

    public async Task SetAsync(T value)
    {
        await _db.HashSetAsync(_key, value.FieldName, JsonSerializer.Serialize(value));
    }

    public async Task SetRangeAsync(IEnumerable<T> values)
    {
        await _db.HashSetAsync(_key, values
            .Select(x => new HashEntry(x.FieldName, JsonSerializer.Serialize(x)))
            .ToArray());
    }

    public async Task<T> GetAsync(string id)
    {
        var value = await _db.HashGetAsync(_key, id);
        if (value.HasValue)
            return JsonSerializer.Deserialize<T>(value)!;

        throw new NotFoundException(nameof(T), id);
    }

    public async Task<bool> DeleteAsync(string id)
    {
        return await _db.HashDeleteAsync(_key, id);
    }

    public async Task<T> PopAsync(string id)
    {
        var value = await _db.HashGetAsync(_key, id);

        if (!value.HasValue)
            throw new NotFoundException(nameof(T), id);

        var dto = JsonSerializer.Deserialize<T>(value)!;
        await _db.HashDeleteAsync(_key, id);
        return dto;
    }
}