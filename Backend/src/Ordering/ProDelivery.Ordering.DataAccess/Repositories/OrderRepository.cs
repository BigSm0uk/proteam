﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;
using System.Collections.Generic;

namespace ProDelivery.Ordering.DataAccess;

public class OrderRepository : EfRepository<Order>, IOrderRepository
{
    private readonly DbSet<Order> _orders;

    public OrderRepository(OrderingDbContext context) : base(context)
    {
        _orders = context.Orders;
    }

    public Order Find(Guid? userId = null,
        string? title = null,
        string? description = null,
        OrderStatus? status = null)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<Order>> FindAllAsync(
        string? term = null,
        Guid? userId = null,
        string? title = null,
        string? description = null,
        OrderStatus? status = null)
    {
        var query = _orders.AsQueryable();

        if (term != null)
            query = query.Where(x => x.Description.Contains(term) || x.Title.Contains(term));
        if (userId.HasValue)
            query = query.Where(x => x.UserId == userId);
        if (title != null)
            query = query.Where(x => x.Title == title);
        if (description != null)
            query = query.Where(x => x.Description == description);
        if (status.HasValue)
            query = query.Where(x => x.Status == status);

        return await query.ToListAsync();
    }

    public IEnumerable<Order> FindAll(Guid? userId = null, string? title = null, string? description = null, OrderStatus? status = null)
    {
        throw new NotImplementedException();
    }
}
