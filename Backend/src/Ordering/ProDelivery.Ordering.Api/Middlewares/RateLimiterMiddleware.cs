﻿using Microsoft.Extensions.Caching.Memory;
using System.Net;

namespace ProDelivery.Ordering.Api.Middlewares;

public class RateLimiterMiddleware
{
    private readonly RequestDelegate _next;

    public RateLimiterMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context, IMemoryCache memoryCache)
    {
        var now = DateTime.UtcNow;
        var minInterval = TimeSpan.FromMilliseconds(500);
        var key = $"rateLimiting_{context.Request.Path}";
        var lastRequestDate = memoryCache.Get<DateTime?>(key);
        if (lastRequestDate != null && now - lastRequestDate < minInterval)
        {
            context.Response.StatusCode = (int)HttpStatusCode.TooManyRequests;
            context.Response.Headers["Retry-After"] =
                (lastRequestDate - now + minInterval).Value.TotalSeconds.ToString("0.000");
        }
        else
        {
            memoryCache.Set(key, now);
            await _next(context);
        }
    }
}

public static class RateLimiterExtensions
{
    public static IApplicationBuilder UseRateLimiting(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<RateLimiterMiddleware>();
    }
}
