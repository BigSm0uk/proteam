using Microsoft.AspNetCore.SignalR;
using ProDelivery.Ordering.Api;
using ProDelivery.Ordering.Api.Middlewares;
using ProDelivery.Ordering.Api.Services;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.SignalR;
using ProDelivery.Ordering.DataAccess;
using ProDelivery.Ordering.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System.Text.Json.Serialization;

#region Logging

Log.Logger = new LoggerConfiguration()
.MinimumLevel.Debug()
.MinimumLevel.Override("System", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Information)
.MinimumLevel.Override("Grpc", LogEventLevel.Information)
.Enrich.FromLogContext()
.WriteTo.Console(theme: AnsiConsoleTheme.Literate)
.WriteTo.File($"Logs/{DateTime.Now.Ticks}-ordering.log")
.CreateLogger();

#endregion
try
{
    var builder = WebApplication.CreateBuilder(args);
    var configuration = builder.Configuration;
    var services = builder.Services;

    services
        .AddControllers(options
            => options.SuppressAsyncSuffixInActionNames = true)
        .AddJsonOptions(options
            => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));


    services
        .AddApplicationCore(configuration)
        .AddDataAccess(configuration)
        .AddGrpcApi(configuration)
        .AddSignalR().Services
        .AddSingleton<IUserIdProvider, UserIdProvider>()
        .AddMassTransitWithRabbitMq(configuration)
        .AddSingleton<ICurrentUserService, CurrentUserService>()
        .AddHttpContextAccessor()
        .AddIdentityServer(configuration)
        .AddSwagger()
        .AddMemoryCache()
        .AddCors(cors => cors.AddPolicy("CorsPolicy", policy =>
        {
            policy.WithOrigins(
                "http://localhost:5173",
                "http://localhost:5174",
                "http://localhost:5175");
            policy.AllowAnyHeader();
            policy.AllowAnyMethod();
            policy.AllowCredentials();
            policy.SetIsOriginAllowedToAllowWildcardSubdomains();
        }));


    var app = builder.Build();
    
    if (app.Environment.IsDevelopment())
        //app.InitializeDatabase<OrderingDbContext>();
        app.SeedDatabase();

    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("v3/swagger.json", "Orders API V3");
        c.OAuthClientId("swagger");
    });

    // Custom middlewares
    app.UseExceptionHandling();

    app.UseCors("CorsPolicy");
    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllers();
    app.MapGrpcService<HotOrdersService>();
    app.MapHub<OrdersHub>("/hubs/ordershub");
    app.Run();
}
#region Catch

catch (Exception ex)
{
    Log.Fatal(ex, "Fatal application error.");
}
finally
{
    Log.CloseAndFlush();
}

#endregion

public partial class Program { }