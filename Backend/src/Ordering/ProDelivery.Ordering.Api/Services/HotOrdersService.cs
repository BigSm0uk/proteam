﻿using Grpc.Core;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.Core.Abstractions;

namespace ProDelivery.Ordering.Api.Services;

public class HotOrdersService : HotOrders.HotOrdersBase
{
    private readonly IRedisRepository<OrderDto> _redisRepository;

    public HotOrdersService(IRedisRepository<OrderDto> redisRepository)
    {
        _redisRepository = redisRepository;
    }

    public override async Task<OrdersListReply> GetOrders(OrdersListRequest request, ServerCallContext context)
    {
        var ordersListReply = new OrdersListReply();
        var orders = await _redisRepository.GetAllAsAsync<OrdersListItem>();
        ordersListReply.Orders.AddRange(orders);
        return ordersListReply;
    }

    public override async Task<OrderReply> GetOrder(OrderRequest request, ServerCallContext context)
    {
        var orders = await _redisRepository.GetAsAsync<OrderReply>(request.Id.ToString());
        return orders;
    }
}