﻿using IdentityModel;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;

namespace ProDelivery.Ordering.Core.SignalR;

public class UserIdProvider : IUserIdProvider
{
    public string GetUserId(HubConnectionContext connection)
    {
        var httpContext = connection.GetHttpContext();
        var user = httpContext.Request.Query["userId"].ToString();
        return user;
    }
}
