﻿using MassTransit;
using MediatR;
using ProDelivery.Contracts;
using ProDelivery.Ordering.Core.Application;
using ProDelivery.Ordering.Core.Domain;

namespace Ordering.Api.Integration;

internal class OrderStatusChangedConsumer : IConsumer<OrderStatusChangedDto>
{
    private readonly IMediator _mediator;

    public OrderStatusChangedConsumer(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Consume(ConsumeContext<OrderStatusChangedDto> context)
    {
        await _mediator.Send(new UpdateOrderStatusCommand
        {
            Id = context.Message.Id,
            Status = Enum.Parse<OrderStatus>(context.Message.Status),
            IsFromShippingApi = true
        });
    }
}

internal class OrderStatusChangedConsumerDefinition 
    : ConsumerDefinition<OrderStatusChangedConsumer>
{
    public OrderStatusChangedConsumerDefinition()
    {
        EndpointName = MessageQueues.OrderStatusChangedQueueName;
    }
}