﻿using IdentityModel;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProDelivery.Ordering.Core.Application;
using ProDelivery.Ordering.Core.Domain;
using System.Security.Claims;

namespace ProDelivery.Ordering.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
[Authorize("Customer")]
public class OrdersController : ControllerBase
{
    private readonly IMediator _mediator;

    public string? UserRole => User.FindFirstValue(JwtClaimTypes.Role);
    public string? UserId => User.FindFirstValue(JwtClaimTypes.Subject);

    public OrdersController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Создать новый заказ.
    /// </summary>
    /// <param name="request" example='
    /// {
    ///  "title": "Краткий заголовок (до 40 символов)",
    ///  "description": "Описание заказа (до 250 символов)",
    ///  "deliveryCost": 500
    /// }
    /// '></param>
    /// <returns>Возвращает Id заказа в базе данных.</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Guid))]
    public async Task<IActionResult> CreateOrderAsync([FromBody] CreateOrderCommand request)
    {
        if (UserRole == "Customer" && UserId != null)
            request = request with { UserId = Guid.Parse(this.UserId) };

        var response = await _mediator.Send(request);
        return Ok(response);
    }

    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    public async Task<IActionResult> ChangeOrderStatusAsync(Guid id, OrderStatus status)
    {
        if (UserId is null)
            return Forbid();

        var request = new UpdateOrderStatusCommand
        {
            Id = id,
            UserId = Guid.Parse(this.UserId),
            Status = status,
            IsFromShippingApi = false
        };
        await _mediator.Send(request);

        return Accepted();
    }

    /// <summary>
    /// Найти заказы согласно условиям фильтрации.
    /// </summary>
    /// <param name="request">Фильтрирующие параметры запроса.</param>
    /// <returns>Возвращает массив кратких сведений о заказе.</returns>
    [HttpGet]
    public async Task<ActionResult> GetOrderListAsync([FromQuery] GetOrderListQuery request)
    {
        var sub = User.FindFirstValue(JwtClaimTypes.Subject);
        if (sub != null)
            request = request with { CustomerId = Guid.Parse(sub) };

        var response = await _mediator.Send(request);
        return Ok(response);
    }

    /// <summary>
    /// Найти заказ по id.
    /// </summary>
    /// <param name="id" example='00000000-0000-0000-0000-000000000010'>Id заказа в базе данных.</param>
    /// <returns>Возвращает массив кратких сведений о заказе.</returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult> GetOrderAsync([FromRoute] Guid id)
    {
        if (UserId is null)
            return Forbid();

        var request = new GetOrderQuery { OrderId = id };
        var response = await _mediator.Send(request);
        return Ok(response);
    }

    /// <summary>
    /// Удалитьзаказ.
    /// </summary>
    /// <param name="id">id заказа</param>
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> DeleteOrderAsync([FromRoute] Guid id)
    {
        if (UserId is null)
            return Forbid();

        var request = new DeleteOrderCommand
        {
            OrderId = id,
            UserId = Guid.Parse(this.UserId)
        };

        await _mediator.Send(request);
        return NoContent();
    }

}
