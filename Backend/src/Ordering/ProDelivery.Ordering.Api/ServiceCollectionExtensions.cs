﻿using IdentityServer4.AccessTokenValidation;
using MassTransit;
using Microsoft.OpenApi.Models;
using Ordering.Api.Integration;
using System.Reflection;

namespace ProDelivery.Ordering.DependencyInjection;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Регистрирует Grpc.
    /// </summary>
    public static IServiceCollection AddGrpcApi(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddGrpc();
        services.AddGrpcReflection();
        services.AddGrpcSwagger();

        return services;
    }
        
    /// <summary>
    /// Регистрирует RabbitMq.
    /// </summary>
    public static IServiceCollection AddMassTransitWithRabbitMq(this IServiceCollection services,
        IConfiguration configuration)
    {
        services
            .AddMassTransit(options =>
            {
                options.AddConsumer<OrderStatusChangedConsumer>(
                    typeof(OrderStatusChangedConsumerDefinition));
                options.UsingRabbitMq((context, config) =>
                {
                    config.Host(
                        host: configuration.GetConnectionString("RabbitMqHost") ?? "localhost",
                        virtualHost: "/",
                        configure: h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });
                    config.ConfigureEndpoints(context);
                });
            });

        return services;
    }

    /// <summary>
    /// Регистрирует аутентификацию и настраивает авторизацию.
    /// </summary>
    public static IServiceCollection AddIdentityServer(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddAuthentication(options =>
        {
            options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
            options.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
        })
            .AddIdentityServerAuthentication(options =>
            {
                options.ApiName = "orderingApi";
                options.Authority = configuration.GetConnectionString("IdentityServerUrl");
                options.RequireHttpsMetadata = false;
                options.LegacyAudienceValidation = true;
            });

        services.AddAuthorization(opt =>
        {
            opt.AddPolicy("Customer", policy =>
            {
                policy.RequireAuthenticatedUser();
                policy.RequireClaim("scope", "customerApi");
                policy.RequireClaim("role", "Customer");
            });
        });

        return services;
    }

    /// <summary>
    /// Регистрирует Swagger.
    /// </summary>
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v3", new OpenApiInfo { Title = "Orders API", Version = "v3" });
            c.SchemaFilter<EnumSchemaFilter>();
            var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlFullPath = Path.Combine(AppContext.BaseDirectory, xmlFileName);
            c.IncludeXmlComments(xmlFullPath);
            c.IncludeGrpcXmlComments(xmlFullPath, includeControllerXmlComments: true);
            c.AddSecurityDefinition("oauth2",
                new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Password = new OpenApiOAuthFlow
                        {
                            //AuthorizationUrl = new Uri("https://localhost:6001/connect/authorize"),
                            TokenUrl = new Uri("http://localhost:6001/connect/token"),
                            Scopes = new Dictionary<string, string>
                            {
                                { "customerApi", "Customer API" }
                            },
                        }
                    },
                });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "oauth2"
                        },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }
            });

            //c.AddSecurityDefinition($"AuthToken",
            //    new OpenApiSecurityScheme
            //    {
            //        In = ParameterLocation.Header,
            //        Type = SecuritySchemeType.Http,
            //        BearerFormat = "JWT",
            //        Scheme = "bearer",
            //        Name = "Authorization",
            //        Description = "Authorization token"
            //    });

            //c.AddSecurityRequirement(new OpenApiSecurityRequirement
            //{
            //    {
            //        new OpenApiSecurityScheme
            //        {
            //            Reference = new OpenApiReference
            //            {
            //                Type = ReferenceType.SecurityScheme,
            //                Id = $"AuthToken"
            //            }
            //        },
            //        new string[] { }
            //    }
            //});
        });
        return services;
    }
}