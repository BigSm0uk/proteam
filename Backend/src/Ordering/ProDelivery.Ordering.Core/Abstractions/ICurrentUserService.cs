﻿namespace ProDelivery.Ordering.Core.Abstractions;

public interface ICurrentUserService
{
    Guid UserId { get; }
}
