﻿using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Abstractions;

public interface IEfRepository<T> : IRepository<T> where T : BaseEntity
{
    public IQueryable<T> GetAll();
    Task SaveChangesAsync();
}
