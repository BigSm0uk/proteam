﻿using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Abstractions;

public interface IOrderRepository : IEfRepository<Order>
{
    public Order Find(
        Guid? userId = null,
        string? title = null,
        string? description = null,
        OrderStatus? status = null);

    IEnumerable<Order> FindAll(
        Guid? userId = null, 
        string? title = null, 
        string? description = null, 
        OrderStatus? status = null);

    Task<IEnumerable<Order>> FindAllAsync(
        string? term = null,
        Guid? userId = null, 
        string? title = null, 
        string? description = null, 
        OrderStatus? status = null);
}
