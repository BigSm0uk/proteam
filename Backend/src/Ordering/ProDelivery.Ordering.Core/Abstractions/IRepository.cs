﻿using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Abstractions;

public interface IRepository<T> where T : BaseEntity
{
    public T? GetById(Guid id);

    public Task<T?> GetByIdAsync(Guid id);

    public Guid Create(T entity);

    public Task<Guid> CreateAsync(T entity);

    public void Update(T entity);

    public Task UpdateAsync(T entity);

    public void Delete(Guid id);

    public Task DeleteAsync(Guid id);
}
