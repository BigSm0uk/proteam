﻿namespace ProDelivery.Ordering.Core.Domain;

public class Order : BaseEntity
{
    /// <summary>
    /// Заголовок заказа
    /// </summary>
    public string? Title { get; private set; }

    /// <summary>
    /// Описание заказа
    /// </summary>
    public string Description { get; private set; }

    /// <summary>
    /// Стоимость доставки
    /// </summary>
    public double DeliveryCost { get; private set; }

    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public Guid UserId { get; private set; }

    /// <summary>
    /// Статус заказа
    /// </summary>
    public OrderStatus Status { get; private set; }

    /// <summary>
    /// Дата создания заказа
    /// </summary>
    public DateTimeOffset CreateDate { get; private set; }

    public Order(Guid userId, string description, double deliveryCost, string? title = null)
    {
        // TODO: Вынести 20 в константы 
        Title = title ?? description[..(description.Length < 20 ? description.Length : 20)];
        Description = description;
        DeliveryCost = deliveryCost;
        UserId = userId;
        Status = OrderStatus.Created;
        CreateDate = DateTimeOffset.UtcNow;
    }

    /// <summary>
    /// Меняет статус заказа.
    /// </summary>
    /// <param name="status">Новый статус</param>
    public bool TryChangeStatus(OrderStatus? status)
    {
        if (!status.HasValue || Status == status) return false;

        switch (status)
        {            
            case OrderStatus.Accepted:
                if(Status == OrderStatus.Created)
                {
                    Status = OrderStatus.Accepted;
                    return true;
                } 
                return false;
            case OrderStatus.Loaded:
                if(Status == OrderStatus.Accepted || Status == OrderStatus.Suspended)
                {
                    Status = OrderStatus.Loaded;
                    return true;
                }
                return false;
            case OrderStatus.Canceled:
                if (Status != OrderStatus.Delivered 
                    || Status != OrderStatus.ReturnedToSender)
                {
                    Status = OrderStatus.Canceled;
                    return true;
                }
                return false;
            case OrderStatus.ReturnedToSender:
                if (Status != OrderStatus.Delivered)
                {
                    Status = OrderStatus.ReturnedToSender;
                    return true;
                }
                return false;
            case OrderStatus.Suspended:
                if (Status == OrderStatus.Accepted 
                    || Status == OrderStatus.Loaded)
                {
                    Status = OrderStatus.Suspended;
                    return true;
                }
                return false;
            case OrderStatus.Delivered:
                if (Status == OrderStatus.Loaded)
                {
                    Status = OrderStatus.Delivered;
                    return true;
                }
                return false;
            default: return false;
        }
    }

    /// <summary>
    /// Меняет данные заказа
    /// </summary>
    /// <param name="title">Новый заголовок</param>
    /// <param name="description">Новое описание</param>
    /// <param name="cost">Новая стоимость доставки</param>
    /// <returns>Результат изменения данных заказа</returns>
    /// <exception cref="Exception">Если поле стоимости не пустое и не удалось привести его к вещественному положительному числу выбрасывается исключение </exception>
    public bool TryUpdateOrder(string? title, string? description, string? cost)
    {
        //проверяем что все значения не пустые или null
        if (String.IsNullOrEmpty(title) &&
            String.IsNullOrEmpty(description) &&
            String.IsNullOrEmpty(cost)) return false;

        if (!String.IsNullOrEmpty(title)) Title = title;
        if (!String.IsNullOrEmpty(description)) Description = description;
        if (!String.IsNullOrEmpty(cost))
        {
            if (Double.TryParse(cost, out double value) && value < 0) DeliveryCost = value;
            else throw new Exception("Incorrect cost value");
        }
        return true;
    }

#pragma warning disable CS8618
    protected Order() { }
#pragma warning restore CS8618
}
