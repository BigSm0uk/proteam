﻿namespace ProDelivery.Ordering.Core.Domain;

public enum OrderStatus
{
    Created,
    Accepted,
    Canceled,
    Suspended,
    ReturnedToSender,
    Loaded,
    Delivered
} 
