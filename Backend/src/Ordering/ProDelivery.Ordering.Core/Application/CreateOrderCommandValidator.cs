﻿using FluentValidation;

namespace ProDelivery.Ordering.Core.Application;

public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
{
    public CreateOrderCommandValidator()
    {
        RuleFor(x => x.Title)
            .MaximumLength(40);
        RuleFor(x => x.Description)
            .NotEmpty()
            .MaximumLength(250);
        RuleFor(x => x.DeliveryCost)
            .GreaterThan(0.0);
    }
}
