﻿using AutoMapper;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using ProDelivery.Contracts;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;
using ProDelivery.Ordering.Core.Exceptions;
using ProDelivery.Ordering.Core.SignalR;

namespace ProDelivery.Ordering.Core.Application;

public class UpdateOrderStatusCommandHandler : IRequestHandler<UpdateOrderStatusCommand>
{
    private readonly IOrderRepository _orderRepository;
    private readonly ICurrentUserService _currentUserService;
    private readonly IBus _bus;
    private readonly IHubContext<OrdersHub> _hub;
    private readonly Dictionary<OrderStatus, string> congruenceOrderDeliveryStatus
            = new Dictionary<OrderStatus, string> {
                { OrderStatus.Canceled, "Canceled" },
                { OrderStatus.Suspended, "Suspended" } };

    public UpdateOrderStatusCommandHandler(IOrderRepository orderRepository,
        ICurrentUserService currentUserService,
        IBus bus, IHubContext<OrdersHub> hub)
    {
        _orderRepository = orderRepository;
        _currentUserService = currentUserService;
        _bus = bus;
        _hub = hub;
    }

    public async Task Handle(UpdateOrderStatusCommand request, CancellationToken cancellationToken)
    {
        var order = await _orderRepository.GetByIdAsync(request.Id)
            ?? throw new NotFoundException(nameof(Order), request.Id);

        //проверяем права на изменение статуса по следующим условиям:
        //1)если запрос пришел не из микросервиса то id владельца заказа и пользователя должны совпадать
        //2)если запрос пришел не из микросервиса то статус не должен быть принят или отгружен (т.к. эти статусы может устанавливать только курьер)
        if ((order.UserId != request.UserId && !request.IsFromShippingApi)
            || (!request.IsFromShippingApi && (request.Status == OrderStatus.Accepted
            || request.Status == OrderStatus.Loaded)))
        {
            throw new InvalidRightmentsException(request.UserId, order.Id);
        }

        if (!order.TryChangeStatus(request.Status))
            throw new InvalidOrderStatusException(request.Status, order.Id);

        await _orderRepository.UpdateAsync(order);
        await _hub.Clients.User("Customer").SendAsync("orderUpdated");

        if (!request.IsFromShippingApi)
        {
            var sendEndpoint = await _bus.GetSendEndpoint(MessageQueues.DeliveryStatusChangedQueueUri);
            await sendEndpoint.Send(new DeliveryStatusChangedDto(request.Id, congruenceOrderDeliveryStatus[request.Status.Value]));
        }
    }
}

