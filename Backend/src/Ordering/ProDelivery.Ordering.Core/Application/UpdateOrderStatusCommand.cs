﻿using MediatR;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public record UpdateOrderStatusCommand : IRequest
{
    public Guid Id { get; init; }    
    
    public Guid? UserId { get; set; }

    public OrderStatus? Status { get; init; }

    public bool IsFromShippingApi { get; init; }
}
