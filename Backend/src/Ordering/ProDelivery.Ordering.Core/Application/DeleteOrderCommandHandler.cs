﻿using AutoMapper;
using MediatR;
using ProDelivery.Ordering.Core.Abstractions;

namespace ProDelivery.Ordering.Core.Application;

public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, Unit>
{
    private readonly IOrderRepository _orderRepository;

    public DeleteOrderCommandHandler(IOrderRepository orderRepository)
        => _orderRepository = orderRepository;

    public async Task<Unit> Handle(DeleteOrderCommand request, CancellationToken _)
    {
        await _orderRepository.DeleteAsync(request.OrderId);
        return Unit.Value;
    }
}
