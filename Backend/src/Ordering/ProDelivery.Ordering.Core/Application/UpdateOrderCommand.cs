﻿using MediatR;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public record UpdateOrderCommand : IRequest<Order>
{
    public Guid Id { get; init; }

    public string? Title { get; init; }

    public string? Description { get; init; }

    public double? DeliveryCost { get; init; }

    public Guid? CourierId { get; init; }

    public OrderStatus? Status { get; init; }

    public bool IsFromShippingApi { get; init; }
}
