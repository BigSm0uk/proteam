﻿using MediatR;

namespace ProDelivery.Ordering.Core.Application
{
    public record GetOrderQuery : IRequest<OrderDetailDto>
    {
        public Guid OrderId { get; init; }
    }
}