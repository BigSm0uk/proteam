﻿using MediatR;

namespace ProDelivery.Ordering.Core.Application
{
    public record DeleteOrderCommand : IRequest<Unit>
    {
        public Guid OrderId { get; init; }
        public Guid UserId { get; init; }
    }
}