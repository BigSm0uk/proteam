﻿using AutoMapper;
using MassTransit;
using MediatR;
using ProDelivery.Contracts;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;
using ProDelivery.Ordering.Core.Exceptions;

namespace ProDelivery.Ordering.Core.Application;

public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, Order>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;
    private readonly IBus _bus;
    private readonly Dictionary<OrderStatus, string> congruenceOrderDeliveryStatus
            = new Dictionary<OrderStatus, string> {
                { OrderStatus.Canceled, "Canceled" },
                { OrderStatus.Suspended, "Suspended" } };

    public UpdateOrderCommandHandler(IOrderRepository orderRepository,
        IMapper mapper,
        ICurrentUserService currentUserService,
        IBus bus)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _currentUserService = currentUserService;
        _bus = bus;
    }

    public async Task<Order> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
    {
        var order = await _orderRepository.GetByIdAsync(request.Id)
            ?? throw new NotFoundException(nameof(Order), request.Id);                
        
        //реализовать        

        await _orderRepository.SaveChangesAsync();


        return order;
    }
}
