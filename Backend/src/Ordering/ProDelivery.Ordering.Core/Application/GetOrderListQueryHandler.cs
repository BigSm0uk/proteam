﻿using AutoMapper;
using MediatR;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public class GetOrderListQueryHandler
    : IRequestHandler<GetOrderListQuery, IEnumerable<OrderShortDto>>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrderListQueryHandler(
        IOrderRepository orderRepository,
        IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<IEnumerable<OrderShortDto>> Handle(GetOrderListQuery request, CancellationToken ct)
    {
        var orders = await _orderRepository.FindAllAsync(
            status: request.OrderStatus,
            userId: request.CustomerId,
            term: request.SearchTerm);

        var response = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderShortDto>>(orders);

        return response;
    }
}
