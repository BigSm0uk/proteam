﻿using AutoMapper;
using MediatR;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;
using ProDelivery.Ordering.Core.Exceptions;

namespace ProDelivery.Ordering.Core.Application;

public class GetOrderQueryHandler
    : IRequestHandler<GetOrderQuery, OrderDetailDto>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrderQueryHandler(
        IOrderRepository orderRepository,
        IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<OrderDetailDto> Handle(GetOrderQuery request, CancellationToken _)
    {
        var order = await _orderRepository.GetByIdAsync(request.OrderId);

        if (order is null)
            throw new NotFoundException(nameof(Order), request.OrderId);

        var response = _mapper.Map<Order, OrderDetailDto> (order);

        return response;
    }
}
