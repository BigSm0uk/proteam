﻿using AutoMapper;
using MediatR;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, Guid>
{
    private readonly IRepository<Order> _orderRepository;
    private readonly IMapper _mapper;
    private readonly IRedisRepository<OrderDto> _redisRepository;

    public CreateOrderCommandHandler(
        IRepository<Order> orderRepository,
        IMapper mapper,
        IRedisRepository<OrderDto> redisRepository)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _redisRepository = redisRepository;
    }

    public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken ct)
    {
        var order = _mapper.Map<CreateOrderCommand, Order>(request);
        await _orderRepository.CreateAsync(order);

        var dto = _mapper.Map<Order, OrderDto>(order);
        await _redisRepository.SetAsync(dto);

        return order.Id;
    }
}
