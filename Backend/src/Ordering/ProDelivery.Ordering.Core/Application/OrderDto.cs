﻿using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public record OrderShortDto
(
    Guid Id,
    string Title,
    double DeliveryCost,
    OrderStatus Status
);

public record OrderDetailDto
(
    Guid Id,
    string Title,
    string Description,
    double DeliveryCost,
    OrderStatus Status
);
