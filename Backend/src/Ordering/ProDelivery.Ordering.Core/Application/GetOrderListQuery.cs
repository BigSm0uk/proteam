﻿using MediatR;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core.Application;

public record GetOrderListQuery : IRequest<IEnumerable<OrderShortDto>>
{
    public Guid? CustomerId { get; init; }

    public Guid? CourierId { get; init; }

    public OrderStatus? OrderStatus { get; init; }

    public string? SearchTerm { get; init; }

}
