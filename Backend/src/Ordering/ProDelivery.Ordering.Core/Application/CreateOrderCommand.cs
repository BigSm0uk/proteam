﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ProDelivery.Ordering.Core.Application;

public record CreateOrderCommand : IRequest<Guid>
{
    [Required(AllowEmptyStrings = true)]
    public string? Title { get; init; }

    [Required]
    public string Description { get; init; } = null!;

    [Required]
    public double DeliveryCost { get; init; }

    [JsonIgnore]
    public Guid? UserId { get; init; }
}
