﻿using AutoMapper;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.Core.Application;
using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.Core;

public class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<CreateOrderCommand, Order>();
        CreateMap<Order, OrderShortDto>();
        CreateMap<Order, OrderDetailDto>();
    }
}

public class ContractsMappingProfile : Profile
{
    public ContractsMappingProfile()
    {
        CreateMap<Order, OrderDto>()
            ;
    }
}

