﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProDelivery.Ordering.Core;
using ProDelivery.Ordering.Core.Application;
using Serilog;
using System.Reflection;

namespace ProDelivery.Ordering.DependencyInjection;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Регистрирует зависимости ядра приложения.
    /// </summary>
    public static IServiceCollection AddApplicationCore(this IServiceCollection services,
         IConfiguration configuration)
    {
        services
            .AddAutoMapper(Assembly.GetExecutingAssembly())
            .AddMediatR(c => c.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()))
            .AddValidatorsFromAssembly(Assembly.GetExecutingAssembly())
            .AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>))
            .AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>))
            .AddLogging(c =>
            {
                c.ClearProviders();
                c.AddSerilog();
            });

        return services;
    }
}
