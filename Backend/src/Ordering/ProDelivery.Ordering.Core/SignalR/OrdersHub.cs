﻿using Microsoft.AspNetCore.SignalR;

namespace ProDelivery.Ordering.Core.SignalR;

public class OrdersHub : Hub
{
    public async override Task OnConnectedAsync()
    {
        await base.OnConnectedAsync();
        await Clients.Caller.SendAsync("Message", "Connected successfully!");
    }

    public async Task SendUser(string username, string message)
    {
        await Clients.User(username).SendAsync("Message", username, message);
    }
}

