﻿using ProDelivery.Ordering.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProDelivery.Ordering.Core.Exceptions
{
    public class InvalidRightmentsException : Exception
    {
        public InvalidRightmentsException(object userId, object subjectId) 
            : base($"User with ID number \"{userId}\" has not rights to work with object with ID number {subjectId}.") { }        
    }
}