﻿using ProDelivery.Ordering.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProDelivery.Ordering.Core.Exceptions
{
    public class InvalidOrderStatusException : Exception
    {
        public InvalidOrderStatusException(OrderStatus? status, object key) 
            : base($"Delivery with ID number \"{key}\" has wrong status {status} for this operation.") { }        
    }
}