﻿using MassTransit;
using MediatR;
using ProDelivery.Contracts;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Application;
using ProDelivery.Shipping.Core.Domain;
using ProDelivery.Shipping.Core.Exceptions;

namespace Shipping.Api.Integration;

internal class DeliveryStatusChangedConsumer : IConsumer<OrderStatusChangedDto>
{
    private readonly IMediator _mediator;
    private readonly IDeliveryRepository _deliveryRepository;

    public DeliveryStatusChangedConsumer(IMediator mediator, IDeliveryRepository deliveryRepository)
    {
        _mediator = mediator;
        _deliveryRepository = deliveryRepository;
    }

    public async Task Consume(ConsumeContext<OrderStatusChangedDto> context)
    {
        var deliveries = await _deliveryRepository.FindAllAsync(); //оптимизировать!
        var deliveryId = deliveries.FirstOrDefault(d => d.OrderId == context.Message.Id).Id;

        if(deliveryId == Guid.Empty || deliveryId == null)
        {
            throw new NotFoundException(nameof(Delivery), context.Message.Id);
        }
        
        await _mediator.Send(new UpdateDeliveryStatusCommand
        {
            DeliveryId = deliveryId,
            Status = Enum.Parse<DeliveryStatus>(context.Message.Status),
            IsFromOrderingApi = true
        });
    }
}

internal class DeliveryStatusChangedConsumerDefinition : ConsumerDefinition<DeliveryStatusChangedConsumer>
{
    public DeliveryStatusChangedConsumerDefinition()
    {
        EndpointName = MessageQueues.DeliveryStatusChangedQueueName;
    }
}