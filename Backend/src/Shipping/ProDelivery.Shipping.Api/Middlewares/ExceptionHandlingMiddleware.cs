﻿using FluentValidation;
using ProDelivery.Shipping.Core.Exceptions;

namespace ProDelivery.Shipping.Api.Middlewares;

public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(
        HttpContext context,
        ILogger<ExceptionHandlingMiddleware> logger,
        IWebHostEnvironment webHostEnvironment)
    {
        context.Request.EnableBuffering();
        try
        {
            await _next(context);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, exception.Message);

            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = exception switch
            {
                ValidationException => StatusCodes.Status400BadRequest,
                NotFoundException => StatusCodes.Status404NotFound,
                _ => StatusCodes.Status500InternalServerError
            };

            var errorMessageDetails = webHostEnvironment.IsProduction()
                ? "Ошибка! Пожалуйста, обратитесь к разработчикам данного сервиса."
                : exception.Message;

            var innerErrorType = exception.InnerException != null
                ? $" ({exception.InnerException.GetType().Name})"
                : "";

            var errorType = exception.GetType().Name + innerErrorType;
            context.Response.Headers.Append("Error", errorType);
            await context.Response.WriteAsJsonAsync(new
            {
                ErrorSource = exception.Source,
                ErrorType = errorType + "",
                ErrorMessage = errorMessageDetails
            });
        }
    }
}

public static class ExceptionHandlingMiddlewareExtensions
{
    public static void UseExceptionHandling(this IApplicationBuilder builder)
    {
        builder.UseMiddleware<ExceptionHandlingMiddleware>();
    }
}