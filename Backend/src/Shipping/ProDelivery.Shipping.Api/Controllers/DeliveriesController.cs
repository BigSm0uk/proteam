﻿using IdentityModel;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.Shipping.Core.Application;
using ProDelivery.Shipping.Core.Domain;
using System.Security.Claims;

namespace ProDelivery.Shipping.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
[Authorize("Courier")]
public class DeliveriesController : ControllerBase
{
    private readonly IMediator _mediator;

    public string? UserRole => User.FindFirstValue(JwtClaimTypes.Role);
    public string? UserId => User.FindFirstValue(JwtClaimTypes.Subject);

    public DeliveriesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Создать доставку (принять заказ к выполнению).
    /// </summary>
    /// <param name="id" example='00000000-0000-0000-0000-000000000010'>Id принимаемого заказа.</param>
    /// <returns>Возвращает Id доставки в базе данных.</returns>
    [HttpPost("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Guid))]
    public async Task<IActionResult> CreateDeliveryAsync(Guid id)
    {
        if (UserId is null)
            return Unauthorized();

        var request = new CreateDeliveryCommand
        {
            OrderId = id,
            CourierId = Guid.Parse(UserId)
        };

        var response = await _mediator.Send(request);
        return Ok(response);
    }

    /// <summary>
    /// Найти доставки согласно условиям фильтрации.
    /// </summary>
    /// <param name="request"></param>
    /// <returns>Возвращает массив кратких сведений о доставках.</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<DeliveryDto>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetDeliveryListAsync([FromQuery] GetDeliveryListQuery request)
    {
        if (UserId is null)
            return Unauthorized();

        request = request with { CustomerId = Guid.Parse(UserId) };

        var response = await _mediator.Send(request);
        return Ok(response);
    }

    /// <summary>
    /// Получить данные по доставке по id.
    /// </summary>
    /// <param name="id">Id доставки в базе данных.</param>
    /// <returns>Возвращает сведения по доставке.</returns>
    [HttpGet("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DeliveryDto))]
    public async Task<IActionResult> GetDeliveryAsync([FromRoute] Guid id)
    {
        var request = new GetDeliveryQuery { DeliveryId = id };

        var response = await _mediator.Send(request);

        return Ok(response);
    }

    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    public async Task<IActionResult> ChangeDeliveryStatusAsync(Guid id, DeliveryStatus status)
    {
        if (UserId is null)
            return Unauthorized();

        var request = new UpdateDeliveryStatusCommand
        {
            DeliveryId = id,
            CourierId = Guid.Parse(UserId),
            Status = status,
            IsFromOrderingApi = false
        };
        await _mediator.Send(request);

        return Accepted();
    }
}
