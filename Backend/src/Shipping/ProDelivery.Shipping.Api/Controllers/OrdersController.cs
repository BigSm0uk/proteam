﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProDelivery.Shipping.Core.Application;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
//[Authorize("Courier")]
public class OrdersController : ControllerBase
{
    private readonly IMediator _mediator;

    public OrdersController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Найти доступные заказы.
    /// </summary>
    /// <returns>Возвращает массив кратких сведений о заказах.</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<OrderShortDto>))]
    public async Task<IActionResult> GetAvailableOrdersAsync()
    {
        var response = await _mediator.Send(new GetOrderListQuery
        {
            OrderStatus = DeliveryStatus.Created
        });
        return Ok(response);
    }

    /// <summary>
    /// Достать детали заказа по id.
    /// </summary>
    [HttpGet("{orderId:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderDetailDto))]
    public async Task<IActionResult> GetOrderDetailsAsync(Guid orderId)
    {
        var response = await _mediator.Send(new GetOrderDetailsQuery
        {
            OrderId = orderId
        });
        return Ok(response);
    }
}