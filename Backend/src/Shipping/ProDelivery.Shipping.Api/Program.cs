using ProDelivery.Shipping.Api;
using ProDelivery.Shipping.DataAccess;
using ProDelivery.Shipping.DependencyInjection;
using ProDelivery.Shipping.Core.Abstractions;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System.Text.Json.Serialization;
using ProDelivery.Shipping.Api.Middlewares;

#region Logging

Log.Logger = new LoggerConfiguration()
.MinimumLevel.Debug()
.MinimumLevel.Override("System", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
.MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
.MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Information)
.Enrich.FromLogContext()
.WriteTo.Console(theme: AnsiConsoleTheme.Literate)
.WriteTo.File($"Logs/{DateTime.Now.Ticks}-shipping.log")
.CreateLogger();

#endregion
try
{
    var builder = WebApplication.CreateBuilder(args);
    var configuration = builder.Configuration;
    var services = builder.Services;

    services
        .AddControllers(options
            => options.SuppressAsyncSuffixInActionNames = true)
        .AddJsonOptions(options
            => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

    services
        .AddApplicationCore(configuration)
        .AddDataAccess(configuration)
        .AddMassTransitWithRabbitMq(configuration)
        .AddSingleton<ICurrentUserService, CurrentUserService>()
        .AddHttpContextAccessor()
        .AddIdentityServer(configuration)
        .AddSwagger()
        .AddMemoryCache()
        .AddCors(cors => cors.AddPolicy("AllowAll", policy =>
        {
            policy.AllowAnyOrigin();
            policy.AllowAnyHeader();
            policy.AllowAnyMethod();
            policy.SetIsOriginAllowedToAllowWildcardSubdomains();
        }));

    var app = builder.Build();

    app.InitializeDatabase<ShippingDbContext>();

    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("v3/swagger.json", "Orders API V3");
        c.OAuthClientId("console");
        c.OAuthClientSecret("secret");
    });

    // Custom middlewares
    app.UseExceptionHandling();

    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllers();
    app.Run();
}
#region Catch

catch (Exception ex)
{
    Log.Fatal(ex, "Fatal application error.");
}
finally
{
    Log.CloseAndFlush();
}

#endregion

public partial class Program { }