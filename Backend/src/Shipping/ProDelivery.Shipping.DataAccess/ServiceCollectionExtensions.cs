﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.DataAccess;

namespace ProDelivery.Shipping.DependencyInjection;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Регистрирует подключение к базе данных.
    /// </summary>
    public static IServiceCollection AddDataAccess(this IServiceCollection services, 
        IConfiguration configuration)
    {
        services
            .AddScoped<DbContext, ShippingDbContext>()
            .AddScoped(typeof(IEfRepository<>), typeof(EfRepository<>))
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
            .AddScoped(typeof(IDeliveryRepository), typeof(DeliveryRepository))
            .AddDbContext<ShippingDbContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("ShippingDb");
                if (connectionString == null)
                    options.UseSqlite("Data Source=ShippingApi.db");
                else
                {
                    options.UseNpgsql(connectionString);
                    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
                }
            })
            .AddScoped(typeof(IRedisRepository<>), typeof(RedisRepository<>))
            .Configure<RedisRepositoryOptions>(
                x => x.Configuration = configuration.GetConnectionString("RedisHost"));

        return services;
    }
}
