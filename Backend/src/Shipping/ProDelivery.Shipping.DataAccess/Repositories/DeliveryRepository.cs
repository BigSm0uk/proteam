﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;
using System.Collections.Generic;

namespace ProDelivery.Shipping.DataAccess;

public class DeliveryRepository : EfRepository<Delivery>, IDeliveryRepository
{
    private readonly DbSet<Delivery> _deliveries;

    public DeliveryRepository(ShippingDbContext context) : base(context)
    {
        _deliveries = context.Deliveries;
    }

    // TODO: доделать фильтрацию
    public async Task<IEnumerable<Delivery>> FindAllAsync(
        string? term = null, 
        Guid? CourierId = null, 
        Guid? CustomerId = null, 
        string? orderDescription = null)
    {
        var query = _deliveries.AsQueryable();

        if (term != null)
            query = query.Where(x => x.OrderDescription.Contains(term));
        //if (status.HasValue)
        //    query = query.Where(x => x.Status == status);

        return await query.ToListAsync();
    }
}
