﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Shipping.Core.Exceptions;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.DataAccess;

public class EfRepository<T> : IEfRepository<T> where T : BaseEntity
{
    private readonly DbContext _context;
    private readonly DbSet<T> _set;

    public EfRepository(DbContext context)
    {
        _context = context;
        _set = context.Set<T>();
    }

    public Guid Create(T entity)
    {
        _set.Add(entity);
        _context.SaveChanges();
        return entity.Id;
    }

    public async Task<Guid> CreateAsync(T entity)
    {
        await _set.AddAsync(entity);
        await _context.SaveChangesAsync();
        return entity.Id;
    }

    public T? GetById(Guid id)
    {
        return _set.Find(new object[] { id });
    }

    public async Task<T?> GetByIdAsync(Guid id)
    {
        return await _set.FindAsync(new object[] { id });
    }

    public void Delete(Guid id)
    {
        var entity = _set.Find(new object[] { id });

        if (entity != null)
            _set.Remove(entity);
    }

    public async Task DeleteAsync(Guid id)
    {
        var entity = await _set.FindAsync(new object[] { id });

        if (entity != null)
            _set.Remove(entity);
    }

    public virtual IQueryable<T> GetAll()
    {

        return _set;
    }

    public async Task SaveChangesAsync()
    {
        await _context.SaveChangesAsync();
    }

    public void Update(T entity)
    {
        _set.Update(entity);
    }

    public async Task UpdateAsync(T entity)
    {
        throw new NotSupportedException();
    }
}
