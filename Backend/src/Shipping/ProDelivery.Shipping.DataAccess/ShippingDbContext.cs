﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Shipping.Core.Domain;
using System.Reflection;

namespace ProDelivery.Shipping.DataAccess;

public class ShippingDbContext : DbContext
{
    public DbSet<Delivery> Deliveries { get; private set; }

    public ShippingDbContext(DbContextOptions<ShippingDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(builder);
    }
}
