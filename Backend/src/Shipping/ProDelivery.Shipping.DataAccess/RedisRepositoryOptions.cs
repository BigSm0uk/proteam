﻿using Microsoft.Extensions.Options;

namespace ProDelivery.Shipping.DataAccess;

/// <summary>
/// Configuration options for <see cref="RedisCache"/>.
/// </summary>
public class RedisRepositoryOptions : IOptions<RedisRepositoryOptions>
{
    /// <summary>
    /// The configuration used to connect to Redis.
    /// </summary>
    public string? Configuration { get; set; }

    RedisRepositoryOptions IOptions<RedisRepositoryOptions>.Value
    {
        get { return this; }
    }
}
