﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ProDelivery.Shipping.DependencyInjection;

public static class ApplicationBuilderExtensions
{
    /// <summary>
    /// Initializes development database.
    /// </summary>
    /// <typeparam name="TContext">Type of db-context</typeparam>
    /// <param name="builder">Application builder</param>
    /// <param name="seedDb">Delegate for seeding database</param>
    /// <returns></returns>
    public static IApplicationBuilder InitializeDatabase<TContext>(
        this IApplicationBuilder builder, Action<TContext>? seedDb = null)
        where TContext : DbContext
    {
        using var scope = builder.ApplicationServices.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<TContext>();
        if (context.Database.EnsureCreated())
            seedDb?.Invoke(context);

        return builder;
    }
}

