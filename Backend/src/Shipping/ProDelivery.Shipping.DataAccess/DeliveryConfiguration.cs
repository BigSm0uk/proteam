﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.DataAccess;

public class DeliveryConfiguration : IEntityTypeConfiguration<Delivery>
{
    public void Configure(EntityTypeBuilder<Delivery> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasIndex(x => x.Id)
            .IsUnique();

        builder.Property(x => x.OrderDescription)
            .HasMaxLength(250)
            .IsRequired();

        builder.Property(x => x.Cost)
            .IsRequired();

        builder.Property(x => x.CourierId)
            .IsRequired();

        builder.Property(x => x.Status)
            .IsRequired();

        builder.Property(x => x.CreateDate)
            .IsRequired();
    }
}
