﻿using MassTransit;
using MediatR;
using ProDelivery.Contracts;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;
using ProDelivery.Shipping.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDelivery.Shipping.Core.Application
{
    public class UpdateDeliveryStatusCommandHandler : IRequestHandler<UpdateDeliveryStatusCommand>
    {
        private readonly IDeliveryRepository _deliveryRepository;
        private readonly IBus _bus;
        private readonly Dictionary<DeliveryStatus, string> congruenceDeliveryOrderStatus 
            = new Dictionary<DeliveryStatus, string> { 
                { DeliveryStatus.Started, "Loaded" },
                { DeliveryStatus.Suspended, "Suspended" },
                { DeliveryStatus.Finished, "Delivered" },
                { DeliveryStatus.Canceled, "Canceled" },
                };

        public UpdateDeliveryStatusCommandHandler(IDeliveryRepository deliveryRepository, IBus bus)
        {
            _deliveryRepository = deliveryRepository;
            _bus = bus;
        }

        public async Task Handle(UpdateDeliveryStatusCommand request, CancellationToken cancellationToken)
        {
            var delivery = await _deliveryRepository.GetByIdAsync(request.DeliveryId)
            ?? throw new NotFoundException(nameof(Delivery), request.DeliveryId);

            if(delivery.CourierId != request.CourierId)
            {
                throw new InvalidRightmentsException(request.CourierId, request.DeliveryId);
            }

            if(delivery.TryChangeStatus(request.Status))
            {
                _deliveryRepository.Update(delivery);
                await _deliveryRepository.SaveChangesAsync();                
                
                if(!request.IsFromOrderingApi)
                {
                    var sendEndpoint = await _bus.GetSendEndpoint(MessageQueues.OrderStatusChangedQueueUri);
                    await sendEndpoint.Send(new OrderStatusChangedDto(delivery.OrderId, congruenceDeliveryOrderStatus[request.Status]));
                }                                
            }
            else throw new InvalidDeliveryStatusException(delivery.Status, delivery.Id);
        }        
    }
}
