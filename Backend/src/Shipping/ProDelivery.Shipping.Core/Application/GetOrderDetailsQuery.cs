﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace ProDelivery.Shipping.Core.Application;

public record GetOrderDetailsQuery : IRequest<OrderDetailDto>
{
    [Required]
    public Guid OrderId { get; init; }
}
