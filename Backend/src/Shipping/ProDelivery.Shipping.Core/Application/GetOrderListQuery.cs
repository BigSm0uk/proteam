﻿using MediatR;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public record GetOrderListQuery : IRequest<IEnumerable<OrderShortDto>>
{
    public Guid? CustomerId { get; init; }

    public Guid? CourierId { get; init; }

    public DeliveryStatus? OrderStatus { get; init; }

    public string? SearchTerm { get; init; }

}
