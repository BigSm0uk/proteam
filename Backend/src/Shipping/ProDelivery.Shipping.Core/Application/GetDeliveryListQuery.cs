﻿using MediatR;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public record GetDeliveryListQuery : IRequest<IEnumerable<DeliveryDto>>
{
    public Guid? CustomerId { get; init; }

    public Guid? CourierId { get; init; }

    public DeliveryStatus? DeliveryStatus { get; init; }

    public string? SearchTerm { get; init; }

}
