﻿using MediatR;

namespace ProDelivery.Shipping.Core.Application;

public record GetDeliveryQuery : IRequest<DeliveryDto>
{
    public Guid DeliveryId { get; init; }

}
