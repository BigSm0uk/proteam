﻿using FluentValidation;

namespace ProDelivery.Shipping.Core.Application;

public class CreateDeliveryCommandValidator : AbstractValidator<CreateDeliveryCommand>
{
    public CreateDeliveryCommandValidator()
    {
        RuleFor(x => x.OrderId)
            .NotEmpty();
        RuleFor(x => x.CourierId)
            .NotEmpty();
    }
}
