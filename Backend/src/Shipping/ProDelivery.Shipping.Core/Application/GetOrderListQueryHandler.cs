﻿using AutoMapper;
using MediatR;
using ProDelivery.Contracts.Redis;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public class GetOrderListQueryHandler
    : IRequestHandler<GetOrderListQuery, IEnumerable<OrderShortDto>>
{
    private readonly IDeliveryRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _userService;
    private readonly IRedisRepository<OrderDto> _redisRepository;

    public GetOrderListQueryHandler(
        IDeliveryRepository orderRepository,
        IMapper mapper,
        ICurrentUserService userService,
        IRedisRepository<OrderDto> redisRepository)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _userService = userService;
        _redisRepository = redisRepository;
    }

    public async Task<IEnumerable<OrderShortDto>> Handle(GetOrderListQuery request, CancellationToken ct)
    {
        return await _redisRepository.GetAllAsAsync<OrderShortDto>();
    }
}
