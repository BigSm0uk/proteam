﻿using MediatR;

namespace ProDelivery.Shipping.Core.Application;

public record CreateDeliveryCommand : IRequest<Guid>
{
    public Guid OrderId { get; set; }
    public Guid CourierId { get; set; }
}