﻿using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public record OrderShortDto
(
    Guid Id,
    string Title,
    double DeliveryCost,
    string Status
);

public record OrderDetailDto
(
    Guid Id,
    string Title,
    string Description,
    double DeliveryCost,
    string Status
);
