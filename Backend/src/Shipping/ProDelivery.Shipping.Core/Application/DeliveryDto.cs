﻿using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application
{
    public record DeliveryDto
    (
        Guid Id,
        string OrderDescription,
        double Cost,
        DeliveryStatus Status
    );
}