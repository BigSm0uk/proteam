﻿using AutoMapper;
using MediatR;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;
using ProDelivery.Shipping.Core.Exceptions;

namespace ProDelivery.Shipping.Core.Application;

public class GetDeliveryQueryHandler
    : IRequestHandler<GetDeliveryQuery, DeliveryDto>
{
    private readonly IDeliveryRepository _deliveryRepository;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _userService;

    public GetDeliveryQueryHandler(
        IDeliveryRepository deliveryRepository,
        IMapper mapper,
        ICurrentUserService userService)
    {
        _deliveryRepository = deliveryRepository;
        _mapper = mapper;
        _userService = userService;
    }

    public async Task<DeliveryDto> Handle(GetDeliveryQuery request, CancellationToken _)
    {
        var delivery = await _deliveryRepository.GetByIdAsync(request.DeliveryId);

        if (delivery == null)
            throw new NotFoundException(nameof(Delivery), request.DeliveryId);

        var response = _mapper.Map<Delivery, DeliveryDto>(delivery);

        return response;
    }
}
