﻿using MediatR;
using ProDelivery.Contracts.Redis;
using ProDelivery.Shipping.Core.Abstractions;

namespace ProDelivery.Shipping.Core.Application;

public class GetOrderDetailQueryHandler
    : IRequestHandler<GetOrderDetailsQuery, OrderDetailDto>
{
    private readonly IRedisRepository<OrderDto> _redisRepository;

    public GetOrderDetailQueryHandler(IRedisRepository<OrderDto> redisRepository)
    {
        _redisRepository = redisRepository;
    }

    public async Task<OrderDetailDto> Handle(GetOrderDetailsQuery request, CancellationToken ct)
    {
        return await _redisRepository.GetAsAsync<OrderDetailDto>(request.OrderId.ToString());
    }
}
