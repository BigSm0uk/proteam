﻿using MediatR;
using ProDelivery.Shipping.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDelivery.Shipping.Core.Application
{
    public record UpdateDeliveryStatusCommand : IRequest
    {
        public Guid DeliveryId { get; set; }
        public Guid? CourierId { get; set; }
        public DeliveryStatus Status { get; set; }
        public bool IsFromOrderingApi { get; set; }
    }
}
