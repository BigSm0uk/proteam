﻿using AutoMapper;
using MediatR;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public class GetDeliveryListQueryHandler
    : IRequestHandler<GetDeliveryListQuery, IEnumerable<DeliveryDto>>
{
    private readonly IDeliveryRepository _deliveryRepository;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _userService;

    public GetDeliveryListQueryHandler(
        IDeliveryRepository deliveryRepository,
        IMapper mapper,
        ICurrentUserService userService)
    {
        _deliveryRepository = deliveryRepository;
        _mapper = mapper;
        _userService = userService;
    }

    public async Task<IEnumerable<DeliveryDto>> Handle(GetDeliveryListQuery request, CancellationToken ct)
    {
        var ordersQuery = await _deliveryRepository.FindAllAsync(
            //status: request.DeliveryStatus,
            //userId: request.CustomerId,
            term: request.SearchTerm);

        var response = _mapper.Map<IEnumerable<Delivery>, IEnumerable<DeliveryDto>>(ordersQuery);

        return response;
    }
}
