﻿using AutoMapper;
using MassTransit;
using MediatR;
using ProDelivery.Contracts;
using ProDelivery.Contracts.Redis;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Application;

public class CreateDeliveryCommandHandler : IRequestHandler<CreateDeliveryCommand, Guid>
{
    private readonly IDeliveryRepository _deliveryRepository;
    private readonly IMapper _mapper;
    private readonly IRedisRepository<OrderDto> _redisRepository;
    private readonly IBus _bus;

    public CreateDeliveryCommandHandler(
        IDeliveryRepository deliveryRepository, 
        IMapper mapper, 
        IRedisRepository<OrderDto> redisRepository,
        IBus bus)
    {
        _deliveryRepository = deliveryRepository;
        _mapper = mapper;
        _redisRepository = redisRepository;
        _bus = bus;
    }

    public async Task<Guid> Handle(CreateDeliveryCommand request, CancellationToken ct)
    {
        OrderDto order = await _redisRepository.PopAsync(request.OrderId.ToString());
        
        // Создаем сущность доставки в БД 
        var delivery = new Delivery(
            courierId: request.CourierId,
            orderDescription: order.Description,
            orderId: request.OrderId,
            cost: order.DeliveryCost);

        await _deliveryRepository.CreateAsync(delivery);

        // TODO: Привязать доставку к сущности курьера 

        // Отправим инфу в реббит 
        var sendEndpoint = await _bus.GetSendEndpoint(MessageQueues.OrderStatusChangedQueueUri);
        await sendEndpoint.Send(new OrderStatusChangedDto(order.Id, "Accepted"));

        return delivery.Id;
    }
}
