﻿using ProDelivery.Shipping.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProDelivery.Shipping.Core.Exceptions
{
    public class InvalidDeliveryStatusException : Exception
    {
        public InvalidDeliveryStatusException(DeliveryStatus? status, object key) 
            : base($"Delivery with ID number \"{key}\" has wrong status {status} for this operation.") { }        
    }
}