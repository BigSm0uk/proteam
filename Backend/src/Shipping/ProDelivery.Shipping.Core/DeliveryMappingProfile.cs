﻿using AutoMapper;
using ProDelivery.Shipping.Core.Application;
using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core;

public class DeliveryMappingProfile : Profile
{
    public DeliveryMappingProfile()
    {
        CreateMap<Delivery, DeliveryDto>();
    }
}

