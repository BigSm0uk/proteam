﻿namespace ProDelivery.Shipping.Core.Domain;

public enum DeliveryStatus
{
    Created,
    Started,
    Finished,
    Suspended,
    Canceled,
}
