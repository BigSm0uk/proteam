﻿using ProDelivery.Shipping.Core.Exceptions;

namespace ProDelivery.Shipping.Core.Domain
{
    public class Delivery : BaseEntity
    {
        /// <summary>
        /// Описание заказа
        /// </summary>
        public string OrderDescription { get; private set; }

        /// <summary>
        /// Стоимость доставки.
        /// </summary>
        public double Cost { get; private set; }

        /// <summary>
        /// Идентификатор курьера.
        /// </summary>
        public Guid CourierId { get; private set; }

        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public Guid OrderId { get; private set; }

        /// <summary>
        /// Статус доставки.
        /// </summary>
        public DeliveryStatus Status { get; private set; }

        /// <summary>
        /// Дата инициализации доставки.
        /// </summary>
        public DateTimeOffset CreateDate { get; private set; }

        public Delivery(Guid courierId, Guid orderId, string orderDescription, double cost)
        {
            OrderDescription = orderDescription;
            Cost = cost;
            CourierId = courierId;
            OrderId = orderId;
            Status = DeliveryStatus.Created;
            CreateDate = DateTimeOffset.UtcNow;
        }

        /// <summary>
        /// Меняет статус заказа.
        /// </summary>
        /// <param name="status"></param>
        public bool TryChangeStatus(DeliveryStatus? status)
        {
            if (!status.HasValue) return false;

            switch (status)
            {                                  
                case DeliveryStatus.Started:
                    if (Status == DeliveryStatus.Created || Status == DeliveryStatus.Suspended)
                    {
                        Status = DeliveryStatus.Started;
                        return true;
                    }
                    return false;                    
                case DeliveryStatus.Suspended:
                    if (Status == DeliveryStatus.Started)
                    {
                        Status = DeliveryStatus.Suspended;
                        return true;
                    }
                    return false;                    
                case DeliveryStatus.Finished:
                    if (Status == DeliveryStatus.Started)
                    {
                        Status = DeliveryStatus.Finished;
                        return true;
                    }
                    return false;                    
                    case DeliveryStatus.Canceled:
                    if (Status == DeliveryStatus.Created 
                        || Status == DeliveryStatus.Suspended
                        || Status == DeliveryStatus.Started)
                    {
                        Status = DeliveryStatus.Canceled;
                        return true;
                    }
                    return false;
                    default: return false;
            }                        
        }

#pragma warning disable CS8618
        protected Delivery() { }
#pragma warning restore CS8618
    }
}
