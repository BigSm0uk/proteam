﻿using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Abstractions;

public interface IEfRepository<T> : IRepository<T> where T : BaseEntity
{
    public IQueryable<T> GetAll();
}
