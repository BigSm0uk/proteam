﻿using ProDelivery.Shipping.Core.Domain;

namespace ProDelivery.Shipping.Core.Abstractions;

public interface IDeliveryRepository : IRepository<Delivery>
{
    Task<IEnumerable<Delivery>> FindAllAsync(
        string? term = null,
        Guid? CourierId = null, 
        Guid? CustomerId = null, 
        string? orderDescription = null);
}
