﻿using ProDelivery.Contracts.Redis;

namespace ProDelivery.Shipping.Core.Abstractions;

public interface IRedisRepository<T> where T : IRedisDbContract
{
    Task<bool> DeleteAsync(string id);
    Task<IEnumerable<TMap>> GetAllAsAsync<TMap>();
    Task<TMap> GetAsAsync<TMap>(string v);
    Task<IEnumerable<T>> GetAllAsync();
    Task<T> GetAsync(string id);
    Task<T> PopAsync(string id);
    Task SetAsync(T value);
    Task SetRangeAsync(IEnumerable<T> values);
}