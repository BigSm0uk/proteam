﻿namespace ProDelivery.Shipping.Core.Abstractions;

public interface ICurrentUserService
{
    Guid UserId { get; }
}
