﻿using Microsoft.AspNetCore.Identity;
using System;

namespace ProDelivery.IdentityServer.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronym { get; set; }
        public string Role { get; set; }
    }
}
