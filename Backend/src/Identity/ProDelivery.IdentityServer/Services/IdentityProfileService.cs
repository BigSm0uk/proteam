﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using ProDelivery.IdentityServer.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProDelivery.IdentityServer.Services
{
    //public class IdentityProfileService : IProfileService
    //{
    //    public Task GetProfileDataAsync(ProfileDataRequestContext context)
    //    {
    //        var roleClaims = context.Subject.FindAll(JwtClaimTypes.Role);
    //        context.IssuedClaims.AddRange(roleClaims);
    //        return Task.CompletedTask;
    //    }

    //    public Task IsActiveAsync(IsActiveContext context)
    //    {
    //        return Task.CompletedTask;
    //    }
    //}

    /// <summary>
    /// Добавляет кастомные клеймы в токен 
    /// </summary>
    /// <remarks> 
    /// <see href="https://stackoverflow.com/questions/44761058/how-to-add-custom-claims-to-access-token-in-identityserver4 "/> 
    /// </remarks>
    public class IdentityProfileService : IProfileService
    {
        protected UserManager<ApplicationUser> _userManager;

        public IdentityProfileService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            var claims = new List<Claim>
            {
                new Claim(JwtClaimTypes.Role, user.Role),
            };

            context.IssuedClaims.AddRange(claims);
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            context.IsActive = (user != null);// && user.IsActive;
        }
    }
}
