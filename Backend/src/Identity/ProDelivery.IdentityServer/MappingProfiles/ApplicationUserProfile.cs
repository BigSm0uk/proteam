﻿using ProDelivery.IdentityServer.Models;
using AutoMapper;
using IdentityServerHost.Quickstart.UI;

namespace ProDelivery.IdentityServer.MappingProfiles
{
    public class ApplicationUserProfile : Profile
    {
        public ApplicationUserProfile()
        {
            CreateMap<CreateApplicationUserCommand, ApplicationUser>();
        }
    }
}
