﻿using ProDelivery.IdentityServer.Models;

namespace ProDelivery.IdentityServer.Data
{
    public class DataFactory
    {
        public static IEnumerable<ApplicationUser> ApplicationUsers => new List<ApplicationUser>()
        {
            new ApplicationUser()
            {
                Id = PreconfiguredCustomerIds[0],
                UserName = "user",
                Email = "user@email.com",
                EmailConfirmed = true,
                Role = ApplicationRoles.Customer
            },
            new ApplicationUser()
            {
                Id = PreconfiguredCourierIds[0],
                UserName = "cour",
                Email = "cour@email.com",
                EmailConfirmed = true,
                Role = ApplicationRoles.Courier
            },
            new ApplicationUser()
            {
                Id = PreconfiguredCustomerIds[1],
                UserName = "Customer",
                Email = "cust@om.er",
                EmailConfirmed = true,
                Role = ApplicationRoles.Customer
            },
            new ApplicationUser()
            {
                Id = PreconfiguredCourierIds[1],
                UserName = "Courier",
                Email = "cour@i.er",
                EmailConfirmed = true,
                Role = ApplicationRoles.Courier
            },
        };

        public static string[] PreconfiguredCustomerIds =
        {
            "00000000-0000-0000-0000-000000000001",
            "00000000-0000-0000-0000-000000000003",
            "00000000-0000-0000-0000-000000000005",
        };

        public static string[] PreconfiguredCourierIds =
        {
            "00000000-0000-0000-0000-000000000002",
            "00000000-0000-0000-0000-000000000004",
            "00000000-0000-0000-0000-000000000006",
        };
    }
}