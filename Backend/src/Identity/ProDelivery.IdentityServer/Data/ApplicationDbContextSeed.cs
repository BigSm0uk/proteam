﻿using IdentityModel;
using Microsoft.AspNetCore.Identity;
using ProDelivery.IdentityServer.Models;
using Serilog;
using System.Security.Claims;

namespace ProDelivery.IdentityServer.Data
{
    public class ApplicationDbContextSeed
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEnumerable<ApplicationUser> _users;

        public ApplicationDbContextSeed(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IEnumerable<ApplicationUser> users)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _users = users;
        }

        public void SeedData()
        {
            foreach (var user in _users)
            {
                AddUser(user);
            }
        }

        private void AddUser(ApplicationUser user)
        {
            if (_userManager.FindByNameAsync(user.UserName).Result != null)
            {
                Log.Debug($"User with login \"{user.UserName}\" already exists");
            }
            else
            {
                var result = _userManager.CreateAsync(user, user.UserName + "!1").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = _userManager.AddClaimsAsync(user, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                    new Claim(JwtClaimTypes.GivenName, $"{user.FirstName}"),
                    new Claim(JwtClaimTypes.FamilyName, $"{user.LastName}"),
                    new Claim(JwtClaimTypes.WebSite, $"http://{user.UserName}.com"),
                    new Claim(JwtClaimTypes.Role, user.Role)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                // TODO: может когда-нибудь разобраться с ролями 
                //result = _userManager.AddToRoleAsync(user, "customer").Result;
                //if (!result.Succeeded)
                //{
                //    throw new Exception(result.Errors.First().Description);
                //}
                Log.Debug($"User with login \"{user.UserName}\" created");
            }
        }

        private void AddRole(string roleName)
        {
            var customer = _roleManager.FindByNameAsync(roleName).Result;
            if (customer == null)
            {
                customer = new IdentityRole
                {
                    Name = roleName
                };
                var result = _roleManager.CreateAsync(customer).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Log.Debug("customer created");
            }
            else
            {
                Log.Debug("customer already exists");
            }
        }
    }
}
