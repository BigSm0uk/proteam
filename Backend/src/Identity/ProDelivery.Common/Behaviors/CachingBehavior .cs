﻿using EasyCaching.Core;
using EasyCaching.Core.Interceptor;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.Logging;

namespace ProDelivery.Common.Behaviors
{
    public class CachingBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
        , ICachingService<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IEasyCachingProvider _cachingProvider;
        private readonly ILogger<IEasyCachingProvider> _logger;

        public CachingBehavior(IEasyCachingProvider cachingProvider, ILogger<IEasyCachingProvider> logger)
        {
            _cachingProvider = cachingProvider;
            _logger = logger;
        }

        public Task<TResponse> Handle(TRequest request,
            RequestHandlerDelegate<TResponse> next,
            CancellationToken cancellationToken)
        {
            var c = _cachingProvider.GetCount();
            _logger.LogInformation(c.ToString());

            if (typeof(TRequest).Name.EndsWith("Query"))
            {
                return Task.FromResult( Get(request, next));
            }
            // &&??????????????????
            return next();
        }

        public TResponse Get(TRequest request, RequestHandlerDelegate<TResponse> next)
        {
            return next().Result;
        }

        public void Evict(TRequest request)
        {
            throw new NotImplementedException();
        }
    }

    public interface ICachingService<TRequest, TResponse>
    {
        [EasyCachingAble(Expiration = 10)]
        TResponse Get(TRequest request, RequestHandlerDelegate<TResponse> next);

        //[EasyCachingPut(CacheKeyPrefix = "AspectCore")]
        //string Put(string str);

        [EasyCachingEvict(IsBefore = true)]
        void Evict(TRequest request);
    }
}
