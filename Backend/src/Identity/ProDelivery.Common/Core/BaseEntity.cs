﻿namespace ProDelivery.Common.Core
{
    abstract public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
