﻿using Microsoft.EntityFrameworkCore.Storage;

namespace ProDelivery.Common.Core.IRepositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        public T? GetById(Guid id);

        public Task<T?> GetByIdAsync(Guid id);

        public Guid Create(T entity);

        public Task<Guid> CreateAsync(T entity);

        // TODO: Понять, зачем возвращать сущность обратно 
        public T Update(T entity);

        public Task<T> UpdateAsync(T entity);

        public void Delete(Guid id);

        public Task DeleteAsync(Guid id);

    }

    public interface IEfRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        IDbContextTransaction BeginTransaction();

        public IQueryable<T> GetAll();

        public Task SaveChangesAsync();
    }
}
