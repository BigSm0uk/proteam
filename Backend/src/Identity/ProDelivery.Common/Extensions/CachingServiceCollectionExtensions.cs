﻿using EasyCaching.InMemory;
using EasyCaching.Interceptor.AspectCore;
using Microsoft.Extensions.DependencyInjection;

namespace ProDelivery.Common.Extensions
{
    public static class CachingServiceCollectionExtensions
    {
        /// <summary>
        /// Регистрирует и конфигурирует EasyCaching In Memory.
        /// </summary>
        public static IServiceCollection AddEasyCachingInMemory(this IServiceCollection services)
        {
            services.AddEasyCaching(options =>
            {
                // use memory cache with a simple way
                //options.UseInMemory("default");

                // use memory cache with your own configuration
                options.UseInMemory(config =>
                {
                    config.DBConfig = new InMemoryCachingOptions
                    {
                        // scan time, default value is 60s
                        ExpirationScanFrequency = 60,
                        // total count of cache items, default value is 10000
                        SizeLimit = 100,

                        // below two settings are added in v0.8.0
                        // enable deep clone when reading object from cache or not, default value is true.
                        EnableReadDeepClone = true,
                        // enable deep clone when writing object to cache or not, default valuee is false.
                        EnableWriteDeepClone = false,
                    };
                    // the max random second will be added to cache's expiration, default value is 120
                    config.MaxRdSecond = 120;
                    // whether enable logging, default is false
                    config.EnableLogging = true;
                    // mutex key's alive time(ms), default is 5000
                    config.LockMs = 5000;
                    // when mutex key alive, it will sleep some time, default is 300
                    config.SleepMs = 300;
                }, "InMemoryCache");
            });

            // AspectCore
            services.ConfigureAspectCoreInterceptor(options => options.CacheProviderName = "InMemoryCache");

            return services;
        }
    }
}
