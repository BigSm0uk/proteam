﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using ProDelivery.Common.Behaviors;
using FluentValidation;
using Microsoft.Extensions.Logging;
using Serilog;
using System.Reflection;

namespace ProDelivery.Common.Extensions
{
    public static class CommonServiceCollectionExtensions
    {
        /// <summary>
        /// Регистрирует зависимости, используемые во всех сервисах нашего приложения.
        /// </summary>
        /// <param name="assembly">Параметр для указания сборки, в которой надо искать конфигурационные файлы (по умолчанию сборка вызывающего сервиса).</param>
        public static IServiceCollection AddCommonDependencies(this IServiceCollection services, Assembly? assembly = null)
        {
            if (assembly is null)
                assembly = Assembly.GetCallingAssembly();

            services.AddMediatR(c => c.RegisterServicesFromAssembly(assembly));
            //services.AddValidatorsFromAssemblies(new[] { Assembly.GetExecutingAssembly() }); нужен FluentValidation.DependencyInjection
            //services.AddTransient(typeof(IPipelineBehavior<,>),
            //    typeof(ValidationBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>),
            //    typeof(LoggingBehavior<,>));

            // Caching
            //services.AddEasyCachingInMemory();
            //services.AddTransient(typeof(IPipelineBehavior<,>),
            //    typeof(CachingBehavior<,>));

            services.AddAutoMapper(assembly);
            services.AddLogging(c =>
            {
                c.ClearProviders();
                c.AddSerilog();
            });

            return services;
        }
    }
}
