﻿using System.Text.Json;
using System.Text;
using RabbitMQ.Client;

namespace ProDelivery.Common.Infrastructure.RabbitMq
{
    public class RabbitMqService : IRabbitMqSevice
    {
        public void SendMessage(object obj)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message);
        }

        public void SendMessage(string message)
        {
            // "localhost" и "MyQueue" должны быть вынесены в файл конфигурации
            var factory = new ConnectionFactory()
            {
                HostName = "rabbitmq",
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "MyQueue",
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                               routingKey: "MyQueue",
                               basicProperties: null,
                               body: body);
            }
        }
    }
}