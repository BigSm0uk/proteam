﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ProDelivery.Common.Core;
using ProDelivery.Common.Core.Abstractions;

namespace ProDelivery.Common.Infrastructure.Data
{
    // TODO: Очень сомнительная вещь, возможно нужно удалить, либо переосмыслить 

    /// <summary>
    /// Предоставляет функционал для заполнения базы данных начальными значениями. 
    /// </summary>
    /// <typeparam name="TContext">Контекст базы данных.</typeparam>
    public abstract class Seed<TContext> : ISeed<TContext> where TContext : DbContext
    {
        private readonly ILogger _logger;
        private readonly TContext _context;

        public Seed(TContext context, ILogger<Seed<TContext>> logger)
        {
            _logger = logger;
            _context = context;
        }

        /// <summary>
        /// Заполняет контекст начальными данными и сохраняет в базу данных.
        /// </summary>
        public void StartSeeding()
        {
            _logger.LogInformation("Seeding database...");
            try
            {
                _logger.LogDebug("Ensure created database...");
                if (_context.Database.EnsureCreated())
                {
                    _logger.LogDebug("The database is created.");
                }
                else
                {
                    _logger.LogDebug("Migrate database...");
                    _context.Database.Migrate();
                }

                _logger.LogDebug("Inserting seed data...");
                OnDatabaseSeeding(new SeedBuilder(_context));

                _logger.LogDebug("Saving changes...");
                _context.SaveChanges();

                _logger.LogInformation("Seeding database is done.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred seeding the database.");
            }
        }

        /// <summary>
        /// Метод для сопоставления таблиц сущностей с тестовыми данными с помощью <see cref="SeedBuilder"/>.
        /// </summary>
        /// <param name="builder"></param>
        protected abstract void OnDatabaseSeeding(SeedBuilder builder);

        public interface IEntityTable
        {
            SeedBuilder AddEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : BaseEntity;
        }

        /// <summary>
        /// Предоставляет методы для указания в какую таблицу и какие данные вложить.
        /// </summary>
        public class SeedBuilder : IEntityTable
        {
            /// <summary>
            /// Поле для консистентности типов для цепочки вызовов. 
            /// Нужно из-за того, что тип сущности неизвестен во время создания билдера.
            /// </summary>
            private Type? _entityType;
            private readonly TContext _context;
            private bool _canSeed;

            public SeedBuilder(TContext context)
            {
                _context = context;
            }

            /// <summary>
            /// Определяет таблицу в которую необходимо добавить данные.
            /// </summary>
            /// <typeparam name="TEntity">Тип сущности<./typeparam>
            /// <param name="getTableFunc">Делегат для выбора таблицы <see cref="DbSet&lt;TEntity>"/>.</param>
            /// <returns>Интерфейс <see cref="IEntityTable"/> позволяющий добавить коллекцию сущностей в таблицу.</returns>
            public IEntityTable ChooseTable<TEntity>(Func<TContext, DbSet<TEntity>> getTableFunc)
                where TEntity : BaseEntity
            {
                _canSeed = false;
                _entityType = typeof(TEntity);
                _canSeed = !getTableFunc(_context).Any();
                return this;
            }

            /// <summary>
            /// Добавляет коллекцию сущностей в соответствующий <see cref="DbSet&lt;TEntity>"/> при условии, что таблица пуста.
            /// </summary>
            /// <typeparam name="TEntity">Тип сущности.</typeparam>
            /// <param name="entities">Коллекция сущностей.</param>
            /// <returns>Возвращает <see cref="SeedBuilder"/> для продолжения цепочки вызовов.</returns>
            public SeedBuilder AddEntities<TEntity>(IEnumerable<TEntity> entities)
                where TEntity : BaseEntity
            {
                if (_canSeed && typeof(TEntity).Equals(_entityType))
                {
                    _context.AddRange(entities);
                }
                return this;
            }
        }
    }
}
