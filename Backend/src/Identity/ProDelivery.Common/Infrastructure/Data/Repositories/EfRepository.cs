﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using ProDelivery.Common.Core;
using ProDelivery.Common.Core.IRepositories;
using ProDelivery.Common.Exceptions;

namespace ProDelivery.Common.Infrastructure.Data.Repositories
{
    public class EfRepository<T> : IEfRepository<T> where T : BaseEntity
    {
        public DbContext _commonContext;
        public DbSet<T> _dbSet;
                
        public EfRepository(DbContext context)
        {
            _commonContext = context;
            _dbSet = context.Set<T>();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return _commonContext.Database.BeginTransaction();
        }

        public Guid Create(T entity)
        {                        
            _dbSet.Add(entity);
            return entity.Id;
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            return entity.Id;
        }

        public void Delete(Guid id)
        {
            // TODO: Уточнить, должна ли быть проверка на этом уровне 
            var entity = _dbSet.FirstOrDefault(x => x.Id == id);
            if (entity == null)
                throw new NotFoundException(nameof(entity), id);

            _dbSet.Remove(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new NotFoundException(nameof(entity), id);

            _dbSet.Remove(entity);
        }

        public virtual IQueryable<T> GetAll()
            => _commonContext.Set<T>();

        public T? GetById(Guid id)
            => _dbSet.FirstOrDefault(x => x.Id == id);

        public async Task<T?> GetByIdAsync(Guid id)
            => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public async Task SaveChangesAsync()
            => await _commonContext.SaveChangesAsync();

        public T Update(T entity)
        {
            _dbSet.Update(entity);
            return entity;
        }

        public Task<T> UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
