﻿using Ocelot.Middleware;

namespace ProDelivery.ApiGateway.Extensions;

public static class OcelotApplicationBuilderExtensions
{
    public static IApplicationBuilder UseOcelotApiGateway(this IApplicationBuilder app)
    {
        app.UseSwaggerForOcelotUI();
        app.UseOcelot();
        return app;
    }
}
