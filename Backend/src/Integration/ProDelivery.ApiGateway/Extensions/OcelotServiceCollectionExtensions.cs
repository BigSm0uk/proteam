﻿using MMLib.Ocelot.Provider.AppConfiguration;
using MMLib.SwaggerForOcelot.DependencyInjection;
using Ocelot.DependencyInjection;

namespace ProDelivery.ApiGateway.Extensions;

public static class OcelotServiceCollectionExtensions
{
    public static IServiceCollection AddOcelotApiGateway(
        this IServiceCollection services, IConfiguration configuration)
    {
        if (configuration == null)
            throw new ArgumentNullException(nameof(configuration));

        // Костыль: Если корень приложения /app значит мы в докере.
        var microservices = configuration["contentRoot"] == "/app"
            ? "microservices.Docker.json"
            : "microservices.json";

        ((IConfigurationBuilder)configuration)
            .AddJsonFile(microservices, optional: false, reloadOnChange: true)
            .AddOcelotWithSwaggerSupport((o) =>
            {
                o.Folder = "Configuration";
            });

        services.AddOcelot()
            .AddAppConfiguration();

        services.AddSwaggerForOcelot(configuration);

        return services;
    }
}
