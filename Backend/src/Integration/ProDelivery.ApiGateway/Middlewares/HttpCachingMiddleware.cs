﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;

namespace ProDelivery.ApiGateway.Middlewares;

public class HttpCachingMiddleware
{
    private readonly RequestDelegate _next;

    public HttpCachingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(
        HttpContext context,
        IMemoryCache memoryCache,
        ILogger<HttpCachingMiddleware> logger)
    {
        if (context.Request.Method != "GET")
        {
            await _next(context);
            return;
        }

        var key = context.Request.Path;
        context.Request.EnableBuffering();
        var responseStream = context.Response.Body;
        var cache = memoryCache.Get<byte[]>(key);
        if (cache != null)
        {
            logger.LogInformation("Getting cached data");
            context.Response.Body = responseStream;
            context.Response.Headers.Append("Content-Type", "application/json; chatrset=utf-8");
            context.Response.Headers.Append("Source", "ApiGateway memory cache");
            await context.Response.Body.WriteAsync(cache);
            return;
        }
        logger.LogInformation("Caching data");
        await using var memoryStream = new MemoryStream();
        context.Response.Body = memoryStream;
        await _next(context);

        if (IsSuccess(context.Response.StatusCode))
        {
            memoryCache.Set(key, memoryStream.ToArray(), TimeSpan.FromSeconds(5));
            context.Response.Body = responseStream;
            await context.Response.Body.WriteAsync(memoryStream.ToArray());
        }
        else
        {
            context.Response.Body = responseStream;
        }
    }

    private bool IsSuccess(int statusCode)
        => statusCode >= StatusCodes.Status200OK 
        && statusCode < StatusCodes.Status300MultipleChoices;
}

public static class HttpCachingMiddlewareExtensions
{
    public static void UseHttpCaching(this IApplicationBuilder builder)
    {
        builder.UseMiddleware<HttpCachingMiddleware>();
    }
}