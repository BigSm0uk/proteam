using Ocelot.Middleware;
using ProDelivery.ApiGateway.Extensions;
using ProDelivery.ApiGateway.Middlewares;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddOcelotApiGateway(builder.Configuration);
builder.Services.AddCors(cors
    => cors.AddPolicy("AllowAll", policy =>
    {
        policy.AllowAnyOrigin();
        policy.AllowAnyHeader();
        policy.AllowAnyMethod();
    }));

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwaggerForOcelotUI();

app.UseCors("AllowAll");

app.UseHttpCaching();

await app.UseOcelot();

app.Run();
