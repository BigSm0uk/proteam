﻿namespace ProDelivery.Contracts.Redis;

public record OrderDto
(
    Guid Id,
    string Title,
    string Description,
    double DeliveryCost,
    string Status

) : IRedisDbContract 
{
    public string FieldName => Id.ToString();
}

public interface IRedisDbContract
{
    string FieldName { get; }
}

public static class RedisContractKeys
{
    public static string AvailableOrders = "AvailableOrders";
}
