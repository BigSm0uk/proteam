﻿namespace ProDelivery.Contracts;

public record StatusChangedDto
{
    public StatusChangedDto(Guid id, string status)
        => (Id, Status) = (id, status);
    public Guid Id { get; init; }
    public string Status { get; init; }
}

public record OrderStatusChangedDto : StatusChangedDto
{
    public OrderStatusChangedDto(Guid id, string status) : base(id, status)
    { }
}

public record DeliveryStatusChangedDto : StatusChangedDto
{
    public DeliveryStatusChangedDto(Guid id, string status) : base(id, status)
    { }
}

public static class MessageQueues
{
    public static readonly string OrderStatusChangedQueueName = Format(nameof(OrderStatusChangedDto));
    public static readonly Uri OrderStatusChangedQueueUri = new($"queue:{OrderStatusChangedQueueName}");

    public static readonly string DeliveryStatusChangedQueueName = Format(nameof(DeliveryStatusChangedDto));
    public static readonly Uri DeliveryStatusChangedQueueUri = new($"queue:{DeliveryStatusChangedQueueName}");

    private static string Format(string dtoName) =>
        string.Concat(dtoName.Select(c => char.IsUpper(c) ? "_" + c : c.ToString()))
        .TrimStart('_')
        .ToLower()[0..^3] + "queue";
}