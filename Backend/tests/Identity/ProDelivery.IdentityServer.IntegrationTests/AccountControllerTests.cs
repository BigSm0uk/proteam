using IdentityModel.Client;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace ProDelivery.IdentityServer.IntegrationTests
{
    public class AccountControllerTests : IClassFixture<IdentityServerFactory>
    {
        private readonly HttpClient _client;

        public AccountControllerTests(IdentityServerFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task Register_ValidUserToRegister_ShouldReturnSuccess()
        {
            var userToRegister = new
            {
                Username = "userToRegister",
                Password = "userToRegister",
                ConfirmPassword = "userToRegister",
                Email = "userToRegister@email.com",
                PhoneNumber = "777000",
                Role = "Customer"
            };

            // TODO: ������ ������ ���������� NotFound
            var response = await _client.PostAsJsonAsync("/account/register", userToRegister);
            
            response.EnsureSuccessStatusCode();

            var userId = await response.Content.ReadFromJsonAsync<Guid>();

            Assert.NotEqual(Guid.Empty, userId);
        }

        [Fact]
        public async Task LoginFromConsole_ValidUserToLogin_ShouldReturnSuccess()
        {
            var discoveryDocument = await _client.GetDiscoveryDocumentAsync();
            
            var tokenResponse = await _client.RequestPasswordTokenAsync(
                new PasswordTokenRequest
                {
                    Address = discoveryDocument.TokenEndpoint,
                    ClientId = "console",
                    ClientSecret = "secret",
                    Scope = "customerApi orderApi",
                    UserName = "testUser",
                    Password = "testUser!1"
                });

            Assert.NotNull(tokenResponse.AccessToken);
        }

        [Fact]
        public async Task LoginFromSwagger_ValidUserToLogin_ShouldReturnSuccess()
        {
            var discoveryDocument = await _client.GetDiscoveryDocumentAsync(_client?.BaseAddress?.ToString());

            var tokenResponse = await _client.RequestPasswordTokenAsync(
                new PasswordTokenRequest
                {
                    Address = discoveryDocument.TokenEndpoint,
                    ClientId = "swagger",
                    UserName = "testUser",
                    Password = "testUser!1"
                });

            Assert.NotNull(tokenResponse.AccessToken);
        }
    }
}