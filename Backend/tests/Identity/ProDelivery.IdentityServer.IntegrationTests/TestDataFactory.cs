﻿using ProDelivery.IdentityServer.Models;

namespace ProDelivery.IdentityServer.IntegrationTests;

internal class TestDataFactory
{
    public static IEnumerable<ApplicationUser> ApplicationUsers => new List<ApplicationUser>()
        {
            new ApplicationUser
            {
                UserName = "testUser",
                Email = "testUser@email.com",
                EmailConfirmed = true,
                Role = ApplicationRoles.Customer
            },
        };
}
