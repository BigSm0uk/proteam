﻿using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using ProDelivery.Common.Extensions;

namespace ProDelivery.IdentityServer.IntegrationTests;

public class IdentityServerFactory : WebApplicationFactory<Program>, IAsyncLifetime
{
    private const string _postgresPort = "5432";
    private readonly IContainer _container;

    public IdentityServerFactory()
    {
        Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", Environments.Development);

        // up postgres container before tests
        _container = new ContainerBuilder()
          .WithImage("postgres:14.3-alpine")
          //.WithNetwork()
          .WithPortBinding(_postgresPort, assignRandomHostPort: true)
          .WithEnvironment("POSTGRES_PASSWORD", "root")
          .WithWaitStrategy(Wait.ForUnixContainer())//.UntilContainerIsHealthy())
          .Build();
    }

    public async Task InitializeAsync() => await _container.StartAsync();
    public new async Task DisposeAsync() => await _container.DisposeAsync();

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        // Override configuration 
        builder.ConfigureAppConfiguration((context, configBuilder) =>
        {
            var hostPort = _container.GetMappedPublicPort(_postgresPort).ToString();

            // From json
            configBuilder.AddJsonFile("testsettings.json");
            // From code
            configBuilder.AddInMemoryCollection(
                new Dictionary<string, string?>
                {
                    //["ConnectionStrings:DefaultConnection"] = "Data Source=_test.db",
                    ["ConnectionStrings:PostgresConnection"]
                        = $"Server=localhost;Database=IdentityDb;Username=postgres;Password=root;Port={hostPort}"
                });
        });

        builder.ConfigureTestServices(services =>
        {
            // You can override db context
            //services.RemoveDbContext<ApplicationDbContext>();
            //services.AddDbContext<ApplicationDbContext>(options =>
            //{
            //    options.UseNpgsql("Server=localhost;Database=IdentityDb;Username=postgres;Password=root;Port=5432");
            //});
            //services.EnsureDbCreated<ApplicationDbContext>();

            services.ReplaceSingleton(TestDataFactory.ApplicationUsers);
        });
    }
}
