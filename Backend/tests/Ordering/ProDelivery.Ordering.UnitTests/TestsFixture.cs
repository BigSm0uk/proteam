﻿using AutoMapper;
using ProDelivery.Ordering.Core;

namespace ProDelivery.Ordering.UnitTests;

public class TestsFixture : IDisposable
{
    public Mapper Mapper { get; }

    public TestsFixture()
    {
        var config = new MapperConfiguration(
            x =>
            {
                x.AddProfile<ContractsMappingProfile>();
                x.AddProfile<OrderMappingProfile>();
            });

        Mapper = new Mapper(config);
    }

    public void Dispose()
    {
        
    }
}