using FluentAssertions;
using Moq;
using AutoFixture.AutoMoq;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Application;
using ProDelivery.Ordering.Core.Domain;
using AutoFixture;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace ProDelivery.Ordering.UnitTests;

public class CreateOrderTests : IClassFixture<TestsFixture>
{
    private readonly CreateOrderCommandHandler _createOrderCommandHandler;

    public CreateOrderTests(TestsFixture testFixture)
    {
        // �������������� ����������� ���������
        var redisRepository = new Fixture()
            .Customize(new AutoMoqCustomization())
            .Create<IRedisRepository<OrderDto>>();

        // ���� ������ ����������� ��� ������� �����
        var dbContext = OrderingDbContextFactory.CreateInMemoryDbContext();
        var orderRepositoty = new OrderRepository(dbContext);

        // ��������������� ������� ��������� � ��������������
        var mapper = testFixture.Mapper;

        _createOrderCommandHandler = new CreateOrderCommandHandler(
            orderRepositoty,
            mapper,
            redisRepository);
    }

    [Fact]
    public async void Handle_CreateOrder_ShouldReturnGuid()
    {
        // Arrange
        var command = new CreateOrderCommand
        {
            Title = "������� ��������� (�� 40 ��������)",
            Description = "�������� ������ (�� 250 ��������)",
            DeliveryCost = 500,
            UserId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165")
        };

        // Act 
        var guid = await _createOrderCommandHandler.Handle(command, CancellationToken.None);

        // Assert
        guid.Should().NotBeEmpty();
    }

    [Fact]
    public async void Handle_CreateOrderWithNullDescription_ShouldFail()
    {
        // Arrange
        var command = new CreateOrderCommand
        {
            Title = "������� ��������� (�� 40 ��������)",
            Description = null!,
            DeliveryCost = 500,
            UserId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165")
        };

        // Act 
        var act = async () => await _createOrderCommandHandler.Handle(command, CancellationToken.None);

        // Assert
        await act.Should().ThrowAsync<DbUpdateException>();
    }
}