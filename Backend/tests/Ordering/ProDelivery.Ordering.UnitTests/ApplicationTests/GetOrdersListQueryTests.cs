using FluentAssertions;
using Moq;
using AutoFixture.AutoMoq;
using ProDelivery.Ordering.Core.Abstractions;
using ProDelivery.Ordering.Core.Application;
using ProDelivery.Ordering.Core.Domain;
using AutoFixture;
using ProDelivery.Contracts.Redis;
using ProDelivery.Ordering.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace ProDelivery.Ordering.UnitTests;

public class GetOrdersListQueryTests : IClassFixture<TestsFixture>
{
    private readonly GetOrderListQueryHandler _getOrderListQueryHandler;

    public GetOrdersListQueryTests(TestsFixture testFixture)
    {
        // �������������� ����������� ���������
        var redisRepository = new Fixture()
            .Customize(new AutoMoqCustomization())
            .Create<IRedisRepository<OrderDto>>();

        // ���� ������ ����������� ��� ������� �����
        var dbContext = OrderingDbContextFactory.CreateInMemoryDbContext();
        var orderRepositoty = new OrderRepository(dbContext);

        // ��������������� ������� ��������� � ��������������
        var mapper = testFixture.Mapper;

        _getOrderListQueryHandler = new GetOrderListQueryHandler(
            orderRepositoty,
            mapper);
    }

    [Fact]
    public async void Handle_GetOrdersList_ShouldNotReturnNull()
    {
        // Arrange
        var query = new GetOrderListQuery();

        // Act 
        var orders = await _getOrderListQueryHandler.Handle(query, CancellationToken.None);

        // Assert
        orders.Should().NotBeNull();
    }

    [Fact]
    public async void Handle_NullQuery_ShouldFail()
    {
        // Arrange
        var query = null as GetOrderListQuery;

        // Act 
        var act = async () => await _getOrderListQueryHandler.Handle(query!, CancellationToken.None);

        // Assert
        await act.Should().ThrowAsync<NullReferenceException>();
    }
}