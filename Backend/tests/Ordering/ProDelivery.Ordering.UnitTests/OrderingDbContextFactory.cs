﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Ordering.DataAccess;

namespace ProDelivery.Ordering.UnitTests;

public static class OrderingDbContextFactory
{
    public static OrderingDbContext CreateInMemoryDbContext()
    {
        var options = new DbContextOptionsBuilder<OrderingDbContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
        var context = new OrderingDbContext(options);
        context.Database.EnsureCreated();

        //context.Orders.AddRange();

        context.SaveChanges();
        return context;
    }

    public static OrderingDbContext CreateSqliteDbContext()
    {
        var options = new DbContextOptionsBuilder<OrderingDbContext>()
            .UseSqlite($"Data Source={Guid.NewGuid()}.db")
            .Options;
        var context = new OrderingDbContext(options);
        context.Database.EnsureCreated();

        //context.Orders.AddRange();

        context.SaveChanges();
        return context;
    }

    public static void Destroy(OrderingDbContext context)
    {
        context.Database.EnsureDeleted();
        context.Dispose();
    }
}