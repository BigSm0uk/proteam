﻿using ProDelivery.Ordering.Core.Domain;

namespace ProDelivery.Ordering.IntegrationTests;

internal class TestDataFactory
{
    public static IEnumerable<Order> Orders => new List<Order>
    {
        new
        (
            userId: Guid.Parse("daceffc1-2626-4b08-8885-cba2a10edbf8"),
            description: "Test order description", 
            deliveryCost: 250.0,
            title: "Test order"
        ),
        new
        (
            userId: Guid.Parse("e7cf2a0a-74e0-4765-90d5-2ca532be37bf"),
            description: "Test order description 2", 
            deliveryCost: 600.0,
            title: "Test order 2"
        ),
    };
}