using FluentAssertions;
using System.Net;
using System.Net.Http.Json;

namespace ProDelivery.Ordering.IntegrationTests;

public class OrderControllerTests : IClassFixture<OrderingApiFactory>
{
    private readonly OrderingApiFactory _api;

    public OrderControllerTests(OrderingApiFactory ordersApiFactory)
    {
        _api = ordersApiFactory;
    }

    [Fact]
    public async void CreateOrder_PositiveFlow_ShouldReturnSeccess()
    {
        //Arrange
        var orderToCreate = new
        {
            Title = "string",
            Description = "string",
            DeliveryCost = 500,
            UserId = "00000000-0000-0000-0000-000000000001"
        };

        //Act
        var client = await _api.GetAuthorizedClientAsync();
        var response = await client.PostAsJsonAsync("api/v1/Orders", orderToCreate);

        //Assert
        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadFromJsonAsync<Guid>();

        content.Should().NotBe(Guid.Empty);

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }
}
