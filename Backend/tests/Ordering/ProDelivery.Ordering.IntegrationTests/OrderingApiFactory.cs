﻿using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using IdentityModel.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestPlatform.PlatformAbstractions;
using ProDelivery.Ordering.DataAccess;
using ProDelivery.Ordering.DependencyInjection;
using System.Reflection;

namespace ProDelivery.Ordering.IntegrationTests;

public class OrderingApiFactory : WebApplicationFactory<Program>, IAsyncLifetime
{
    private const string _rabbitMqPort = "5672";
    private const string _redisPort = "6379";
    private const string _postgresPort = "5432";
    private const string _dotnetPort = "80";
    private readonly IContainer _authApiContainer;
    private readonly IContainer _orderingDbContainer;
    private readonly IContainer _rabbitMqContainer;
    private readonly IContainer _redisContainer;
    private HttpClient? _authorizedClient;


    public OrderingApiFactory()
    {
        _authApiContainer = GetAuthApiContainer();
        _orderingDbContainer = GetOrderingDbContainer();
        _rabbitMqContainer = GetRabbitMqContainer();
        _redisContainer = GetRedisContainer();

        // Testing configuration 
        Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Testing");
        Environment.SetEnvironmentVariable("ConnectionStrings:RedisHost", "localhost");
        Environment.SetEnvironmentVariable("ConnectionStrings:RabbitMqHost", "localhost");
    }

    // *Подключение к контейнерам с рандомным портом вычисляется по запуску контейнера
    //Environment.SetEnvironmentVariable(
    //   "ConnectionStrings:IdentityServerUrl", "http://localhost:6001");

    public async Task InitializeAsync()
    {
        await _orderingDbContainer.StartAsync();
        await _rabbitMqContainer.StartAsync();
        await _redisContainer.StartAsync();
    }

    public new async Task DisposeAsync()
    {
        await _authApiContainer.DisposeAsync();
        await _orderingDbContainer.DisposeAsync();
        await _rabbitMqContainer.DisposeAsync();
        await _redisContainer.DisposeAsync();
    }

    public async Task<HttpClient> GetAuthorizedClientAsync()
        => _authorizedClient ?? await CreateAuthorizedClientAsync();

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        var hostPort = _orderingDbContainer.GetMappedPublicPort(_postgresPort).ToString();
        var connection = $"Server=localhost;Database=OrderingApiDb;Username=postgres;Password=root;Port={hostPort}";

        // Запускается ещё до конфигурирования пайплайна,
        // но уже может обращаться к IServiceProvider
        builder.ConfigureTestServices(services =>
        {
            services.RemoveDbContext<OrderingDbContext>();
            services.AddDbContext<OrderingDbContext>(options =>
            {
                options.UseNpgsql(connection);
                AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            });
            services.EnsureDbCreated<OrderingDbContext>(context =>
            {
                context.AddRange(TestDataFactory.Orders);
                context.SaveChanges();
            });
        });
    }

    private IContainer GetOrderingDbContainer()
    {
        return new ContainerBuilder()
          .WithImage("postgres:15.2")
          .WithPortBinding(_postgresPort, assignRandomHostPort: true)
          .WithEnvironment("POSTGRES_PASSWORD", "root")
          .WithWaitStrategy(Wait.ForUnixContainer())
          .Build();
    }

    private IContainer GetRabbitMqContainer()
    {
        return new ContainerBuilder()
          .WithImage("rabbitmq:3-management")
          .WithHostname("rabbitmq")
          .WithPortBinding(_rabbitMqPort, _rabbitMqPort)
          .WithWaitStrategy(Wait.ForUnixContainer())
          .Build();
    }

    private IContainer GetRedisContainer()
    {
        return new ContainerBuilder()
           .WithImage("redis")
           .WithHostname("redis")
           .WithPortBinding(_redisPort, _redisPort)
           .WithCommand("redis-server", "--appendonly", "yes")
           .WithWaitStrategy(Wait.ForUnixContainer())
           .Build();
    }

    private IContainer GetAuthApiContainer()
    {
        return new ContainerBuilder()
            .WithImage("identityserver:4.0")
            .WithPortBinding(_dotnetPort, assignRandomHostPort: true)
            .WithEnvironment("ASPNETCORE_ENVIRONMENT", Environments.Development)
            .WithWaitStrategy(Wait.ForUnixContainer())
            .Build();
    }

    private async Task<HttpClient> CreateAuthorizedClientAsync()
    {
        await BuildAuthApiImage();

        await _authApiContainer.StartAsync();
        var authHostPort = _authApiContainer.GetMappedPublicPort(_dotnetPort);
        var authClient = new HttpClient()
        {
            BaseAddress = new Uri($"http://localhost:{authHostPort}/")
        };

        Environment.SetEnvironmentVariable(
            "ConnectionStrings:IdentityServerUrl", authClient.BaseAddress.ToString());

        string accessToken = await GetAccessToken(authClient);

        _authorizedClient = CreateClient();
        _authorizedClient.DefaultRequestHeaders.Add(
            "Authorization", "Bearer " + accessToken);

        return _authorizedClient;
    }

    private async Task BuildAuthApiImage()
    {
        await new ImageFromDockerfileBuilder()
            .WithDockerfileDirectory(
                CommonDirectoryPath.GetSolutionDirectory(), string.Empty)
            .WithDockerfile("src/Identity/ProDelivery.IdentityServer/Dockerfile.Test")
            .WithName("identityserver:4.0")
            .WithDeleteIfExists(false)
            .Build()
            .CreateAsync()
            .ConfigureAwait(false);
    }

    private async Task<string> GetAccessToken(HttpClient authClient)
    {
        // Перепопытки запроса на случай, если приложение не успело подняться 
        DiscoveryDocumentResponse? discoveryDocument;
        int i = 4;
        do
        {
            await Task.Delay(100 * i++);
            discoveryDocument = await authClient.GetDiscoveryDocumentAsync();
        }
        while
        (
            _authApiContainer.State == TestcontainersStates.Running
            && discoveryDocument.HttpResponse == null
            && i < 100
        );

        var tokenResponse = await authClient.RequestPasswordTokenAsync(
            new PasswordTokenRequest
            {
                Address = discoveryDocument.TokenEndpoint,
                ClientId = "console",
                ClientSecret = "secret",
                Scope = "customerApi orderApi",
                UserName = "Customer",
                Password = "Customer!1"
            });

        return tokenResponse.AccessToken;
    }

}
