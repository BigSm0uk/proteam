using FluentAssertions;
using IdentityModel;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Text;

namespace ProDelivery.Ordering.IntegrationTests;

public class TokenTests
{ 
    public TokenTests()
    {
        var claimsdata = new[]
           {
                 new Claim(JwtClaimTypes.Subject, "23468a06-012e-47d9-97e6-a1e6128bfaca"),
                 new Claim(JwtClaimTypes.Scope, "customerApi orderApi"),
                 new Claim(JwtClaimTypes.ClientId, "console"),
           };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("secret"));
        var signInCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
                  issuer: "http://localhost:6001",
                  audience: "http://localhost:6001/resources",
                  //expires: DateTime.Now.AddMinutes(3),
                  claims: claimsdata
                //signingCredentials: signInCred
                );

        var tokenHandler = new JwtSecurityTokenHandler();
        tokenHandler.SetDefaultTimesOnTokenCreation = true;

        var token2 = tokenHandler.CreateJwtSecurityToken(
            issuer: "http://localhost:6001",
            audience: "http://localhost:6001/resources",
            signingCredentials: signInCred
            );

        var encodedToken = tokenHandler.WriteToken(token);

        tokenHandler.CreateJwtSecurityToken(new SecurityTokenDescriptor
         {
             NotBefore = new DateTime(1681833888),
             Expires = new DateTime(1681837488),
             Issuer = "http://localhost:6001",
             Audience = "http://localhost:6001/resources",

         });

        var encodedToken2 = new JwtBuilder()
            .AddHeader(HeaderName.Type, "at+jwt")
            .AddHeader(HeaderName.KeyId, "D6F3FF02CCA32C0B03E6ACF47279284E")
            .WithAlgorithm(new HMACSHA256Algorithm())
            .WithSecret("secret")
            .AddClaim(JwtClaimTypes.NotBefore, DateTimeOffset.UtcNow.ToUnixTimeSeconds())
            .AddClaim(JwtClaimTypes.Expiration, DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
            .AddClaim(JwtClaimTypes.Issuer, "http://localhost:6001")
            .AddClaim(JwtClaimTypes.Audience, "http://localhost:6001/resources")
            .AddClaim(JwtClaimTypes.ClientId, "console")
            .AddClaim(JwtClaimTypes.Subject, "23468a06-012e-47d9-97e6-a1e6128bfaca")
            .AddClaim(JwtClaimTypes.AuthenticationTime, DateTimeOffset.UtcNow.ToUnixTimeSeconds())
            .AddClaim(JwtClaimTypes.IdentityProvider, "local")
            .AddClaim(JwtClaimTypes.JwtId, "BE040BA3ABB97FC5A09F7A8DB052AB67")
            .AddClaim(JwtClaimTypes.IssuedAt, DateTimeOffset.UtcNow.ToUnixTimeSeconds())
            .AddClaim(JwtClaimTypes.Scope, new[] { "customerApi", "orderApi" })
            .AddClaim(JwtClaimTypes.AuthenticationMethod, new[] { "pwd" })
            .Encode();
    }
}
