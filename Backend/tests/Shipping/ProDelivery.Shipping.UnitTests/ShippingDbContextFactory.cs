﻿using Microsoft.EntityFrameworkCore;
using ProDelivery.Shipping.DataAccess;

namespace ProDelivery.Shipping.UnitTests;

public static class ShippingDbContextFactory
{
    public static ShippingDbContext CreateInMemoryDbContext()
    {
        var options = new DbContextOptionsBuilder<ShippingDbContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
        var context = new ShippingDbContext(options);
        context.Database.EnsureCreated();

        //context.Orders.AddRange();

        context.SaveChanges();
        return context;
    }

    public static ShippingDbContext CreateSqliteDbContext()
    {
        var options = new DbContextOptionsBuilder<ShippingDbContext>()
            .UseSqlite($"Data Source={Guid.NewGuid()}.db")
            .Options;
        var context = new ShippingDbContext(options);
        context.Database.EnsureCreated();

        //context.Deliveries.AddRange();

        context.SaveChanges();
        return context;
    }

    public static void Destroy(ShippingDbContext context)
    {
        context.Database.EnsureDeleted();
        context.Dispose();
    }
}