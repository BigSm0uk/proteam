﻿using AutoMapper;
using MassTransit;
using ProDelivery.Shipping.Core;

namespace ProDelivery.Shipping.UnitTests;

public class TestsFixture : IDisposable
{
    public Mapper Mapper { get; }
    public IBus MessageBus { get; internal set; }

    public TestsFixture()
    {
        var config = new MapperConfiguration(
            x =>
            {
                //x.AddProfile<ContractsMappingProfile>();
                x.AddProfile<DeliveryMappingProfile>();
            });

        Mapper = new Mapper(config);
    }

    public void Dispose()
    {
        
    }
}