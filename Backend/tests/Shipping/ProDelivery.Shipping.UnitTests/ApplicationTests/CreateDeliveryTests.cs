using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using MassTransit;
using Moq;
using ProDelivery.Contracts.Redis;
using ProDelivery.Shipping.Core.Abstractions;
using ProDelivery.Shipping.Core.Application;
using ProDelivery.Shipping.DataAccess;

namespace ProDelivery.Shipping.UnitTests;

public class CreateDeliveryTests : IClassFixture<TestsFixture>
{
    private readonly CreateDeliveryCommandHandler _createDeliveryCommandHandler;
    private readonly IFixture _autoMoqFixture;
    private readonly Mock<IRedisRepository<OrderDto>> _redisRepositoryMock;
    private readonly Mock<IBus> _bus;

    public CreateDeliveryTests(TestsFixture testFixture)
    {
        // �������������� ����������� ���������
        _autoMoqFixture = new Fixture().Customize(new AutoMoqCustomization());
        _redisRepositoryMock = _autoMoqFixture.Create<Mock<IRedisRepository<OrderDto>>>();
        _bus = _autoMoqFixture.Create<Mock<IBus>>();

        // ���� ������ ����������� ��� ������� �����
        var dbContext = ShippingDbContextFactory.CreateInMemoryDbContext();
        var deliveryRepositoty = new DeliveryRepository(dbContext);

        // ��������������� ������� ��������� � ��������������
        var mapper = testFixture.Mapper;

        _createDeliveryCommandHandler = new CreateDeliveryCommandHandler(
            deliveryRepositoty,
            mapper,
            _redisRepositoryMock.Object,
            _bus.Object);
    }

    [Fact]
    public async void Handle_CreateDelivery_ShouldReturnGuid()
    {
        // Arrange
        var command = new CreateDeliveryCommand
        {
            OrderId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"),
            CourierId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b166")
        };

        _redisRepositoryMock
            .Setup(x => x.PopAsync(It.IsAny<string>()))
            .ReturnsAsync(new Fixture().Create<OrderDto>);
        _bus.Setup(x => x.GetSendEndpoint(It.IsAny<Uri>()))
            .ReturnsAsync(_autoMoqFixture.Create<ISendEndpoint>);

        // Act 
        var guid = await _createDeliveryCommandHandler.Handle(command, CancellationToken.None);

        // Assert
        guid.Should().NotBeEmpty();
    }
}